<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([

            'recepient_id' => '1',
            'nama' => 'Razak Ahmad',
            'jantina' => 'L',
            // 'no_kp' => '201013141111',
            'tarikh_lahir' => Carbon::create('2000', '01', '01'),
            'bangsa' => 'M',
            'bangsa_lain' => 'M',
            'warganegara' => 'M',
            'agama' => 'I',
            'agama_lain' => 'I',
            'status_perkahwinan' => 'K',
            'alamat' => 'No.732',
            'village_id' => '1',
            'bandar' => 'Batu Caves',
            'poskod' => '68100',
            'negeri' => 'Selangor',
            'dun_id' => '1',
            'dm_id' => '1',
            'no_telefon' => '011774743',
            'kad_oku' => 'T',
            'status' => 'Aktif',
            'created_by' => '1',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('profiles')->insert([

            'recepient_id' => '2',
            'nama' => 'Noni',
            'jantina' => 'P',
            // 'no_kp' => '201013141112',
            'tarikh_lahir' => Carbon::create('2000', '01', '01'),
            'bangsa' => 'M',
            'bangsa_lain' => 'M',
            'warganegara' => 'M',
            'agama' => 'I',
            'agama_lain' => 'I',
            'status_perkahwinan' => 'B',
            'alamat' => 'No.732',
            'village_id' => '2',
            'bandar' => 'Batu Caves',
            'poskod' => '68100',
            'negeri' => 'Selangor',
            'dun_id' => '1',
            'dm_id' => '2',
            'no_telefon' => '011774743',
            'kad_oku' => 'T',
            'status' => 'Aktif',
            'created_by' => '1',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
