<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VillagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('villages')->insert([
            'nama' => 'Kampung A',
            'keterangan' => '-',
            'status' => 'Aktif',
            'created_by' => '1',
        ]);

        DB::table('villages')->insert([
            'nama' => 'Kampung B',
            'keterangan' => '-',
            'status' => 'Aktif',
            'created_by' => '1',
        ]);

        DB::table('villages')->insert([
            'nama' => 'Kampung C',
            'keterangan' => '-',
            'status' => 'Aktif',
            'created_by' => '1',
        ]);

        DB::table('villages')->insert([
            'nama' => 'Kampung D',
            'keterangan' => '-',
            'status' => 'Aktif',
            'created_by' => '1',
        ]);

        DB::table('villages')->insert([
            'nama' => 'Kampung E',
            'keterangan' => '-',
            'status' => 'Aktif',
            'created_by' => '1',
        ]);
    }
}
