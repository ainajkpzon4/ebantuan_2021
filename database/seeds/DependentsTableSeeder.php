<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class DependentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dependents')->insert([
            'nama' => 'Qistina',
            'no_kp' => '091010100981',
            'recepient_id' => '1',
            'hubungan' => 'Anak Kandung',
            'ref_education_id' => '2',
            'jantina' => 'P',
            'umur' => '11',
            'kad_oku' => 'T',
            'nota' => 'test 5',
            'status' => 'Aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '01', '01'),
        ]);

        DB::table('dependents')->insert([
            'nama' => 'Qabil',
            'no_kp' => '101010100981',
            'recepient_id' => '1',
            'hubungan' => 'Anak Kandung',
            'ref_education_id' => '3',
            'jantina' => 'L',
            'umur' => '12',
            'kad_oku' => 'T',
            'nota' => 'test 6',
            'status' => 'Aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '01', '01'),
        ]);

        DB::table('dependents')->insert([

            'nama' => 'Arissa',
            'no_kp' => '091010100986',
            'recepient_id' => '2',
            'hubungan' => 'Anak Kandung',
            'ref_education_id' => '5',
            'jantina' => 'P',
            'umur' => '11',
            'kad_oku' => 'T',
            'nota' => 'test 6',
            'status' => 'Aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '01', '01'),
        ]);

        DB::table('dependents')->insert([

            'nama' => 'Iman',
            'no_kp' => '101010100986',
            'recepient_id' => '2',
            'hubungan' => 'Anak Kandung',
            'ref_education_id' => '4',
            'jantina' => 'L',
            'umur' => '10',
            'kad_oku' => 'T',
            'nota' => 'test 8',
            'status' => 'Aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '01', '01'),
        ]);
    }
}
