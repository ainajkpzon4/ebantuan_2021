<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class DependentOtherDonationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dependent_other_donations')->insert([
            'recepient_id' => '1',
            'dependent_id' => '2',
            'category_id' => '1',
            'tarikh_serahan' => Carbon::create('2020', '08', '01'),
            'nama' => 'Bantuan laptop',
            'sebab' => 'Anak belajar',
            'status' => 'aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '04', '01'),
        ]);

        DB::table('dependent_other_donations')->insert([
            'recepient_id' => '1',
            'dependent_id' => '2',
            'category_id' => '2',
            'tarikh_serahan' => Carbon::create('2020', '08', '01'),
            'nama' => 'Bantuan laptop',
            'sebab' => 'Anak belajar 2',
            'status' => 'aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '04', '01'),
        ]);

        DB::table('dependent_other_donations')->insert([
            'recepient_id' => '2',
            'dependent_id' => '1',
            'category_id' => '2',
            'tarikh_serahan' => Carbon::create('2020', '08', '01'),
            'nama' => 'Bantuan laptop',
            'sebab' => 'Anak belajar 3',
            'status' => 'aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '04', '01'),
        ]);

        DB::table('dependent_other_donations')->insert([
            'recepient_id' => '2',
            'dependent_id' => '1',
            'category_id' => '1',
            'tarikh_serahan' => Carbon::create('2020', '08', '01'),
            'nama' => 'Bantuan laptop 4',
            'sebab' => 'Anak belajar 4',
            'status' => 'aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '04', '01'),
        ]);
    }
}
