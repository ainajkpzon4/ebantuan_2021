<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'nama' => 'Super Admin',
            'keterangan' => 'All Task',

        ]);

        DB::table('roles')->insert([
            'nama' => 'Admin',
            'keterangan' => 'All Task',

        ]);

        DB::table('roles')->insert([
            'nama' => 'Supervisor',
            'keterangan' => 'Search & Report - Cannot delete',

        ]);

        DB::table('roles')->insert([
            'nama' => 'Data Entry',
            'keterangan' => 'Search & Insert - Cannot delete',

        ]);
    }
}
