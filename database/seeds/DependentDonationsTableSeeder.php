<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class DependentDonationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dependent_donations')->insert([
            'recepient_id' => '1',
            'dependent_id' => '1',
            'ref_donation_id' => '1',
            'tarikh_mohon' => Carbon::create('2000', '01', '01'),
            'tarikh_lulus' => Carbon::create('2000', '01', '01'),
            'status_kelulusan' => 'Lulus',
            'nota' => 'test 11',
            'status' => 'aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '01', '01'),
        ]);

        DB::table('dependent_donations')->insert([
            'recepient_id' => '1',
            'dependent_id' => '1',
            'ref_donation_id' => '2',
            'tarikh_mohon' => Carbon::create('2000', '01', '01'),
            'tarikh_lulus' => Carbon::create('2000', '01', '01'),
            'status_kelulusan' => 'Lulus',
            'nota' => 'test 11',
            'status' => 'aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '01', '01'),
        ]);

        DB::table('dependent_donations')->insert([
            'recepient_id' => '2',
            'dependent_id' => '2',
            'ref_donation_id' => '1',
            'tarikh_mohon' => Carbon::create('2000', '01', '01'),
            'tarikh_lulus' => Carbon::create('2000', '01', '01'),
            'status_kelulusan' => 'Lulus',
            'nota' => 'test 11',
            'status' => 'aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '01', '01'),
        ]);

        DB::table('dependent_donations')->insert([
            'recepient_id' => '2',
            'dependent_id' => '2',
            'ref_donation_id' => '2',
            'tarikh_mohon' => Carbon::create('2000', '01', '01'),
            'tarikh_lulus' => Carbon::create('2000', '01', '01'),
            'status_kelulusan' => 'Lulus',
            'nota' => 'test 11',
            'status' => 'aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '01', '01'),
        ]);
    }
}
