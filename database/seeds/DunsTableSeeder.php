<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DunsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('duns')->insert([
            'nama' => 'Gombak Setia',
            'keterangan' => 'Gombak Setia',
            'status' => 'aktif',
            // 'created_by' => '1',
        ]);

        DB::table('duns')->insert([
            'nama' => 'Sungai Tua',
            'keterangan' => 'Sungai Tua',
            'status' => 'aktif',
            // 'created_by' => '1',
        ]);

        DB::table('duns')->insert([
            'nama' => 'Kelang',
            'keterangan' => 'Kelang',
            'status' => 'aktif',
            // 'created_by' => '1',
        ]);
    }
}
