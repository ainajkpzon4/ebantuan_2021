<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class DonationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('donations')->insert([
            'recepient_id' => '1',
            'ref_donation_id' => '4',
            'tarikh_mohon' => Carbon::create('2000', '01', '01'),
            'tarikh_lulus' => Carbon::create('2000', '01', '01'),
            'status_kelulusan' => 'Lulus',
            'nota' => 'test 9',
            'status' => 'aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '01', '01'),
        ]);

        DB::table('donations')->insert([
            'recepient_id' => '1',
            'ref_donation_id' => '2',
            'tarikh_mohon' => Carbon::create('2000', '01', '01'),
            'tarikh_lulus' => Carbon::create('2000', '01', '01'),
            'status_kelulusan' => 'Lulus',
            'nota' => 'test 9',
            'status' => 'aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '01', '01'),

        ]);

        DB::table('donations')->insert([
            'recepient_id' => '2',
            'ref_donation_id' => '3',
            'tarikh_mohon' => Carbon::create('2000', '01', '01'),
            'tarikh_lulus' => Carbon::create('2000', '01', '01'),
            'status_kelulusan' => 'Lulus',
            'nota' => 'test 11',
            'status' => 'aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '01', '01'),
        ]);
    }
}
