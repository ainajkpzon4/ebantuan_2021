<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([

            'name' => 'Anuar bin Jasmin',
            'no_kp' => '790314141234',
            'email' => 'anuar@gmail.com',
            'password' => bcrypt('password'),
            'role_id' => '3',
        ]);

        DB::table('users')->insert([

            'name' => 'Siti Noor Aina',
            'no_kp' => '801013146114',
            'email' => 'maisarah@gmail.com',
            'password' => bcrypt('password'),
            'role_id' => '1',
        ]);
    }
}
