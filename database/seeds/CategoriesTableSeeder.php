<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'nama' => 'Kewangan',
            'keterangan' => 'Sumbangan berbentuk kewangan',
            'status' => 'Aktif',
            'created_by' => '1',
        ]);

        DB::table('categories')->insert([
            'nama' => 'Makanan Asas',
            'keterangan' => 'Makanan Asas seperti beras, gula, susu, biskut dll',
            'status' => 'Aktif',
            'created_by' => '1',
        ]);

        DB::table('categories')->insert([
            'nama' => 'Komputer',
            'keterangan' => 'bantuan komputer',
            'status' => 'Aktif',
            'created_by' => '1',
        ]);

        DB::table('categories')->insert([
            'nama' => 'Baiki Rumah',
            'keterangan' => 'Membaiki rumah',
            'status' => 'Aktif',
            'created_by' => '1',
        ]);
    }
}
