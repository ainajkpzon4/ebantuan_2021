<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dms')->insert([
            'nama' => 'Gombak Utara',
            'keterangan' => 'Gombak Setia',
            'status' => 'Aktif',
            'created_by' => '1',
        ]);

        DB::table('dms')->insert([
            'nama' => 'Gombak Selatan',
            'keterangan' => 'Gombak Setia',
            'status' => 'Aktif',
            'created_by' => '1',
        ]);

        DB::table('dms')->insert([
            'nama' => 'SG 8',
            'keterangan' => 'Gombak Setia',
            'status' => 'Aktif',
            'created_by' => '1',
        ]);

        DB::table('dms')->insert([
            'nama' => 'SG 9',
            'keterangan' => 'Gombak Setia',
            'status' => 'Aktif',
            'created_by' => '1',
        ]);

        DB::table('dms')->insert([
            'nama' => 'Hillcreast',
            'keterangan' => 'Fasa 1/1-7',
            'status' => 'Aktif',
            'created_by' => '1',
        ]);
    }
}
