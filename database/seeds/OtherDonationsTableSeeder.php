<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class OtherDonationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('other_donations')->insert([
            'recepient_id' => '1',
            'tarikh_serahan' => Carbon::create('2020', '08', '01'),
            'category_id' => '2',
            'nama' => 'Bantuan laptop',
            'sebab' => 'Anak belajar',
            'status' => 'aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '04', '01'),
        ]);

        DB::table('other_donations')->insert([
            'recepient_id' => '1',
            'tarikh_serahan' => Carbon::create('2019', '05', '11'),
            'category_id' => '1',
            'nama' => 'Bantuan Wang RM500',
            'sebab' => 'Bantuan Asnaf',
            'status' => 'aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '01', '01'),

        ]);

        DB::table('other_donations')->insert([
            'recepient_id' => '2',
            'tarikh_serahan' => Carbon::create('2019', '02', '09'),
            'category_id' => '4',
            'nama' => 'Bantuan Wang RM500',
            'sebab' => 'Bantuan Asnaf',
            'status' => 'aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '01', '01'),
        ]);
    }
}
