<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(DunsTableSeeder::class);
        $this->call(DmsTableSeeder::class);
        $this->call(VillagesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(RefDonationsTableSeeder::class);
        $this->call(RefEducationsTableSeeder::class);
        $this->call(RecepientsTableSeeder::class);
        $this->call(ProfilesTableSeeder::class);
        $this->call(DependentsTableSeeder::class);
        $this->call(DonationsTableSeeder::class);
        $this->call(OtherDonationsTableSeeder::class);
        $this->call(DependentDonationsTableSeeder::class);
        $this->call(DependentOtherDonationsTableSeeder::class);
    }
}
