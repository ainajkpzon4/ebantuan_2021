<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon as SupportCarbon;
use Illuminate\Support\Facades\DB;

class RecepientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('recepients')->insert([
            'no_kp' => '201010100981',
            'pengundi' => '1',
            'nota' => 'test1',
            'status' => 'Aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '01', '01'),
        ]);

        DB::table('recepients')->insert([
            'no_kp' => '191010100981',
            'pengundi' => '0',
            'nota' => 'test 2',
            'status' => 'Aktif',
            'created_by' => '1',
            'created_at' => Carbon::create('2000', '01', '01'),
        ]);
    }
}
