<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RefEducationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ref_education')->insert([
            'nama' => 'Tiada',
            'peringkat' => 'Tiada',
            'keterangan' => '-Tiada-',
            'created_by' => '1',
            'created_at' => now(),
        ]);

        DB::table('ref_education')->insert([
            'nama' => 'Sekolah Rendah',
            'peringkat' => 'Sekolah Rendah ',
            'keterangan' => 'Sekolah Rendah',
            'created_by' => '1',
            'created_at' => now(),
        ]);

        DB::table('ref_education')->insert([
            'nama' => 'Sekolah Menengah',
            'peringkat' => 'Sekolah Menengah',
            'keterangan' => 'Sekolah Menengah',
            'created_by' => '1',
            'created_at' => now(),
        ]);

        DB::table('ref_education')->insert([
            'nama' => 'Sijil',
            'peringkat' => 'Sijil',
            'keterangan' => 'Sijil',
            'created_by' => '1',
            'created_at' => now(),
        ]);

        DB::table('ref_education')->insert([
            'nama' => 'Diploma',
            'peringkat' => 'Diploma',
            'keterangan' => 'Diploma',
            'created_by' => '1',
            'created_at' => now(),
        ]);

        DB::table('ref_education')->insert([
            'nama' => 'Ijazah',
            'peringkat' => 'Ijazah',
            'keterangan' => 'Ijazah',
            'created_by' => '1',
            'created_at' => now(),
        ]);

        DB::table('ref_education')->insert([
            'nama' => 'Sarjana',
            'peringkat' => 'Sarjana',
            'keterangan' => 'Sarjana',
            'created_by' => '1',
            'created_at' => now(),
        ]);

        DB::table('ref_education')->insert([
            'nama' => 'Ph.D',
            'peringkat' => 'Ph.D',
            'keterangan' => 'Ph.D',
            'created_by' => '1',
            'created_at' => now(),
        ]);
    }
}
