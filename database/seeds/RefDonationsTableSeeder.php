<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class RefDonationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ref_donations')->insert([
            'nama' => 'Tiada',
            'keterangan' => '-Tiada-',
            'created_by' => '1',
        ]);

        DB::table('ref_donations')->insert([

            'nama' => 'Biasiswa Selangor',
            'keterangan' => 'Biasiswa Selangor',
            'created_by' => '1',
        ]);

        DB::table('ref_donations')->insert([

            'nama' => 'Skim Peduli Sihat',
            'keterangan' => 'Skim Peduli Sihat',
            'created_by' => '1',
        ]);

        DB::table('ref_donations')->insert([

            'nama' => 'Skim Rumah Selangorku',
            'keterangan' => 'Skim Rumah Selangorku',
            'created_by' => '1',
        ]);

        DB::table('ref_donations')->insert([

            'nama' => 'Wifi Smart Selangor',
            'keterangan' => 'Wifi Smart Selangor',
            'created_by' => '1',
        ]);

        DB::table('ref_donations')->insert([

            'nama' => 'Bantuan Tadika Selangor',
            'keterangan' => 'Bantuan Tadika Selangor',
            'created_by' => '1',
        ]);

        DB::table('ref_donations')->insert([

            'nama' => 'Skim Asuh Pintar',
            'keterangan' => 'Skim Asuh Pintar',
            'created_by' => '1',
        ]);

        DB::table('ref_donations')->insert([

            'nama' => 'Skim Asuh Pintar',
            'keterangan' => 'Skim Asuh Pintar',
            'created_by' => '1',
        ]);

        DB::table('ref_donations')->insert([

            'nama' => 'Skim Hijrah Selangor',
            'keterangan' => 'Skim Hijrah Selangor',
            'created_by' => '1',
        ]);

        DB::table('ref_donations')->insert([

            'nama' => 'Bantuan Hadiah IPT Selangor',
            'keterangan' => 'Bantuan Hadiah IPT Selangor',
            'created_by' => '1',
        ]);

        DB::table('ref_donations')->insert([

            'nama' => 'Tabung Warisan Anak Selangor',
            'keterangan' => 'Tabung Warisan Anak Selangor',
            'created_by' => '1',
        ]);
    }
}
