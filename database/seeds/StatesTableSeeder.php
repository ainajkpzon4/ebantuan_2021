<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert([
            'nama' => 'Johor',
            'keterangan' => 'Johor',
            'status' => 'aktif',
            'created_by' => '1',
        ]);

        DB::table('states')->insert([
            'nama' => 'Kedah',
            'keterangan' => 'Kedah',
            'status' => 'aktif',
            'created_by' => '1',
        ]);

        DB::table('states')->insert([
            'nama' => 'Kelantan',
            'keterangan' => 'Kelantan',
            'status' => 'aktif',
            'created_by' => '1',
        ]);

        DB::table('states')->insert([
            'nama' => 'Melaka',
            'keterangan' => 'Melaka',
            'status' => 'aktif',
            'created_by' => '1',
        ]);

        DB::table('states')->insert([
            'nama' => 'Negeri Sembilan',
            'keterangan' => 'Negeri Sembilan',
            'status' => 'aktif',
            'created_by' => '1',
        ]);

        DB::table('states')->insert([
            'nama' => 'Perlis',
            'keterangan' => 'Perlis',
            'status' => 'aktif',
            'created_by' => '1',
        ]);

        DB::table('states')->insert([
            'nama' => 'Pahang',
            'keterangan' => 'Pahang',
            'status' => 'aktif',
            'created_by' => '1',
        ]);

        DB::table('states')->insert([
            'nama' => 'Perak',
            'keterangan' => 'Perak',
            'status' => 'aktif',
            'created_by' => '1',
        ]);

        DB::table('states')->insert([
            'nama' => 'Pulau Pinang',
            'keterangan' => 'Pulau Pinang',
            'status' => 'aktif',
            'created_by' => '1',
        ]);

        DB::table('states')->insert([
            'nama' => 'Sabah',
            'keterangan' => 'Sabah',
            'status' => 'aktif',
            'created_by' => '1',
        ]);

        DB::table('states')->insert([
            'nama' => 'Sarawak',
            'keterangan' => 'Sarawak',
            'status' => 'aktif',
            'created_by' => '1',
        ]);

        DB::table('states')->insert([
            'nama' => 'Selangor',
            'keterangan' => 'Selangor',
            'status' => 'aktif',
            'created_by' => '1',
        ]);

        DB::table('states')->insert([
            'nama' => 'Terengganu',
            'keterangan' => 'Terengganu',
            'status' => 'aktif',
            'created_by' => '1',
        ]);

        DB::table('states')->insert([
            'nama' => 'W.P Labuan',
            'keterangan' => 'Wilayah Persekutuan Labuan',
            'status' => 'aktif',
            'created_by' => '1',
        ]);

        DB::table('states')->insert([
            'nama' => 'W.P Kuala Lumpur',
            'keterangan' => 'Wilayah Persekutuan Kuala Lumpur',
            'status' => 'aktif',
            'created_by' => '1',
        ]);

        DB::table('states')->insert([
            'nama' => 'W.P Putrajaya',
            'keterangan' => 'Wilayah Persekutuan Putrajaya',
            'status' => 'aktif',
            'created_by' => '1',
        ]);
    }
}
