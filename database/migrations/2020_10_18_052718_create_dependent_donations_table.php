<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDependentDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dependent_donations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('recepient_id');
            $table->foreign('recepient_id')->references('id')->on('recepients');
            $table->unsignedBigInteger('dependent_id');
            $table->foreign('dependent_id')->references('id')->on('dependents');
            $table->unsignedBigInteger('ref_donation_id');
            $table->foreign('ref_donation_id')->references('id')->on('ref_donations');
            $table->date('tarikh_mohon');
            $table->date('tarikh_lulus')->nullable();
            $table->string('status_kelulusan');
            $table->date('tarikh_mula')->nullable();
            $table->date('tarikh_akhir')->nullable();
            $table->boolean('salinan_kp')->nullable();
            $table->boolean('kp1')->nullable();
            $table->boolean('kp2')->nullable();
            $table->boolean('slip_gaji')->nullable();
            $table->boolean('baucer')->nullable();
            $table->string('nota')->nullable();
            $table->string('status')->default('Aktif');
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dependent_donations');
    }
}
