<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('recepient_id');
            $table->foreign('recepient_id')->references('id')->on('recepients');
            $table->string('nama');
            $table->string('jantina');
            // $table->string('no_kp')->unique();
            $table->date('tarikh_lahir');
            $table->integer('umur');
            $table->string('bangsa', 20);         // Melayu/Bumiputera, Cina, India, Lain-lain
            $table->string('bangsa_lain', 20)->nullable();         // Melayu/Bumiputera, Cina, India, Lain-lain
            $table->string('warganegara', 30);   // Ya, Tidak
            $table->string('agama', 10);     // Islam, Buddha, Kristian, Hindu, Lain-lain
            $table->string('agama_lain', 10)->nullable();     // Islam, Buddha, Kristian, Hindu, Lain-lain
            $table->string('status_perkahwinan', 10);   // Berkahwin,Bujang,Pernah Berkahwin
            $table->string('emel')->nullable();   // Berkahwin,Bujang,Pernah Berkahwin
            $table->text('alamat');
            $table->string('bandar');
            $table->unsignedBigInteger('village_id');
            $table->foreign('village_id')->references('id')->on('villages');
            $table->string('poskod', 5);
            $table->string('negeri');
            $table->unsignedBigInteger('dun_id');
            $table->foreign('dun_id')->references('id')->on('duns')->nullable();
            $table->unsignedBigInteger('dm_id');
            $table->foreign('dm_id')->references('id')->on('dms');
            $table->string('no_telefon', 12);
            $table->string('kad_oku', 5)->nullable(); // Ya, Tidak
            $table->text('nota')->nullable(); // Ya, Tidak
            $table->string('status')->default('Aktif'); // Ya, Tidak
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
