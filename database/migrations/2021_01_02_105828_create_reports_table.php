<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('recepient_id');
            $table->foreign('recepient_id')->references('id')->on('recepients');
            $table->string('recepient_no_kp')->nullable();
            $table->string('recepient_nama')->nullable();
            $table->string('dependent_no_kp')->nullable();
            $table->string('dependent_nama')->nullable();
            $table->string('hubungan')->nullable();
            $table->string('alamat')->nullable();
            $table->string('no_telefon')->nullable();
            $table->string('baucer')->nullable();
            $table->string('kp1')->nullable();
            $table->string('kp2')->nullable();
            $table->string('slip_gaji')->nullable();
            $table->string('pengundi')->nullable();
            $table->string('catatan')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
