<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtherDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_donations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('recepient_id');
            $table->foreign('recepient_id')->references('id')->on('recepients');
            $table->unsignedBigInteger('dependent_id')->nullable();
            $table->foreign('dependent_id')->references('id')->on('dependents');
            $table->date('tarikh_serahan');
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->string('nama');
            $table->string('sebab')->nullable();
            $table->boolean('salinan_kp')->nullable();
            $table->boolean('kp1')->nullable();
            $table->boolean('kp2')->nullable();
            $table->boolean('slip_gaji')->nullable();
            $table->boolean('baucer')->nullable();
            $table->string('status')->default('Aktif');
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_donations');
    }
}
