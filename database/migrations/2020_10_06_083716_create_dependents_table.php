<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDependentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dependents', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('no_kp')->unique();
            $table->unsignedBigInteger('recepient_id');
            $table->foreign('recepient_id')->references('id')->on('recepients');
            $table->string('hubungan');
            $table->unsignedBigInteger('ref_education_id');
            $table->foreign('ref_education_id')->references('id')->on('ref_education');
            $table->string('jurusan')->nullable();
            $table->string('semesta')->nullable();
            $table->string('jantina');
            $table->string('umur')->nullable();
            $table->string('kad_oku')->nullable();
            $table->text('nota')->nullable();
            $table->string('status')->default('Aktif'); // Ya, Tidak
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dependents');
    }
}
