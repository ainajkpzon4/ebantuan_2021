<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DependentDonation extends Model
{
    public function dependent()
    {
        return $this->hasOne('App\Dependent');
    }

    public function refDonation()
    {
        return $this->belongsTo('App\refDonation');
    }

    public function recepient()
    {
        return $this->belongsTo('App\recepient');
    }
}
