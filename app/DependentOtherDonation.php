<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DependentOtherDonation extends Model
{
    public function dependent()
    {
        return $this->belongsTo('App\Dependent');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
