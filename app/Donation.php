<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    public function recepient()
    {
        return $this->belongsTo('App\Recepient');
    }

    public function dependent()
    {
        return $this->belongsTo('App\Dependent', 'dependent_id');
    }

    public function refDonation()
    {
        return $this->belongsTo('App\RefDonation');
    }
}
