<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recepient extends Model
{
    public function donations()
    {
        return $this->hasMany('App\Donation');
    }

    public function otherDonations()
    {
        return $this->hasMany('App\OtherDonation');
    }

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    // public function spouses()
    // {
    //     return $this->hasMany('App\Spouse');
    // }

    public function dependents()
    {
        return $this->hasMany('App\Dependent');
    }

    // public function spouseDonations()
    // {
    //     return $this->hasMany('App\SpouseDonation');
    // }
}
