<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OtherDonationCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'kategori' => 'required',
            'tarikh_serahan' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nama.required' => 'Maklumat Nama Bantuan wajib dipilih',
            'kategori.required' => 'Maklumat Kategori wajib dipilih',
            'tarikh_serahan.required' => 'Maklumat Tarikh Serahan wajib diisi',
        ];
    }
}
