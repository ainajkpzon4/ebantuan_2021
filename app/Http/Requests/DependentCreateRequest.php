<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DependentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'hubungan' => 'required',
            'jantina' => 'required',
            'pendidikan' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nama.required' => 'Maklumat Nama wajib diisi',
            'hubungan.required' => 'Maklumat Hubungan wajib diisi',
            'jantina.required' => 'Maklumat Jantina wajib diisi',
            'pendidikan.required' => 'Maklumat Pendidikan wajib diisi',
        ];
    }
}
