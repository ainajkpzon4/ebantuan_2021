<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DonationCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'donation' => 'required',
            'tarikh_mohon' => 'required',
            'tarikh_lulus' => 'required',
            'status_kelulusan' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'donation.required' => 'Maklumat Nama Bantuan wajib dipilih',
            'tarikh_mohon.required' => 'Maklumat Tarikh Mohon wajib diisi',
            'tarikh_lulus.required' => 'Maklumat Tarikh Kelulusan wajib diisi',
            'status_kelulusan.required' => 'Maklumat Status Kelulusan wajib diisi',
        ];
    }
}
