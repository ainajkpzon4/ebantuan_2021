<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecipientCreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nama' => 'required',
            'tarikh_lahir' => 'required',
            'jantina' => 'required',
            'agama' => 'required',
            'bangsa' => 'required',
            'status_perkahwinan' => 'required',
            'no_telefon' => 'required',
            'alamat' => 'required',
            'village' => 'required',
            'bandar' => 'required',
            'poskod' => 'required',
            'negeri' => 'required',
            'negara' => 'required',
            'dun' => 'required',
            'pengundi' => 'required',
            // 'dm' => 'required',
            'status' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nama.required' => 'Maklumat Nama wajib diisi',
            'tarikh_lahir.required' => 'Maklumat Tarikh Lahir wajib diisi',
            'jantina.required' => 'Maklumat Jantina wajib diisi',
            'agama.required' => 'Maklumat Agama wajib diisi',
            'bangsa.required' => 'Maklumat Bangsa wajib diisi',
            'status_perkahwinan.required' => 'Maklumat Status Perkahwinan wajib diisi',
            'no_telefon.required' => 'Maklumat No.Telefon wajib diisi',
            'alamat.required' => 'Maklumat Alamat wajib diisi',
            'village.required' => 'Maklumat Kampung wajib diisi',
            'bandar.required' => 'Maklumat Bandar wajib diisi',
            'poskod.required' => 'Maklumat poskod wajib diisi',
            'negeri.required' => 'Maklumat Negeri wajib diisi',
            'negara.required' => 'Maklumat Warga wajib diisi',
            'dm.required' => 'Maklumat DM wajib diisi',
            'pengundi.required' => 'Maklumat Pengundi wajib diisi',
            'status.required' => 'Maklumat Status wajib diisi',
        ];
    }
}
