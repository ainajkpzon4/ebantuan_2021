<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('profile.index', compact('users'));
    }

    public function create()
    {
        $roles = Role::all();
        return view('auth.register', compact('roles'));
    }

    public function register()
    {
        $roles = Role::all();
        return view('auth.register', compact('roles'));
    }

    public function store(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->no_kp = $request->no_kp;
        $user->email = $request->email;
        $user->role_id = $request->role;
        $user->password = bcrypt($request->password);
        $user->status = $request->status;
        // $user->created_by = Auth::user()->id;
        $user->created_at = now();
        $user->save();

        $msg = 'Maklumat pengguna telah disimpan.';

        return redirect('/user/profile')->with('successMessage', $msg);
    }

    public function show()
    {
        $userId =  Auth::id();
        $user = User::find($userId);

        return view('auth.view', compact('user'));
    }

    public function show_user($user)
    {

        $user = User::find($user);

        return view('auth.view', compact('user'));
    }

    public function edit(User $user)
    {
        $roles = Role::all();
        return view('auth.edit', compact('roles', 'user'));
    }

    public function update(Request $request, User $user)
    {
        $user->name = $request->name;
        $user->no_kp = $request->no_kp;
        $user->email = $request->email;
        $user->role_id = $request->role;
        // $user->password = bcrypt($request->password);
        $user->status = $request->status;
        // $user->created_by = Auth::user()->id;
        $user->updated_at = now();
        $user->save();

        $msg = 'Maklumat pengguna telah dikemaskini.';

        return back()->with('successMessage', $msg);
    }

    // Change Password
    public function changePassword()
    {
        // $userId =  Auth::id();
        // $user = User::find($userId);
        return view('auth.passwords.reset');
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);

        User::find(auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);

        dd('Password change successfully.');
    }

    public function destroy($user)
    {
        // return $user;
        // $user = User::where('id', $user)->first();
        User::find($user)->delete();

        $msg = 'Maklumat pengguna telah dipadam.';

        return back()->with('successMessage', $msg);
    }
}
