<?php

namespace App\Http\Controllers;

use App\Dependent;
use App\Donation;
use App\Http\Requests\DependentCreateRequest;
use App\OtherDonation;
use App\Recepient;
use App\RefDonation;
use App\RefEducation;
use Illuminate\Support\Facades\Auth;
use Str;

class DependentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($recepientId)
    {
        $recepient = Recepient::where('no_kp', $recepientId)->first();
        $dependentsCount = Dependent::where('recepient_id', $recepient->id)->count();

        $dependents = Dependent::where('recepient_id', $recepient->id)->get();
        $refEducations = RefEducation::where('status', 'Aktif')->get();

        if ($dependentsCount > 0) {
            return view('Tanggungan.index', compact('dependents', 'recepientId', 'recepient', 'refEducations'));
        } else {
            return redirect('/tanggungan/search/' . $recepientId);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search($recepientId)
    {
        $refDonations = RefDonation::where('status', 'Aktif')->get();

        $recepient = Recepient::where('no_kp', $recepientId)->first();
        $dependents = Dependent::where('recepient_id', $recepient->id)->get();

        // return $recepient->profile->nama;
        return view('Tanggungan.search', compact('recepientId', 'recepient', 'refDonations', 'dependents'));
    }

    public function create($dependentId, $recepientId)
    {
        $recepientCount = Recepient::where('no_kp', $dependentId)->count();
        $recepient = Recepient::where('no_kp', $recepientId)->first();
        $refEducations = RefEducation::where('status', 'Aktif')->get();

        $dependentCount = Dependent::where('no_kp', $dependentId)->count();

        $tahun_lahir = Str::substr($dependentId, 0, 2);
        $tahun_lahir = (int)$tahun_lahir;

        // if ($tahun_lahir >= 0 && $tahun_lahir <= 35)
        // {
        //     $umur = now()->year - 2000 + $tahun_lahir;
        // }

        // elseif ($tahun_lahir > 35 && $tahun_lahir <= 99)  $umur = now()->year - 1900 + $tahun_lahir;
        // else $umur = "";

        //Jantina
        $sex = Str::substr($dependentId, 11, 1);
        if ($sex % 2 == 0) $jantina = 'P';
        else $jantina = 'L';

        if ($recepientCount > 0 || $dependentCount > 0) {

            if ($recepientCount > 0) { //exist
                return back()->with('errorMessage', 'No. kad pengenalan telah wujud sebagai penerima');
            }

            if ($dependentCount > 0) { //exist
                return back()->with('errorMessage', 'No. kad pengenalan/MyKid (' . $dependentId . ') telah wujud sebagai tanggungan');
            }
        } else {
            return view('Tanggungan.create', compact('recepientId', 'dependentId', 'recepient', 'refEducations', 'jantina'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DependentCreateRequest $request, $dependentId, $recepientId)
    {
        // return $recepientId;
        $recepient = Recepient::where('no_kp', $recepientId)->first();

        // return $recepient;
        $dependent = new Dependent();
        $dependent->nama = $request->nama;
        $dependent->hubungan = $request->hubungan;
        $dependent->no_kp = $request->no_kp;
        $dependent->recepient_id = $recepient->id;
        $dependent->ref_education_id =  $request->pendidikan;
        $dependent->jurusan =  $request->jurusan;
        $dependent->semesta =  $request->semesta;
        $dependent->jantina = $request->jantina;
        $dependent->umur = $request->umur;
        $dependent->kad_oku = $request->kad_oku;
        $dependent->nota = $request->nota;
        $dependent->status = 'Aktif';
        $dependent->created_by = Auth::id();
        $dependent->updated_at = now();
        $dependent->save();

        // $report = Report::where('recepient_id', $recepient->id)->first();
        // if ($request->hubungan == 'Isteri') {
        //     $report->pasangan_no_kp = $request->nama . '(' . $request->no_kp . ')';
        // } else {
        //     $report->anak_nama_kp =  $request->nama . '('. $request->no_kp.')';
        // }
        // $report->created_at = now();
        // $report->save();


        return redirect('/tanggungan/' . $recepient->no_kp)->with('successMessage', 'Maklumat Tanggungan telah dikemaskini');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dependent  $dependent
     * @return \Illuminate\Http\Response
     */
    public function show(Dependent $dependent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dependent  $dependent
     * @return \Illuminate\Http\Response
     */
    public function edit(Dependent $dependent)
    {
        $recepientId = $dependent->recepient->no_kp;
        $recepient = Recepient::where('no_kp', $recepientId)->first();
        $refEducations = RefEducation::where('status', 'Aktif')->get();
        return view('Tanggungan.edit', compact('recepientId', 'recepient', 'dependent', 'refEducations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dependent  $dependent
     * @return \Illuminate\Http\Response
     */
    public function update(DependentCreateRequest $request, $dependent)
    {

        $dependentId = $dependent;
        $dependent = Dependent::where('id', $dependentId)->first();
        $dependent->nama = $request->nama;
        $dependent->hubungan = $request->hubungan;
        $dependent->no_kp = $request->no_kp;
        $dependent->recepient_id = $dependent->recepient->id;
        $dependent->ref_education_id =  $request->pendidikan;
        $dependent->jurusan =  $request->jurusan;
        $dependent->semesta =  $request->semesta;
        $dependent->jantina = $request->jantina;
        $dependent->umur = $request->umur;
        $dependent->kad_oku = $request->kad_oku;
        $dependent->nota = $request->nota;
        $dependent->status = $request->status;
        $dependent->updated_by = Auth::id();
        $dependent->updated_at = now();
        $dependent->save();

        return back()->with('successMessage', 'Maklumat Tanggungan telah berjaya dikemaskini');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dependent  $dependent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dependent $dependent)
    {
        $dependentDonation = Donation::where('dependent_id', $dependent->id)->first();
        $dependentOtherDonation = OtherDonation::where('dependent_id', $dependent->id)->first();

        if (!empty($dependentDonation) || !empty($dependentOtherDonation)) {

            $errmsg = 'Maklumat Tanggungan tidak boleh dipadam kerana terdapat maklumat bantuan.';
            return redirect('/tanggungan/' . $dependent->recepient->no_kp)->with('errorMessage', $errmsg);
        }
        Dependent::find($dependent->id)->delete();

        $sucmsg = 'Maklumat Tanggungan telah dipadam.';

        return redirect('/tanggungan/' . $dependent->recepient->no_kp)->with('successMessage', $sucmsg);
    }
}
