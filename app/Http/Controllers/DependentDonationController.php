<?php

namespace App\Http\Controllers;

use App\Recepient;
use App\DependentDonation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class DependentDonationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $recepientId)
    {
        $recepient = Recepient::where('no_kp', $recepientId)->first();
        $dependentDonation = new DependentDonation();
        $dependentDonation->recepient_id = $recepient->id;
        $dependentDonation->dependent_id = $request->dependentId;
        $dependentDonation->ref_donation_id = $request->donation;
        $dependentDonation->tarikh_mohon = $request->tarikh_mohon;
        $dependentDonation->tarikh_lulus = $request->tarikh_lulus;
        $dependentDonation->status_kelulusan = $request->status_kelulusan;
        $dependentDonation->tarikh_mula = $request->tarikh_mula;
        $dependentDonation->tarikh_akhir = $request->tarikh_akhir;
        $dependentDonation->salinan_kp = $request->salinan_kp;
        $dependentDonation->kp1 = $request->kp1;
        $dependentDonation->kp2 = $request->kp2;
        $dependentDonation->slip_gaji = $request->slip_gaji;
        $dependentDonation->nota = $request->nota;
        $dependentDonation->status = $request->status;
        $dependentDonation->created_by = Auth::id();
        $dependentDonation->created_at = now();
        $dependentDonation->status = $request->status;

        $dependentDonation->save();
        return back()->with('successMessage', 'Maklumat Bantuan telah berjaya disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DependentDonation  $dependentDonation
     * @return \Illuminate\Http\Response
     */
    public function show(DependentDonation $dependentDonation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DependentDonation  $dependentDonation
     * @return \Illuminate\Http\Response
     */
    public function edit(DependentDonation $dependentDonation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DependentDonation  $dependentDonation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $dependentDonation)
    {
        $dependentDonation = DependentDonation::where('id', $dependentDonation)->first();
        $dependentDonation->ref_donation_id = $request->donation;
        $dependentDonation->tarikh_mohon = $request->tarikh_mohon;
        $dependentDonation->tarikh_lulus = $request->tarikh_lulus;
        $dependentDonation->status_kelulusan = $request->status_kelulusan;
        $dependentDonation->tarikh_mula = $request->tarikh_mula;
        $dependentDonation->tarikh_akhir = $request->tarikh_akhir;
        $dependentDonation->salinan_kp = $request->salinan_kp;
        $dependentDonation->kp1 = $request->kp1;
        $dependentDonation->kp2 = $request->kp2;
        $dependentDonation->slip_gaji = $request->slip_gaji;
        $dependentDonation->nota = $request->nota;
        $dependentDonation->status = $request->status;
        $dependentDonation->created_by = Auth::id();
        $dependentDonation->created_at = now();
        $dependentDonation->status = $request->status;

        $dependentDonation->save();
        return back()->with('successMessage', 'Maklumat Bantuan telah berjaya dikemaskini');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DependentDonation  $dependentDonation
     * @return \Illuminate\Http\Response
     */
    public function destroy(DependentDonation $dependentDonation)
    {
        // return $dependentDonation->recepient->no_kp;

        DependentDonation::find($dependentDonation->id)->delete();

        $sucmsg = 'Maklumat bantuan tanggungan telah dipadam.';

        return redirect('/bantuan/' . $dependentDonation->recepient->no_kp)->with('successMessage', $sucmsg);
    }
}
