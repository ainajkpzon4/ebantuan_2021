<?php

namespace App\Http\Controllers;

use App\Category;
use App\OtherDonation;
use Illuminate\Http\Request;

class ReportKhasController extends Controller
{
    public function index()
    {
        // $refDonations = RefDonation::where('status', 'Aktif')->get();
        // return view('Laporan.index', compact('refDonations'));
    }

    public function create()
    {
        $catagories = Category::where('status', 'Aktif')->get();
        return view('laporan_khas.create', compact('catagories'));
    }

    public function store(Request $request)
    {
        $tahun = $request->tahun;
        $otherDonation = $request->donation;
        $all = $request->all;

        if ($all == 'on') {
            $otherDonation = '%';
        }
        $otherDonations = OtherDonation::join('recepients', 'recepients.id', '=', 'other_donations.recepient_id')
            ->where('other_donations.category_id', 'LIKE', $otherDonation)
            ->whereYear('other_donations.tarikh_serahan', $tahun)
            ->orderBy('recepients.no_kp', 'asc')
            ->groupBy('recepients.no_kp')
            ->get(['other_donations.*', 'recepients.*']);

        return view('laporan_khas.show', compact('otherDonations'));

        // return 'sini';
        // Report::truncate();

        // $donations = Donation::where('ref_donation_id', $request->donation)
        //     ->where('status', 'Aktif')
        //     ->get();

        // $data = [];

        // foreach ($donations as $donation) {
        //     $dependents = $donation->recepient->dependents;

        //     foreach ($dependents as $dependent) {
        //         $data[] = [
        //             'recepient_id'  => $donation->recepient_id,
        //             'recepient_no_kp'  => $donation->recepient->no_kp,
        //             'recepient_nama'  => $dependent->recepient->nama,
        //             'dependent_no_kp'  => $dependent->no_kp,
        //             'dependent_nama'  => $dependent->nama,
        //             'hubungan'  => $dependent->hubungan,
        //             'alamat'  => $dependent->recepient->profile->alamat . ', ' . $dependent->recepient->profile->village->nama . ', ' . $dependent->recepient->profile->bandar,
        //             'no_telefon'  => $dependent->recepient->profile->no_telefon,
        //             'kp1'  => $donation->kp1,
        //             'kp2'  => $donation->kp2,
        //             'slip_gaji'  => $donation->slip_gaji,
        //             'pengundi'  => $donation->recepient->pengundi

        //         ];
        //     }
        // $dataDependent = [];
        // foreach ($donation->recepient->dependents as $dependent) {
        //     $dataDependent[] = [
        //         'recepient_id'  => $donation->recepient_id,
        //         'pasangan_no_kp'  => $donation->recepient->dependent->no_kp,
        //         'pasangan_nama_kp'  => $donation->recepient_id
        //     ];
        // }
        // $dataDependent[] = [
        //     'recepient_id'  => $donation->recepient_id,
        // 'pasangan_no_kp'  => $donation->recepient->dependent->no_kp,
        // 'pasangan_nama_kp'  => $donation->recepient_id
        // ];
        // $dependents = $donation->recepient->dependents;
        // $report->recepient_id = $donation->recepient_id;
        // $report->recepient_id = $donation->recepient_id;
        // $report->recepient_id = $donation->recepient_id;
        // $report->save($data);

        // }
        // DB::table('reports')->insert($data);
        // return $dependents;
    }
}
