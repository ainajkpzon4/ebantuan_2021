<?php

namespace App\Http\Controllers;

use App\Dun;
use Illuminate\Http\Request;

class DunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dun  $dun
     * @return \Illuminate\Http\Response
     */
    public function show(Dun $dun)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dun  $dun
     * @return \Illuminate\Http\Response
     */
    public function edit(Dun $dun)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dun  $dun
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dun $dun)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dun  $dun
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dun $dun)
    {
        //
    }
}
