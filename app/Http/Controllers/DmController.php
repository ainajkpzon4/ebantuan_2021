<?php

namespace App\Http\Controllers;

use App\Dm;
use App\Http\Requests\ReferenceCreateRequest;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DmController extends Controller
{

    public function index()
    {
        $dms = Dm::all();
        return view('rujukan.dm.index', compact('dms'));
    }

    public function create()
    {
        return view('rujukan.dm.create');
    }


    public function store(ReferenceCreateRequest $request)
    {
        $reference_donation = new Dm();
        $reference_donation->id = $request->id;
        $reference_donation->nama = $request->nama;
        $reference_donation->keterangan = $request->keterangan;
        $reference_donation->status = $request->status;
        $reference_donation->created_by = Auth::user()->id;
        $reference_donation->created_at = now();
        $reference_donation->save();

        $msg = 'Data Rujukan DM telah disimpan.';

        return redirect('/rujukan/dm/show/' . $reference_donation->id)->with('successMessage', $msg);
    }

    public function show($dm)
    {
        $id = $dm;
        $dm = Dm::where('id', '=', $id)->first();
        return view('rujukan.dm.show', compact('dm'));
    }

    public function edit($dm)
    {
        $id = $dm;
        $dm = Dm::where('id', '=', $id)->first();
        return view('rujukan.dm.edit', compact('dm'));
    }

    public function update(ReferenceCreateRequest $request, Dm $dm)
    {
        $dm->nama = $request->nama;
        $dm->keterangan = $request->keterangan;
        $dm->status = $request->status;
        $dm->updated_by = Auth::user()->id;
        $dm->save();

        $msg = 'Data Rujukan DM telah dikemaskini.';

        return back()->with('successMessage', $msg);
    }


    public function destroy($dm)
    {
        try {
            $refDonation = Dm::where('id', $dm)->first();
            $refDonation->delete();
            $queryStatus = "Successful";
        } catch (Exception $e) {
            $queryStatus = "Not success";
        }

        if ($queryStatus == 'Successful') {
            $msg = 'Maklumat DM telah dipadam.';
            return redirect('/rujukan/dm/')->with('successMessage', $msg);
        } else {
            $msg = 'Maklumat DM tidak berjaya dipadam kerana terdapat data yang menggunakan nama bantuan ini.';
            return redirect('/rujukan/dm/')->with('errorMessage', $msg);
        }
    }
}
