<?php

namespace App\Http\Controllers;

use App\RefEducation;
use Illuminate\Http\Request;

class RefEducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RefEducation  $refEducation
     * @return \Illuminate\Http\Response
     */
    public function show(RefEducation $refEducation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RefEducation  $refEducation
     * @return \Illuminate\Http\Response
     */
    public function edit(RefEducation $refEducation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RefEducation  $refEducation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RefEducation $refEducation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RefEducation  $refEducation
     * @return \Illuminate\Http\Response
     */
    public function destroy(RefEducation $refEducation)
    {
        //
    }
}
