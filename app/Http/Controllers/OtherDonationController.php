<?php

namespace App\Http\Controllers;

use App\Category;
use App\Dependent;
use App\Http\Requests\OtherDonationCreateRequest;
use App\OtherDonation;
use App\Recepient;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class OtherDonationController extends Controller
{
    public function index($recepientId)
    {
        $categories = Category::where('status', 'Aktif')->get();
        $recepient = Recepient::where('no_kp', $recepientId)->first();
        $dependents = Dependent::where('recepient_id', $recepient->id)->get();
        $otherDonations = OtherDonation::where('recepient_id', $recepient->id)
            ->whereBetween('tarikh_serahan', [
                Carbon::now()->startOfYear(),
                Carbon::now()->endOfYear(),
            ])
            ->get();
        // return $otherDonations;
        return view('bantuan_khas.index', compact('otherDonations', 'recepientId', 'recepient', 'categories', 'dependents'));
    }

    public function carian(Request $request, $recepientId)
    {
        // $carian = (int)$request->tahun;

        $categories = Category::where('status', 'Aktif')->get();
        $recepient = Recepient::where('no_kp', $recepientId)->first();
        $dependents = Dependent::where('recepient_id', $recepient->id)->get();
        $otherDonations = OtherDonation::where('recepient_id', $recepient->id)
            ->whereYear('tarikh_serahan', $request->tahun)
            ->get();
        return view('bantuan_khas.index', compact('otherDonations', 'recepientId', 'recepient', 'categories', 'dependents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OtherDonationCreateRequest $request, $recepientId)
    {

        $recepient = Recepient::where('no_kp', $recepientId)->first();
        $otherDonation = new OtherDonation();
        $otherDonation->recepient_id = $recepient->id;
        $otherDonation->nama = $request->nama;
        $otherDonation->category_id = $request->kategori;
        $otherDonation->dependent_id = $request->dependent;
        $otherDonation->tarikh_serahan = $request->tarikh_serahan;
        $otherDonation->salinan_kp = $request->salinan_kp;
        $otherDonation->kp1 = $request->kp1;
        $otherDonation->kp2 = $request->kp2;
        $otherDonation->slip_gaji = $request->slip_gaji;
        $otherDonation->sebab = $request->sebab;
        $otherDonation->status = $request->status;
        $otherDonation->created_by = Auth::id();
        $otherDonation->created_at = now();
        $otherDonation->status = $request->status;

        $otherDonation->save();
        return back()->with('successMessage', 'Maklumat Bantuan Khas telah dikemaskini');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OtherDonation  $OtherDonation
     * @return \Illuminate\Http\Response
     */
    public function show(OtherDonation $OtherDonation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OtherDonation  $OtherDonation
     * @return \Illuminate\Http\Response
     */
    public function edit(OtherDonation $OtherDonation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OtherDonation  $OtherDonation
     * @return \Illuminate\Http\Response
     */
    public function update(OtherDonationCreateRequest $request, $OtherDonation)
    {
        $otherDonation = OtherDonation::where('id', $OtherDonation)->first();
        $otherDonation->nama = $request->nama;
        $otherDonation->category_id = $request->kategori;
        $otherDonation->dependent_id = $request->dependent;
        $otherDonation->tarikh_serahan = $request->tarikh_serahan;
        $otherDonation->salinan_kp = $request->salinan_kp;
        $otherDonation->kp1 = $request->kp1;
        $otherDonation->kp2 = $request->kp2;
        $otherDonation->slip_gaji = $request->slip_gaji;
        $otherDonation->sebab = $request->nota;
        $otherDonation->status = $request->status;
        $otherDonation->updated_by = Auth::id();
        $otherDonation->updated_at = now();
        $otherDonation->status = $request->status;

        $otherDonation->save();
        return back()->with('successMessage', 'Maklumat Bantuan Khas telah dikemaskini');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OtherDonation  $OtherDonation
     * @return \Illuminate\Http\Response
     */
    public function destroy($OtherDonation)
    {
        OtherDonation::find($OtherDonation)->delete();
        $msg = 'Maklumat Bantuan Khas telah dipadam.';
        return back()->with('successMessage', $msg);
    }
}
