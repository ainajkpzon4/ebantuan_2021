<?php

namespace App\Http\Controllers;

use App\Dm;
use App\Profile;
use App\Recepient;
use App\State;
use Carbon\Carbon as CarbonCarbon;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\Exists;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $recepients = Recepient::all();
        // // return $profiles;
        // return view('penerima.index', compact('recepients'));
    }

    // public function search($profile)
    // {
    //     dd('sini');
    //     $all_dm = Dm::all();
    //     $countProfile = Profile::where('no_kp', $profile)->count();
    //     $profile = Profile::where('no_kp', $profile)->first();
    //     if ($countProfile > 0) {
    //         // return redirect('/profile/edit', compact('no_kp'));
    //         return view('profile.edit', compact('profile', 'all_dm'));
    //     } else {
    //         $msg = 'Rekod tidak dijumpai';
    //         return redirect()->back()->with('errorMessage', $msg);
    //     }
    // }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $states = State::all();
        return view('penerima.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        // return $profile;
        return view('pemohon.peribadi.view', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        $adminId = Auth::id();
        $no_kp = $request->no_kp;
        $profile = Profile::where('no_kp', $no_kp)->first();

        $profile->nama = $request->nama;
        $profile->no_kp = $request->no_kp;
        $profile->tarikh_lahir = now();
        $profile->jantina = $request->jantina;
        $profile->bangsa = $request->bangsa;
        $profile->bangsa_lain = $request->bangsa;
        $profile->warganegara = $request->negara;
        $profile->agama = $request->agama;
        $profile->agama_lain = $request->agama;
        $profile->status_perkahwinan = $request->status_perkahwinan;
        $profile->emel = $request->emel;
        $profile->alamat = $request->alamat;
        $profile->bandar = $request->bandar;
        $profile->kampung = $request->kampung;
        $profile->poskod = $request->poskod;
        $profile->negeri = $request->negeri;
        $profile->dun_id = '1';
        $profile->dm_id = $request->dm;
        $profile->no_telefon = $request->no_telefon;
        $profile->kad_oku = $request->oku;
        $profile->nota = $request->nota;
        $profile->nota = $request->nota;
        $profile->status = $request->status;
        $profile->updated_by = $adminId;
        $profile->updated_at = now();

        $profile->save();

        return back()->with('successMessage', 'Berjaya Dikemaskini');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
