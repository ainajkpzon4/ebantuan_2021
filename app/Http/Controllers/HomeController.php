<?php

namespace App\Http\Controllers;

use App\Dependent;
use App\Profile;
use App\Recepient;
use App\Spouse;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index()
    {
        return view('home');
    }
    public function search(Request $request)
    {
        $search = $request->search;
        // return $search;

        $recepient = Recepient::where('no_kp', $search)->first();
        // $spouse = Spouse::where('no_kp', $search)->first();
        $dependent = Dependent::where('no_kp', $search)->first();

        // return $recepient->id;
        // return $recepient->id;
        if ($recepient === null) {

            // if ($spouse === null) {
            if ($dependent === null) {
                return back()->with('errorMessage', 'Rekod tidak dijumpai');
            } else {
                return redirect('/search/show/' . $search . '/' . $dependent->recepient->profile->id);
            }
            // } else {

            return redirect('/search/show/' . $search . '/' . $dependent->recepient->profile->id);
            // }
        } else {
            return redirect('/search/show/' . $search . '/' . $recepient->id);
        }

        // if ($recepient > 0) {
        //     return view('profile.edit', compact('profile'));
        // }
        // if ($spouse > 0) {
        //     return view('profile.edit', compact('profile'));
        // }
        // if ($dependent > 0) {
        //     return view('profile.edit', compact('profile'));
        // }

        // else {
        //     $msg = 'Rekod tidak dijumpai';
        //     return redirect()->back()->with('errorMessage', $msg);
        // }
    }

    public function show($search, Recepient $recepient)
    {
        return view('search.view', compact('recepient', 'search'));
    }
}
