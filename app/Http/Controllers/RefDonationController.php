<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReferenceCreateRequest;
use Illuminate\Support\Facades\Auth;
use App\RefDonation;
use Exception;
use Illuminate\Http\Request;

class RefDonationController extends Controller
{

    public function index()
    {
        $donations = RefDonation::all();
        return view('rujukan.bantuan.index', compact('donations'));
    }

    public function create()
    {
        return view('rujukan.bantuan.create');
    }

    public function store(ReferenceCreateRequest $request)
    {
        $reference_donation = new RefDonation();
        $reference_donation->id = $request->id;
        $reference_donation->nama = $request->nama;
        $reference_donation->keterangan = $request->keterangan;
        $reference_donation->status = $request->status;
        $reference_donation->created_by = Auth::user()->id;
        $reference_donation->created_at = now();
        $reference_donation->save();

        $msg = 'Data Rujukan (Bantuan IPR Selangor) telah disimpan.';

        return redirect('/rujukan/bantuan/show/' . $reference_donation->id)->with('successMessage', $msg);
    }

    public function show($refDonation)
    {
        $id = $refDonation;
        $refDonation = RefDonation::where('id', '=', $id)->first();
        return view('rujukan.bantuan.show', compact('refDonation'));
    }

    public function edit($refDonation)
    {
        $id = $refDonation;
        $refDonation = RefDonation::where('id', '=', $id)->first();
        return view('rujukan.bantuan.edit', compact('refDonation'));
    }

    public function update(ReferenceCreateRequest $request, RefDonation $refDonation)
    {
        $refDonation->nama = $request->nama;
        $refDonation->keterangan = $request->keterangan;
        $refDonation->status = $request->status;
        $refDonation->updated_by = Auth::user()->id;
        $refDonation->save();

        $msg = 'Data Rujukan (Bantuan IPR Selangor) telah dikemaskini.';

        return back()->with('successMessage', $msg);
    }

    public function destroy($refDonation)
    {
        try {
            $refDonation = RefDonation::where('id', $refDonation)->first();
            $refDonation->delete();
            $queryStatus = "Successful";
        } catch (Exception $e) {
            $queryStatus = "Not success";
        }

        if ($queryStatus == 'Successful') {
            $msg = 'Maklumat Bantuan IPR Selangor telah dipadam.';
            return redirect('/rujukan/bantuan/')->with('successMessage', $msg);
        } else {
            $msg = 'Maklumat Bantuan IPR Selangor tidak berjaya dipadam kerana terdapat data yang menggunakan nama bantuan ini.';
            return redirect('/rujukan/bantuan/')->with('errorMessage', $msg);
        }
    }
}
