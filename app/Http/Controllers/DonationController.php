<?php

namespace App\Http\Controllers;

use App\Dependent;
use App\Donation;
use App\Http\Requests\DonationCreateRequest;
use App\Profile;
use App\Recepient;
use App\RefDonation;
use App\SpouseDonation;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class DonationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($recepientId)
    {
        $recepient = Recepient::where('no_kp', $recepientId)->first();
        $donations = Donation::where('recepient_id', $recepient->id)
            ->whereBetween('tarikh_lulus', [
                Carbon::now()->startOfYear(),
                Carbon::now()->endOfYear(),
            ])
            ->get();
        // return $donations;
        $dependents = Dependent::where('recepient_id', $recepient->id)->get();
        $refDonations = RefDonation::where('status', 'Aktif')->get();


        if (!empty($recepient) > 0) {
            return view('bantuan.index', compact('donations', 'recepientId', 'recepient', 'refDonations', 'dependents'));
        } else {
            return redirect('/bantuan/create/' . $recepientId);
        }
    }

    public function carian(Request $request, $recepientId)
    {
        $recepient = Recepient::where('no_kp', $recepientId)->first();
        $donations = Donation::where('recepient_id', $recepient->id)
            ->whereYear('tarikh_lulus', $request->tahun)
            ->get();
        $dependents = Dependent::where('recepient_id', $recepient->id)->get();
        $refDonations = RefDonation::where('status', 'Aktif')->get();

        if (!empty($recepient) > 0) {
            return view('bantuan.index', compact('donations', 'recepientId', 'recepient', 'refDonations', 'dependents'));
        } else {
            return redirect('/bantuan/create/' . $recepientId);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DonationCreateRequest $request, $recepientId)
    {
        $recepient = Recepient::where('no_kp', $recepientId)->first();
        $donation = new Donation();
        $donation->recepient_id = $recepient->id;
        $donation->dependent_id = $request->dependent;
        $donation->ref_donation_id = $request->donation;
        $donation->tarikh_mohon = $request->tarikh_mohon;
        $donation->tarikh_lulus = $request->tarikh_lulus;
        $donation->status_kelulusan = $request->status_kelulusan;
        $donation->tarikh_mula = $request->tarikh_mula;
        $donation->tarikh_akhir = $request->tarikh_akhir;
        $donation->salinan_kp = $request->salinan_kp;
        $donation->kp1 = $request->kp1;
        $donation->kp2 = $request->kp2;
        $donation->slip_gaji = $request->slip_gaji;
        $donation->nota = $request->nota;
        $donation->status = $request->status;
        $donation->created_by = Auth::id();
        $donation->created_at = now();
        $donation->status = $request->status;

        $donation->save();
        return back()->with('successMessage', 'Maklumat Bantuan telah berjaya disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Donation  $donation
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        // return dd('list of donation');
        // return $profile->donations;
        return view('pemohon.bantuan.index', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Donation  $donation
     * @return \Illuminate\Http\Response
     */
    public function edit(Donation $donation)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Donation  $donation
     * @return \Illuminate\Http\Response
     */
    public function update(DonationCreateRequest $request, $donation)
    {
        $donation = Donation::where('id', $donation)->first();
        $donation->ref_donation_id = $request->donation;
        $donation->dependent_id = $request->dependent;
        $donation->tarikh_mohon = $request->tarikh_mohon;
        $donation->tarikh_lulus = $request->tarikh_lulus;
        $donation->status_kelulusan = $request->status_kelulusan;
        $donation->tarikh_mula = $request->tarikh_mula;
        $donation->tarikh_akhir = $request->tarikh_akhir;
        $donation->salinan_kp = $request->salinan_kp;
        $donation->kp1 = $request->kp1;
        $donation->kp2 = $request->kp2;
        $donation->slip_gaji = $request->slip_gaji;
        $donation->nota = $request->nota;
        $donation->status = $request->status;
        $donation->updated_by = Auth::id();
        $donation->updated_at = now();
        $donation->status = $request->status;

        $donation->save();
        return back()->with('successMessage', 'Maklumat Bantuan telah dikemaskini');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Donation  $donation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Donation $donation)
    {
        Donation::find($donation->id)->delete();

        $sucmsg = 'Maklumat bantuan penerima telah dipadam.';

        return redirect('/bantuan/' . $donation->recepient->no_kp)->with('successMessage', $sucmsg);
    }
}
