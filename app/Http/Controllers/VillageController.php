<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReferenceCreateRequest;
use App\Village;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VillageController extends Controller
{

    public function index()
    {
        $villages = Village::all();
        return view('rujukan.kampung.index', compact('villages'));
    }


    public function create()
    {
        return view('rujukan.kampung.create');
    }


    public function store(ReferenceCreateRequest $request)
    {
        $village = new Village();
        $village->id = $request->id;
        $village->nama = $request->nama;
        $village->keterangan = $request->keterangan;
        $village->status = $request->status;
        $village->created_by = Auth::user()->id;
        $village->created_at = now();
        $village->save();

        $msg = 'Data Rujukan Kampung telah disimpan.';

        return redirect('/rujukan/village/show/' . $village->id)->with('successMessage', $msg);
    }


    public function show($village)
    {
        $id = $village;
        $village = Village::where('id', '=', $village)->first();
        return view('rujukan.kampung.show', compact('village'));
    }

    public function edit($village)
    {
        $id = $village;
        $village = Village::where('id', '=', $village)->first();
        return view('rujukan.kampung.edit', compact('village'));
    }


    public function update(ReferenceCreateRequest $request, Village $village)
    {
        $village->nama = $request->nama;
        $village->keterangan = $request->keterangan;
        $village->status = $request->status;
        $village->updated_by = Auth::user()->id;
        $village->save();

        $msg = 'Data Rujukan Kampung telah dikemaskini.';

        return back()->with('successMessage', $msg);
    }

    public function destroy($village)
    {
        try {
            $village = Village::where('id', $village)->first();
            $village->delete();
            $queryStatus = "Successful";
        } catch (Exception $e) {
            $queryStatus = "Not success";
        }

        if ($queryStatus == 'Successful') {
            $msg = 'Maklumat Kampung telah dipadam.';
            return redirect('/rujukan/village/')->with('successMessage', $msg);
        } else {
            $msg = 'Maklumat Kampung tidak berjaya dipadam kerana terdapat data yang menggunakan nama kampung ini.';
            return redirect('/rujukan/village/')->with('errorMessage', $msg);
        }
    }
}
