<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\ReferenceCreateRequest;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('rujukan.kategori.index', compact('categories'));
    }

    public function create()
    {
        return view('rujukan.kategori.create');
    }


    public function store(ReferenceCreateRequest $request)
    {
        $category = new Category();
        $category->id = $request->id;
        $category->nama = $request->nama;
        $category->keterangan = $request->keterangan;
        $category->status = $request->status;
        $category->created_by = Auth::user()->id;
        $category->created_at = now();
        $category->save();

        $msg = 'Data Rujukan Kategori telah disimpan.';

        return redirect('/rujukan/category/show/' . $category->id)->with('successMessage', $msg);
    }

    public function show($category)
    {
        $id = $category;
        $category = Category::where('id', '=', $id)->first();
        return view('rujukan.kategori.show', compact('category'));
    }

    public function edit($category)
    {
        $id = $category;
        $category = Category::where('id', '=', $id)->first();
        return view('rujukan.kategori.edit', compact('category'));
    }

    public function update(ReferenceCreateRequest $request, Category $category)
    {
        $category->nama = $request->nama;
        $category->keterangan = $request->keterangan;
        $category->status = $request->status;
        $category->updated_by = Auth::user()->id;
        $category->save();

        $msg = 'Data Rujukan Kategori telah dikemaskini.';

        return back()->with('successMessage', $msg);
    }


    public function destroy($category)
    {
        try {
            $refCategory = Category::where('id', $category)->first();
            $refCategory->delete();
            $queryStatus = "Successful";
        } catch (Exception $e) {
            $queryStatus = "Not success";
        }

        if ($queryStatus == 'Successful') {
            $msg = 'Maklumat Kategori telah dipadam.';
            return redirect('/rujukan/category/')->with('successMessage', $msg);
        } else {
            $msg = 'Maklumat Kategori tidak berjaya dipadam kerana terdapat data yang menggunakan nama bantuan ini.';
            return redirect('/rujukan/category/')->with('errorMessage', $msg);
        }
    }
}
