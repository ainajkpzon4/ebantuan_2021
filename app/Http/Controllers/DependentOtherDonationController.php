<?php

namespace App\Http\Controllers;

use App\DependentOtherDonation;
use App\Http\Requests\OtherDonationCreateRequest;
use App\Recepient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DependentOtherDonationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OtherDonationCreateRequest $request, $recepientId)
    {
        $recepient = Recepient::where('no_kp', $recepientId)->first();
        $dependentOtherDonation = new DependentOtherDonation();
        $dependentOtherDonation->recepient_id = $recepient->id;
        $dependentOtherDonation->dependent_id = $request->dependentId;
        $dependentOtherDonation->nama = $request->nama;
        $dependentOtherDonation->category_id = $request->kategori;
        $dependentOtherDonation->tarikh_serahan = $request->tarikh_serahan;
        $dependentOtherDonation->salinan_kp = $request->salinan_kp;
        $dependentOtherDonation->kp1 = $request->kp1;
        $dependentOtherDonation->kp2 = $request->kp2;
        $dependentOtherDonation->slip_gaji = $request->slip_gaji;
        $dependentOtherDonation->sebab = $request->sebab;
        $dependentOtherDonation->status = $request->status;
        $dependentOtherDonation->created_by = Auth::id();
        $dependentOtherDonation->created_at = now();
        $dependentOtherDonation->status = $request->status;

        $dependentOtherDonation->save();
        return back()->with('successMessage', 'Maklumat Bantuan Khas (Tanggungan) telah dikemaskini');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DependentOtherDonation  $dependentOtherDonation
     * @return \Illuminate\Http\Response
     */
    public function show(DependentOtherDonation $dependentOtherDonation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DependentOtherDonation  $dependentOtherDonation
     * @return \Illuminate\Http\Response
     */
    public function edit(DependentOtherDonation $dependentOtherDonation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DependentOtherDonation  $dependentOtherDonation
     * @return \Illuminate\Http\Response
     */
    public function update(OtherDonationCreateRequest $request, $dependentOtherDonation)
    {
        $dependentOtherDonation = DependentOtherDonation::where('id', $dependentOtherDonation)->first();
        $dependentOtherDonation->nama = $request->nama;
        $dependentOtherDonation->category_id = $request->kategori;
        $dependentOtherDonation->tarikh_serahan = $request->tarikh_serahan;
        $dependentOtherDonation->salinan_kp = $request->salinan_kp;
        $dependentOtherDonation->kp1 = $request->kp1;
        $dependentOtherDonation->kp2 = $request->kp2;
        $dependentOtherDonation->slip_gaji = $request->slip_gaji;
        $dependentOtherDonation->sebab = $request->nota;
        $dependentOtherDonation->status = $request->status;
        $dependentOtherDonation->updated_by = Auth::id();
        $dependentOtherDonation->updated_at = now();
        $dependentOtherDonation->status = $request->status;

        $dependentOtherDonation->save();
        return back()->with('successMessage', 'Maklumat Bantuan Khas telah dikemaskini');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DependentOtherDonation  $dependentOtherDonation
     * @return \Illuminate\Http\Response
     */
    public function destroy($dependentOtherDonation)
    {
        DependentOtherDonation::find($dependentOtherDonation)->delete();
        $msg = 'Maklumat Bantuan Khas (Tanggungan) telah dipadam.';
        return back()->with('successMessage', $msg);
    }
}
