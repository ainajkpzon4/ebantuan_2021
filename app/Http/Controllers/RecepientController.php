<?php

namespace App\Http\Controllers;

use App\Dependent;
use App\DependentDonation;
use App\DependentOtherDonation;
use App\Dm;
use App\Donation;
use App\Http\Requests\RecipientCreateRequest;
use App\OtherDonation;
use App\Profile;
use App\Recepient;
use App\Report;
use App\Spouse;
use App\Village;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Str;
use Stringable;

class RecepientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status)
    {
        if ($status == '1') $status = "Aktif";
        elseif ($status == '0') $status = "Tidak Aktif";
        $recepients = Recepient::where('status', $status)->get();

        return view('penerima.index', compact('recepients'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($recepientId)
    {
        $villages = Village::where('status', 'Aktif')->get();
        $dms = Dm::where('status', 'Aktif')->get();

        $recepientCount = Recepient::where('no_kp', $recepientId)
            ->where('status', 'Aktif')
            ->count();
        if ($recepientCount > 0) { //exist
            return back()->with('errorMessage', 'No. Kad Pengenalan aktif sebagai Penerima');
        }

        $dependentCount = Dependent::where('no_kp', $recepientId)
            ->where('status', 'Aktif')
            ->count();
        if ($dependentCount > 0) { //exist
            return back()->with('errorMessage', 'No. Kad Pengenalan aktif sebagai Tanggungan');
        }

        $tahun_lahir_string = Str::substr($recepientId, 0, 2);
        $bulan_lahir_string = Str::substr($recepientId, 2, 2);
        $hari_lahir_string = Str::substr($recepientId, 4, 2);

        $tahun_lahir = (int)$tahun_lahir_string;
        $bulan_lahir = (int)$bulan_lahir_string;
        $hari_lahir = (int)$hari_lahir_string;

        if ($bulan_lahir > 12 || $hari_lahir > 31) {
            return back()->with('errorMessage', 'Sila Masukkan No. Kad Pengenalan yang betul');
        }

        if ($tahun_lahir >= 0 && $tahun_lahir <= 35) $tahun_lahir = '20' . $tahun_lahir_string;
        elseif ($tahun_lahir > 35 && $tahun_lahir <= 99)  $tahun_lahir = '19' . $tahun_lahir_string;
        else $tahun_lahir = "";


        //Jantina
        $sex = Str::substr($recepientId, 11, 1);
        if ($sex % 2 == 0) $jantina = 'P';
        else $jantina = 'L';

        $tarikh_lahir = strtotime($tahun_lahir . '/' . $bulan_lahir_string . '/' . $hari_lahir_string);
        $tarikh_lahir = date('Y-m-d', $tarikh_lahir);


        // return $tarikh_lahir;
        return view('penerima.create', compact('villages', 'dms', 'recepientId', 'tarikh_lahir', 'jantina'));
    }

    public function search()
    {
        return view('penerima.search');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RecipientCreateRequest $request, $no_kp)
    {
        $recepient = new Recepient();

        $recepient->no_kp = $no_kp;
        $recepient->pengundi = $request->pengundi;
        $recepient->status = 'Aktif';
        $recepient->created_by = Auth::id();
        $recepient->created_at = now();
        $recepient->save();
        $recepientId = $recepient->id;

        // return $recepientId;
        $profile = new Profile();
        $profile->recepient_id = $recepientId;
        $profile->nama = $request->nama;
        $profile->jantina = $request->jantina;
        $profile->tarikh_lahir = $request->tarikh_lahir;
        // $profile->tarikh_lahir = now();
        $profile->bangsa = $request->bangsa;
        // $profile->bangsa_lain = $request->bangsa;
        $profile->warganegara = 'MAL';
        $profile->agama = $request->agama;
        // $profile->agama_lain = $request->agama;
        $profile->status_perkahwinan = $request->status_perkahwinan;
        $profile->emel = $request->emel;
        $profile->alamat = $request->alamat;
        $profile->bandar = $request->bandar;
        $profile->village_id = $request->village;
        $profile->poskod = $request->poskod;
        $profile->negeri = $request->negeri;
        $profile->dun_id = '1';
        $profile->dm_id = $request->dm;
        $profile->no_telefon = $request->no_telefon;
        $profile->kad_oku = $request->kad_oku;
        $profile->nota = $request->nota;
        $profile->created_by = Auth::id();
        $profile->status = 'Aktif';
        $profile->created_at = now();
        $profile->save();



        return redirect('penerima/edit/' . $request->no_kp)->with('successMessage', 'Maklumat Penerima berjaya di simpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recepient  $recepient
     * @return \Illuminate\Http\Response
     */
    public function show($recepientId)
    {
        // return $recepientId;
        $recepient = Recepient::where('no_kp', $recepientId)->first();
        return view('penerima.view', compact('recepient'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recepient  $recepient
     * @return \Illuminate\Http\Response
     */
    public function edit($recepientId)
    {
        $recepient = Recepient::where('no_kp', $recepientId)->first();

        $villages = Village::where('status', 'Aktif')->get();
        $dms = Dm::where('status', 'Aktif')->get();
        return view('penerima.edit', compact('recepientId', 'recepient', 'dms', 'villages'));
    }

    /**
     * Update the specified resource in storage.
     *
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recepient  $recepient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $recepientId)
    {
        $recepient = Recepient::where('no_kp', $recepientId)->first();
        $profile = Profile::where('recepient_id', $recepient->id)->first();

        $profile->nama = $request->nama;
        $profile->jantina = $request->jantina;
        // $profile->tarikh_lahir = $request->tarikh_lahir;
        // $profile->tarikh_lahir = now();
        $profile->bangsa = $request->bangsa;
        // $profile->bangsa_lain = $request->bangsa;
        $profile->warganegara = 'MAL';
        $profile->agama = $request->agama;
        // $profile->agama_lain = $request->agama;
        $profile->status_perkahwinan = $request->status_perkahwinan;
        $profile->emel = $request->emel;
        $profile->alamat = $request->alamat;
        $profile->bandar = $request->bandar;
        $profile->village_id = $request->village;
        $profile->poskod = $request->poskod;
        $profile->negeri = $request->negeri;
        $profile->dun_id = '1';
        $profile->dm_id = $request->dm;
        $profile->no_telefon = $request->no_telefon;
        $profile->kad_oku = $request->kad_oku;
        $profile->nota = $request->nota;
        $profile->updated_by = Auth::id();
        $profile->updated_at = now();
        $recepient->status = $request->status;
        $profile->save();
        $recepient->save();
        return redirect('penerima/edit/' . $request->no_kp)->with('successMessage', 'Maklumat Penerima berjaya di kemaskini');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recepient  $recepient
     * @return \Illuminate\Http\Response
     */
    public function destroy($recepientId)
    {
        return $recepientId;
        $recepient = Recepient::where('no_kp', $recepientId)->first();

        // check first
        // Donation
        $donation = Donation::where('recepient_id', $recepient->id)->count();
        if ($donation > 0) {
            $delete = 'no';
            $msg = 'Bantuan IPR,';
        }
        // OtherDonation
        $otherDonation = OtherDonation::where('recepient_id', $recepient->id)->count();
        if ($otherDonation > 0) {
            $delete = 'no';
            if (empty($msg)) $msg = ' Bantuan Khas,';
            else $msg .= ' Bantuan Khas,';
        }

        // Dependent
        $dependent = Dependent::where('recepient_id', $recepient->id)->count();
        if ($dependent > 0) {
            $delete = 'no';
            if (empty($msg)) $msg = ' Tanggungan,';
            else $msg .= ' Tanggungan,';
        }
        // DependentDonation
        $dependentDonation = DependentDonation::where('recepient_id', $recepient->id)->count();
        if ($dependentDonation > 0) {
            $delete = 'no';
            if (empty($msg)) $msg = ' Bantuan IPR Tanggungan,';
            else $msg .= ' Bantuan IPR Tanggungan,';
        }
        // DependentOtherDonation
        $dependentOtherDonation = DependentOtherDonation::where('recepient_id', $recepient->id)->count();
        if ($dependentOtherDonation > 0) {
            $delete = 'no';
            if (empty($msg)) $msg = ' Bantuan Khas Tanggungan,';
            else $msg .= ' Bantuan Khas Tanggungan,';
        }

        if ($delete == 'no') {
            // $errorMessage = 'Maklumat Penerima tidak boleh di padam kerana terdapat maklumat ' . $msg;
            $errorMessage = 'Maklumat Penerima tidak berjaya di padam kerana terdapat maklumat-maklumat Bantuan / Tanggungan.';
            return back()->with('errorMessage', $errorMessage);
        } else {
            $recepient = Recepient::where('no_kp', $recepientId)->first();
            Recepient::find($recepient->id)->delete();
        }
        $msg = 'Maklumat penerima telah dipadam.';

        return back()->with('errorMessage', $msg);
    }
}
