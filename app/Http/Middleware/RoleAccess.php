<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class RoleAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = Auth::id();
        $user = User::where('id', $userId)->first();

        if ($user->role_id == '3' || $user->role_id == '4') {
            return back()->with('errorMessage', 'Anda tiada akses fungsi ini');
        }

        return $next($request);
    }
}
