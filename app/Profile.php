<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public function recepient()
    {
        return $this->belongsTo('App\Recepient', 'recepient_id');
    }

    public function donations()
    {
        return $this->hasMany('App\Donation');
    }

    public function state()
    {
        return $this->belongsTo('App\State', 'negeri_id');
    }

    public function dun()
    {
        return $this->belongsTo('App\Dun');
    }

    public function dm()
    {
        return $this->belongsTo('App\Dm');
    }

    public function village()
    {
        return $this->belongsTo('App\Village');
    }

    public function spouses()
    {
        return $this->hasMany('App\Spouse');
    }

    public function dependents()
    {
        return $this->hasMany('App\Dependents');
    }
}
