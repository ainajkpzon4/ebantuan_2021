<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherDonation extends Model
{
    public function recepient()
    {
        return $this->belongsTo('App\Recepient');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function dependent()
    {
        return $this->belongsTo('App\Dependent', 'dependent_id');
    }
}
