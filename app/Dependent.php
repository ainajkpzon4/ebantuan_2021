<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dependent extends Model
{
    public function recepient()
    {
        return $this->belongsTo('App\Recepient', 'recepient_id');
    }

    public function dependentDonations()
    {
        return $this->hasMany('App\DependentDonation');
    }

    public function dependentOtherDonations()
    {
        return $this->hasMany('App\DependentOtherDonation');
    }

    public function refEducation()
    {
        return $this->belongsTo('App\RefEducation');
    }
}
