<?php

use App\Http\Middleware\RoleAccess;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::view('/', 'auth.login');

// Route::view('/', 'auth.login');

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(); // login, logout, register, forgot password

// Route::get('/home', 'HomeController@index')->name('home');



Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home', 'HomeController@search');
Route::get('/search/show/{search}/{recepient}', 'HomeController@show');

Route::middleware([RoleAccess::class])->group(function () {
    Route::get('/user/profile/', 'UserController@index');
    Route::get('/user/register/', 'UserController@create');
    Route::post('/user/register/', 'UserController@store');

    Route::get('/user/profile/show_user/{user}', 'UserController@show_user');
    Route::get('/user/profile/edit/{user}', 'UserController@edit');
    Route::post('/user/profile/edit/{user}', 'UserController@update');
    Route::get('/user/destroy/{user}', 'UserController@destroy');
});
Route::get('/user/profile/show/{user}', 'UserController@show');

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/change-password', 'ChangePasswordController@index');
Route::post('/change-password', 'ChangePasswordController@store')->name('change.password');

// penerima

Route::get('/penerima/status/{status}', 'RecepientController@index');

// Route::get('/penerima/search/', 'RecepientController@search');
// Route::post('/penerima/add', 'RecepientController@addStore');
Route::get('/penerima/search', 'RecepientController@search');
Route::get('/penerima/create/{recepientId}', 'RecepientController@create');
// Route::get('/penerima/search/{nokp}', 'RecepientController@searchBeforeCreate');
Route::post('/penerima/create/{recepientId}', 'RecepientController@store');

Route::get('/penerima/edit/{recepientId}', 'RecepientController@edit');
Route::post('/penerima/edit/{recepientId}', 'RecepientController@update');
Route::get('/penerima/show/{recepientId}', 'RecepientController@show');
Route::get('/penerima/destroy/{recepientId}', 'RecepientController@destroy')->middleware(RoleAccess::class);


Route::get('/tanggungan/{recepientId}', 'DependentController@index');
// Route::get('/tanggungan/semakan/{dependentId}/{recepientId}', 'DependentController@checkId');
Route::get('/tanggungan/search/{recepientId}', 'DependentController@search');
Route::get('/tanggungan/create/{recepientId}/{dependentId}', 'DependentController@create');
Route::post('/tanggungan/create/{recepientId}/{dependentId}', 'DependentController@store');
Route::get('/tanggungan/edit/{dependent}', 'DependentController@edit');
Route::post('/tanggungan/edit/{recepientId}', 'DependentController@update');
// Route::get('/tanggungan/show/{dependent}', 'DependentController@show');
Route::delete('/tanggungan/destroy/{dependent}', 'DependentController@destroy')->middleware(RoleAccess::class);

Route::get('/bantuan/{recepientId}', 'DonationController@index');
Route::post('/bantuan/{recepientId}', 'DonationController@carian');
Route::get('/bantuan/create/{recepientId}', 'DonationController@create');
Route::post('/bantuan/create/{recepientId}', 'DonationController@store');
Route::get('/bantuan/edit/{donation}', 'DonationController@edit');
Route::post('/bantuan/edit/{recepientId}', 'DonationController@update');
// Route::get('/tanggungan/show/{dependent}', 'DependentController@show');
Route::delete('/bantuan/destroy/{donation}', 'DonationController@destroy')->middleware(RoleAccess::class);

Route::get('/bantuan/tanggungan/{recepientId}', 'DependentDonationController@index');
Route::get('/bantuan/tanggungan/create/{recepientId}', 'DependentDonationController@create');
Route::post('/bantuan/tanggungan/create/{recepientId}', 'DependentDonationController@store');
Route::get('/bantuan/tanggungan/edit/{recepientId}', 'DependentDonationController@edit');
Route::post('/bantuan/tanggungan/edit/{dependentDonation}', 'DependentDonationController@update');
// Route::get('/tanggungan/show/{dependent}', 'DependentController@show');
Route::delete('/bantuan/tanggungan/destroy/{dependentDonation}', 'DependentDonationController@destroy')->middleware(RoleAccess::class);


Route::post('/bantuan_khas/create/{otherDonation}', 'OtherDonationController@store');
Route::post('/bantuan_khas/edit/{otherDonation}', 'OtherDonationController@update');
Route::delete('/bantuan_khas/destroy/{otherDonation}', 'OtherDonationController@destroy');

Route::get('/bantuan_khas/{recepientId}', 'OtherDonationController@index');
Route::post('/bantuan_khas/{recepientId}', 'OtherDonationController@carian');
Route::get('/bantuan_khas/tanggungan/create/{recepientId}', 'DependentOtherDonationController@create');
Route::post('/bantuan_khas/tanggungan/create/{recepientId}', 'DependentOtherDonationController@store');
Route::get('/bantuan_khas/edit/tanggungan/{recepientId}', 'DependentOtherDonationController@edit');
Route::post('/bantuan_khas/edit/tanggungan/{dependentotherDonation}', 'DependentOtherDonationController@update');
// Route::get('/tanggungan/show/{dependent}', 'DependentController@show');
Route::delete('/bantuan_khas/tanggungan/destroy/{dependentotherDonation}', 'DependentOtherDonationController@destroy')->middleware(RoleAccess::class);


// //DATA RUJUKAN
// // 1. BANTUAN
Route::get('/rujukan/bantuan', 'RefDonationController@index');
Route::get('/rujukan/bantuan/create', 'RefDonationController@create')->middleware(RoleAccess::class);
Route::post('/rujukan/bantuan/create', 'RefDonationController@store')->middleware(RoleAccess::class);
Route::get('/rujukan/bantuan/edit/{refDonation}', 'RefDonationController@edit')->middleware(RoleAccess::class);
Route::post('/rujukan/bantuan/edit/{refDonation}', 'RefDonationController@update')->middleware(RoleAccess::class);
Route::get('/rujukan/bantuan/show/{refDonation}', 'RefDonationController@show');
Route::get('/rujukan/bantuan/destroy/{refDonation}', 'RefDonationController@destroy')->middleware(RoleAccess::class);

// // 2. DM
Route::get('/rujukan/dm', 'DmController@index');
Route::get('/rujukan/dm/create', 'DmController@create')->middleware(RoleAccess::class);
Route::post('/rujukan/dm/create', 'DmController@store')->middleware(RoleAccess::class);
Route::get('/rujukan/dm/edit/{dm}', 'DmController@edit')->middleware(RoleAccess::class);
Route::post('/rujukan/dm/edit/{dm}', 'DmController@update')->middleware(RoleAccess::class);
Route::get('/rujukan/dm/show/{dm}', 'DmController@show');
Route::get('/rujukan/dm/destroy/{dm}', 'DmController@destroy')->middleware(RoleAccess::class);

// // 3. Village
Route::get('/rujukan/village', 'VillageController@index');
Route::get('/rujukan/village/create', 'VillageController@create')->middleware(RoleAccess::class);
Route::post('/rujukan/village/create', 'VillageController@store')->middleware(RoleAccess::class);
Route::get('/rujukan/village/edit/{village}', 'VillageController@edit')->middleware(RoleAccess::class);
Route::post('/rujukan/village/edit/{village}', 'VillageController@update')->middleware(RoleAccess::class);
Route::get('/rujukan/village/show/{village}', 'VillageController@show');
Route::get('/rujukan/village/destroy/{village}', 'VillageController@destroy')->middleware(RoleAccess::class);

// 4. Kategori
Route::get('/rujukan/category', 'CategoryController@index');
Route::get('/rujukan/category/create', 'CategoryController@create')->middleware(RoleAccess::class);
Route::post('/rujukan/category/create', 'CategoryController@store')->middleware(RoleAccess::class);
Route::get('/rujukan/category/edit/{category}', 'CategoryController@edit')->middleware(RoleAccess::class);
Route::post('/rujukan/category/edit/{category}', 'CategoryController@update')->middleware(RoleAccess::class);
Route::get('/rujukan/category/show/{category}', 'CategoryController@show');
Route::get('/rujukan/category/destroy/{category}', 'CategoryController@destroy')->middleware(RoleAccess::class);

// Laporan
Route::get('/laporan/bantuan', 'ReportController@index');
Route::get('/laporan/bantuan/create', 'ReportController@create');
Route::post('/laporan/bantuan/create', 'ReportController@store');
// Route::post('/laporan/bantuan', 'ReportController@store');

// Route::get('/laporan/bantuan', 'ReportController@edit');
// Route::post('/laporan/bantuan', 'ReportController@update');
// Route::get('/laporan/bantuan', 'ReportController@show');
// Route::get('/laporan/bantuan', 'ReportController@destroy');

Route::get('/laporan/bantuan-khas/create', 'ReportKhasController@create');
Route::post('/laporan/bantuan-khas/create', 'ReportKhasController@store');
