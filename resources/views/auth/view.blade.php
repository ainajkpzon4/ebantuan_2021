@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                {{-- <div class="card-header">{{ __('Profil Pengguna') }}</div> --}}

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-lg">

                            <thead class="bg-info-300">
                                <tr class="table-active">
                                    <th colspan="3">Paparan Profil</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td class="text-right">Nama :</td>
                                    <td>{{ $user->name }}</td>
                                </tr>

                                <tr>
                                    <td class="text-right">No.Pengenalan :</td>
                                    <td>{{ $user->no_kp }}</td>
                                </tr>

                                <tr>
                                    <td class="text-right">E-mel :</td>
                                    <td>{{ $user->email}}</td>
                                </tr>

                                <tr>
                                    <td class="text-right">Peranan :</td>
                                    <td>{{ $user->role->nama }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
