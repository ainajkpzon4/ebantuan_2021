@extends('layouts.backend')

@section('title')
    {{-- Urusetia --}}
@endsection

@section('top_button')
    {{-- <a href="/user/profile" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Pengguna</span></a> --}}
    <a href="/user/register" class="btn btn-link btn-float text-default"><i class="icon-plus-circle2 text-primary"></i> <span>Tambah Pengguna</span></a>
@endsection

@section('breadcrumb')
    <a href="/dashboard" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="/user/profile" class="breadcrumb-item">Senarai Pengguna sistem</a>
@endsection

@section('content')

    <div class="card">
        <div class="card-header bg-info header-elements-inline">
            <h5 class="card-title">Senarai Pengguna Sistem</h5>
        </div>

        <table class="table datatable-row-full">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Penuh</th>
                    <th>No.KP</th>
                    <th>E-mel</th>
                    <th>Peranan</th>
                    <th>Status</th>
                    <th class="text-center">Tindakan</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($users as $user)
                    <tr>
                    <td></td>
                    <td>{{ $user->name }}<a href=""></a></td>
                    <td>{{ $user->no_kp }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->role->nama }}</td>
                    <td><span class="badge badge-success">Active</span></td>
                    <td class="text-center">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="/user/profile/show_user/{{ $user->id }}" class="dropdown-item"><i class="icon-list"></i> Papar</a>
                                    <a href="/user/profile/edit/{{ $user->id }}" class="dropdown-item"><i class="icon-pencil"></i> Kemaskini</a>
                                    <a href="/user/destroy/{{ $user->id }}" class="dropdown-item delete"><i class="icon-trash"></i> Padam</a>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>

@endsection

@section('script')

    <script>
         $(".delete").on("click", function(){
            return confirm("Adakah anda pasti untuk menghapus rekod?");
        });
    </script>


@endsection
