@extends('layouts.backend')

{{-- @section('title')
    Urusetia
@endsection --}}

@section('top_button')
    <a href="/penerima" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Penerima</span></a>
    <a href="/penerima/create" class="btn btn-link btn-float text-default"><i class="icon-plus-circle2 text-primary"></i> <span>Tambah Penerima</span></a>
@endsection

@section('breadcrumb')
    <a href="/dashboard" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="/pemohon" class="breadcrumb-item">Senarai Penerima</a>
    {{-- <span class="breadcrumb-item active">Senarai Pemohon</span> --}}
@endsection

@section('content')

<div class="card">
    <div class="card-header header-elements-inline">
        <h6 class="card-title"><b>Maklumat Penerima</b></h6>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>

    <div class="card-body">
        <h6>
        <ul class="nav nav-tabs bg-primary nav-justified">
            <li class="nav-item"><a href="#peribadi" class="nav-link active" data-toggle="tab">Maklumat Peribadi</a></li>
            <li class="nav-item"><a href="#pasangan" class="nav-link" data-toggle="tab">Maklumat Pasangan</a></li>
            <li class="nav-item"><a href="#anak" class="nav-link" data-toggle="tab">Maklumat Anak</a></li>
            <li class="nav-item"><a href="#bantuan" class="nav-link" data-toggle="tab">Bantuan</a></li>
            <li class="nav-item"><a href="#bantuan_khas" class="nav-link" data-toggle="tab">Bantuan Khas</a></li>
        </ul>
        </h6>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="peribadi">
                <div class="card">


                    <div class="card-body">
                        <form action="#">
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="font-weight-semibold"><i class="icon-reading mr-2"></i> Maklumat Peribadi</legend>

                                        <div class="form-group">
                                            <label>Nama penerima:</label>
                                            <input type="text" name="nama" class="form-control" placeholder="Mohd Roslee bin Ahmad">
                                        </div>

                                        <div class="form-group">
                                            <label>Pilih jantina:</label>
                                            <select name="jantina" data-placeholder="Pilih jantina" class="form-control form-control-select2" data-fouc>
                                                <option></option>
                                                <option value="L">LELAKI</option>
                                                <option value="P">PEREMPUAN</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Pilih Agama:</label>
                                            <select name="jantina" data-placeholder="Pilih jantina" class="form-control form-control-select2" data-fouc>
                                                <option></option>
                                                <option value="I">ISLAM</option>
                                                <option value="B">BUDHA</option>
                                                <option value="H">HINDU</option>
                                                <option value="L">LAIN-LAIN</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Pilih Bangsa:</label>
                                            <select name="bangsa" data-placeholder="Pilih bangsa" class="form-control form-control-select2" data-fouc>
                                                <option></option>
                                                <option value="I">MELAYU</option>
                                                <option value="B">CINA</option>
                                                <option value="H">INDIA</option>
                                                <option value="L">LAIN-LAIN</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Nyatakan jika bangsa lain-lain:</label>
                                            <input type="text" name="bangsa_lain" class="form-control" placeholder="Bidayuh">
                                        </div>

                                        <div class="form-group">
                                            <label>Status Perkahwinan:</label>
                                            <select name="status_perkahwinan" data-placeholder="Pilih Status" class="form-control form-control-select2" data-fouc>
                                                <option></option>
                                                <option value="B">BUJANG</option>
                                                <option value="K">BERKAHWIN</option>
                                                <option value="P">PERNAH BERKAHWIN</option>
                                            </select>
                                        </div>

                                        {{-- <div class="form-group">
                                            <label>Salinan Kad Pengenalan:</label>
                                            <input type="file" class="form-input-styled" data-fouc>
                                        </div> --}}

                                        <div class="form-group">
                                            <label>Nota Tambahan:</label>
                                            <textarea rows="5" cols="5" class="form-control" placeholder="Masukkan nota tambahan anda di sini"></textarea>
                                        </div>
                                    </fieldset>
                                </div>

                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="font-weight-semibold"><i class="icon-home mr-2"></i> Maklumat Alamat & Kontak</legend>


                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>E-mel:</label>
                                                    <input type="text" name="emel" placeholder="mohd@gmail.com" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>No.Telefon:</label>
                                                    <input type="text" name="notel" placeholder="017-4521003" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>No. Rumah/Lot/Jalan:</label>
                                                    <input type="text" name="alamat" placeholder="No. 802, Jalan SG 1/0, Fasa 1" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>DM:</label>
                                                    <input type="text" name="alamat" placeholder="No. 128, Lorong Enggang" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Poskod:</label>
                                                    <input type="text" name="poskod" placeholder="68100" value="68100" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Bandar:</label>
                                                    <input type="text" name="bandar" placeholder="Batu Caves" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Negeri:</label>
                                                    <input type="text" name="negeri" placeholder="Selangor" value="Selangor" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Negara:</label>
                                                    <select data-placeholder="Pilih Negara" class="form-control form-control-select2" data-fouc>
                                                        <option></option>
                                                        <option value="Cambodia" selected>Malaysia</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>DUN:</label>
                                                    <select data-placeholder="Pilih DUN" class="form-control form-control-select2" data-fouc>
                                                        <option></option>
                                                        <option value="Cambodia" selected>Gombak Setia</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>


                                        {{-- <div class="form-group">
                                            <label>Additional message:</label>
                                            <textarea rows="5" cols="5" class="form-control" placeholder="Enter your message here"></textarea>
                                        </div> --}}
                                    </fieldset>
                                </div>
                            </div>

                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="pasangan">
                Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid laeggin.
            </div>

            <div class="tab-pane fade" id="anak">
                DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg whatever.
            </div>

            <div class="tab-pane fade" id="bantuan">
                Aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthet.
            </div>

            <div class="tab-pane fade" id="bantuan_khas">
                bantuan khas.
            </div>
        </div>
    </div>
</div>



@endsection
