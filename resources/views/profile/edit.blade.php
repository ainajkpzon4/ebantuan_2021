@extends('layouts.backend')

{{-- @section('title')
    Urusetia
@endsection --}}

@section('top_button')
    <a href="/penerima" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Penerima</span></a>
    <a href="/penerima/create" class="btn btn-link btn-float text-default"><i class="icon-plus-circle2 text-primary"></i> <span>Tambah Penerima</span></a>
@endsection

@section('breadcrumb')
    <a href="/dashboard" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="/pemohon" class="breadcrumb-item">Senarai Penerima</a>
    {{-- <span class="breadcrumb-item active">Senarai Pemohon</span> --}}
@endsection

@section('content')

<div class="card">
    <div class="card-header header-elements-inline">
        <h6 class="card-title"><b>Maklumat Penerima</b></h6>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div>

    <div class="card-body">
        <h6>
        <ul class="nav nav-tabs bg-primary nav-justified">
            <li class="nav-item"><a href="penerima" class="nav-link active" data-toggle="tab">Maklumat Peribadi</a></li>


        <li class="nav-item"><a href="/pasangan/{{$profile->no_kp }}" class="nav-link">Maklumat Pasangan</a></li>
            <li class="nav-item"><a href="tanggungan" class="nav-link" data-toggle="tab">Maklumat Anak</a></li>
            <li class="nav-item"><a href="bantuan" class="nav-link" data-toggle="tab">Bantuan</a></li>
            <li class="nav-item"><a href="bantuan_khas" class="nav-link" data-toggle="tab">Bantuan Khas</a></li>
        </ul>
        </h6>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="peribadi">
                <div class="card">


                    <div class="card-body">
                        <form action="/profile/edit" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="font-weight-semibold"><i class="icon-reading mr-2"></i> Maklumat Peribadi</legend>

                                        {{-- <div>
                                            <div class="form-group">
                                                <label>Status Maklumat:</label>
                                                <select name="status" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                                    <option></option>
                                                    <option value="Aktif" @if(old('status',$profile->status)=='Aktif') selected @endif>Aktif</option>
                                                    <option value="Tidak Aktif" @if(old('status',$profile->status)=='Tidak Aktif') selected @endif>Tidak Aktif</option>
                                                </select>
                                            </div>
                                        </div> --}}

                                        <div class="form-group">
                                            <label>No. Kad Pengenalan:</label>
                                            <input type="text" name="no_kp" value="{{ old('no_kp',$profile->no_kp) }}" class="form-control" placeholder="901013101234" readonly>
                                        </div>

                                        <div class="form-group">
                                            <label>Nama penerima:</label>
                                            <input type="text" name="nama" value="{{ old('nama',$profile->nama) }}" class="form-control" placeholder="Mohd Roslee bin Ahmad">
                                        </div>

                                        <div class="form-group">
                                            <label>Pilih jantina:</label>
                                            <select name="jantina" data-placeholder="Pilih jantina" class="form-control form-control-select2" data-fouc>
                                                <option></option>
                                                <option value="L" @if(old('jantina',$profile->jantina)=='L') selected @endif>LELAKI</option>
                                                <option value="P" @if(old('jantina',$profile->jantina)=='P') selected @endif>PEREMPUAN</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Pilih Agama:</label>
                                            <select name="agama" data-placeholder="Pilih jantina" class="form-control form-control-select2" data-fouc>
                                                <option></option>
                                                <option value="I" @if(old('agama',$profile->agama)=='I') selected @endif>ISLAM</option>
                                                <option value="B" @if(old('agama',$profile->agama)=='B') selected @endif>BUDHA</option>
                                                <option value="H" @if(old('agama',$profile->agama)=='H') selected @endif>HINDU</option>
                                                <option value="L" @if(old('agama',$profile->agama)=='L') selected @endif>LAIN-LAIN</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Nyatakan jika agama lain-lain:</label>
                                            <input type="text" name="agama_lain" class="form-control" placeholder="-">
                                        </div>

                                        <div class="form-group">
                                            <label>Pilih Bangsa:</label>
                                            <select name="bangsa" data-placeholder="Pilih bangsa" class="form-control form-control-select2" data-fouc>
                                                <option></option>
                                                <option value="M" @if(old('bangsa',$profile->bangsa)=='M') selected @endif>MELAYU</option>
                                                <option value="C" @if(old('bangsa',$profile->bangsa)=='C') selected @endif>CINA</option>
                                                <option value="I" @if(old('bangsa',$profile->bangsa)=='I') selected @endif>INDIA</option>
                                                <option value="L" @if(old('bangsa',$profile->bangsa)=='L') selected @endif>LAIN-LAIN</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Nyatakan jika bangsa lain-lain:</label>
                                            <input type="text" name="bangsa_lain" class="form-control" placeholder="Contoh : Bidayuh">
                                        </div>

                                        <div class="form-group">
                                            <label>Status Perkahwinan:</label>
                                            <select name="status_perkahwinan" data-placeholder="Pilih Status" class="form-control form-control-select2" data-fouc>
                                                <option></option>
                                                <option value="B" @if(old('status_perkahwinan',$profile->status_perkahwinan)=='B') selected @endif>BUJANG</option>
                                                <option value="K" @if(old('status_perkahwinan',$profile->status_perkahwinan)=='K') selected @endif>BERKAHWIN</option>
                                                <option value="P" @if(old('status_perkahwinan',$profile->status_perkahwinan)=='P') selected @endif>PERNAH BERKAHWIN</option>
                                            </select>
                                        </div>

                                        <div>
                                            <div class="form-group">
                                                <label>Kad OKU:</label>
                                                <select name="oku" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                                    <option></option>
                                                    <option value="Y" selected>YA</option>
                                                    <option value="T" selected>TIDAK</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Nota Tambahan:</label>
                                            <textarea rows="5" cols="5" class="form-control" placeholder="Masukkan nota tambahan anda di sini">{{ $profile->nota }}</textarea>
                                        </div>
                                    </fieldset>
                                </div>

                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="font-weight-semibold"><i class="icon-home mr-2"></i> Maklumat Alamat & Kontak</legend>

                                            <div>
                                                <div class="form-group">
                                                    <label>E-mel:</label>
                                                    <input type="text" name="emel" value="{{ $profile->emel }}" placeholder="Contoh : mohd@gmail.com" class="form-control">
                                                </div>
                                            </div>

                                            <div>
                                                <div class="form-group">
                                                    <label>No.Telefon:</label>
                                                    <input type="text" name="no_telefon" value="{{ $profile->no_telefon }}" placeholder="017-4521003" class="form-control">
                                                </div>
                                            </div>

                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>No. Rumah/Lot/Jalan:</label>
                                                    <input type="text" name="alamat" value="{{ $profile->alamat }}" placeholder="No. 802, Jalan SG 1/0, Fasa 1" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Kampung:</label>
                                                    <input type="text" name="kampung" value="{{ $profile->kampung }}" placeholder="Contoh : Pinggiran Batu Caves" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Poskod:</label>
                                                    <input type="text" name="poskod" placeholder="68100" value="{{ $profile->poskod }}" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Bandar:</label>
                                                    <input type="text" name="bandar" value="{{ $profile->bandar }}" placeholder="Batu Caves" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Negeri:</label>
                                                    <input type="text" name="negeri" value="{{ $profile->negeri }}" placeholder="Selangor" value="Selangor" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Negara:</label>
                                                    <select name="negara" data-placeholder="Pilih Negara" class="form-control form-control-select2" data-fouc>
                                                        <option></option>
                                                        <option value="MAL" selected>Malaysia</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>DUN:</label>
                                                    <select name="dun" data-placeholder="Pilih DUN" aria-readonly="true" class="form-control form-control-select2" data-fouc>
                                                        <option></option>
                                                        <option value="1" selected>Gombak Setia</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>DM:</label>
                                                     <select name="dm" data-placeholder="Pilih DM" class="form-control @if ($errors->has('dm')) border-danger @endif">
                                                        <option></option>
                                                        @foreach ($all_dm as $dm)
                                                        <option value="{{ $dm->id }}" @if(old('dm', $dm->id) == $profile->dm_id) selected @endif>{{ $dm->nama }}</option>
                                                        @endforeach
                                                    </select>
                                                    {{-- <input type="text" name="dm" value="{{ old('dm',$profile->dm->nama) }}" placeholder="Contoh : Gombak Utara" class="form-control"> --}}
                                                </div>
                                            </div>

                                        </div>


                                        {{-- <div class="form-group">
                                            <label>Additional message:</label>
                                            <textarea rows="5" cols="5" class="form-control" placeholder="Enter your message here"></textarea>
                                        </div> --}}
                                    </fieldset>
                                </div>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">Kemaskini <i class="icon-paperplane ml-2"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            {{-- <div class="tab-pane fade" id="pasangan">
                {{ $profile->spouse}}
            </div>

            <div class="tab-pane fade" id="anak">
                DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg whatever.
            </div>

            <div class="tab-pane fade" id="bantuan">
                Aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthet.
            </div>

            <div class="tab-pane fade" id="bantuan_khas">
                bantuan khas.
            </div> --}}
        </div>
    </div>
</div>



@endsection
