@extends('layouts.backend')

@section('title')
    Urusetia
@endsection

@section('top_button')
    <a href="/penerima" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Penerima</span></a>
    <a href="/penerima/create" class="btn btn-link btn-float text-default"><i class="icon-plus-circle2 text-primary"></i> <span>Tambah Penerima</span></a>
@endsection

@section('breadcrumb')
    <a href="/dashboard" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="/pemohon" class="breadcrumb-item">Senarai Penerima</a>
    {{-- <span class="breadcrumb-item active">Senarai Pemohon</span> --}}
@endsection

@section('content')

<div class="card">
    {{-- <div class="card-header header-elements-inline">
        <h6 class="card-title">Justified tabs. Left icons</h6>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div> --}}

    <div class="card-body">
        <h6>
        <ul class="nav nav-tabs bg-info nav-tabs-highlight nav-justified">
            <li class="nav-item"><a href="#justified-left-icon-tab1" class="nav-link active" data-toggle="tab"><i class="icon-menu7 mr-2"></i> Maklumat Penerima</a></li>
            <li class="nav-item"><a href="#justified-left-tab2" class="nav-link" data-toggle="tab"><i class="icon-stack-check"></i> Bantuan</a></li>
            <li class="nav-item"><a href="#justified-left-tab2" class="nav-link" data-toggle="tab"><i class="icon-stack-plus"></i> Bantuan Lain-Lain</a></li>
            {{-- <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="icon-gear mr-2"></i> Dropdown</a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#justified-left-tab3" class="dropdown-item" data-toggle="tab">Dropdown tab</a>
                    <a href="#justified-left-tab4" class="dropdown-item" data-toggle="tab">Another tab</a>
                </div>
            </li> --}}
        </ul>
        </h6>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="justified-left-tab1">
                <div class="card">


                    <div class="card-body">
                        <form action="#">
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="font-weight-semibold"><i class="icon-reading mr-2"></i> Maklumat Peribadi</legend>

                                        <div class="form-group">
                                            <label>Masukkan nama penerima:</label>
                                            <input type="text" name="nama" class="form-control" placeholder="Khadijah binti Roslee">
                                        </div>

                                        <div class="form-group">
                                            <label>Pilih jantina:</label>
                                            <select name="jantina" data-placeholder="Pilih jantina" class="form-control form-control-select2" data-fouc>
                                                <option></option>
                                                <option value="L">LELAKI</option>
                                                <option value="P">PEREMPUAN</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Pilih Agama:</label>
                                            <select name="jantina" data-placeholder="Pilih jantina" class="form-control form-control-select2" data-fouc>
                                                <option></option>
                                                <option value="I">ISLAM</option>
                                                <option value="B">BUDHA</option>
                                                <option value="H">HINDU</option>
                                                <option value="L">LAIN-LAIN</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Status Perkahwinan:</label>
                                            <select name="status_perkahwinan" data-placeholder="Pilih Status" class="form-control form-control-select2" data-fouc>
                                                <option></option>
                                                <option value="B">BUJANG</option>
                                                <option value="K">BERKAHWIN</option>
                                                <option value="P">PERNAH BERKAHWIN</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Salinan Kad Pengenalan:</label>
                                            <input type="file" class="form-input-styled" data-fouc>
                                        </div>

                                        <div class="form-group">
                                            <label>Nota Tambahan:</label>
                                            <textarea rows="5" cols="5" class="form-control" placeholder="Masukkan nota tambahan anda di sini"></textarea>
                                        </div>
                                    </fieldset>
                                </div>

                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="font-weight-semibold"><i class="icon-home mr-2"></i> Maklumat Alamat & Kontak</legend>


                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>E-mel:</label>
                                                    <input type="text" name="emel" placeholder="khadijah@gmail.com" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>No.Telefon:</label>
                                                    <input type="text" name="notel" placeholder="+99-99-9999-9999" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>No. Rumah/Lot/Jalan:</label>
                                                    <input type="text" name="alamat" placeholder="No. 128, Lorong Enggang" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Poskod:</label>
                                                    <input type="text" name="poskod" placeholder="68100" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Bandar:</label>
                                                    <input type="text" name="bandar" placeholder="Batu Caves" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Negeri:</label>
                                                    <input type="text" name="negeri" placeholder="Selangor" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Negara:</label>
                                                    <select data-placeholder="Select your country" class="form-control form-control-select2" data-fouc>
                                                        <option></option>
                                                        <option value="Cambodia">Malaysia</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>


                                        {{-- <div class="form-group">
                                            <label>Additional message:</label>
                                            <textarea rows="5" cols="5" class="form-control" placeholder="Enter your message here"></textarea>
                                        </div> --}}
                                    </fieldset>
                                </div>
                            </div>

                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="justified-left-tab2">
                Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid laeggin.
            </div>

            <div class="tab-pane fade" id="justified-left-tab3">
                DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg whatever.
            </div>

            <div class="tab-pane fade" id="justified-left-tab4">
                Aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthet.
            </div>
        </div>
    </div>
</div>



@endsection
