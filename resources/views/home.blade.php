@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="mb-3">Carian No. Kad Pengenalan</h5>

                    <form method="post" enctype="multipart/form-data">

                        @csrf
                        <div class="input-group mb-3">
                            <div class="form-group-feedback form-group-feedback-left">
                                <input type="number" size="12" class="form-control form-control-lg alpha-grey" name="search" placeholder="Contoh : 901014101234">
                                <div class="form-control-feedback form-control-feedback-lg">
                                    <i class="icon-search4 text-muted"></i>
                                </div>
                            </div>

                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary btn-lg">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>			
        </div>
    </div>
</div>
@endsection
