@extends('layouts.backend')

@section('breadcrumb')
    {{-- <a href="/home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="/search/show/{{ $search }}/{{ $recepient->id }}" class="breadcrumb-item active">Maklumat Pemohon</a> --}}
    {{-- <span class="breadcrumb-item active">Senarai Pemohon</span> --}}
@endsection

@section('content')          <!-- Content area -->
                <div class="content">
                    {{-- @include('layouts.partials.notification')
                    @yield('content') --}}

                    <div class="card">
                        <div class="card-header header-elements-inline">
                            <h5 class="card-title">Laporan Bantuan IPR Selangor</h5>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                    <a class="list-icons-item" data-action="reload"></a>
                                    <a class="list-icons-item" data-action="remove"></a>
                                </div>
                            </div>
                        </div>

                        {{-- <div class="card-body">
                            Each HTML control element presented by DataTables is denoted by a single character in the <code>dom</code> option. For example the <code>l</code> option is used for the <code>L</code>ength changing input option. In this example default DOM positions are mixed by adding correction classes to the parent container: <code>.length-left</code> to show length on the left side, <code>.filter-right</code> to show filter on the right etc.
                        </div> --}}

					<table class="table datatable-button-html5-basic">
						<thead>

							<tr>
								<th style="width:1%">Bil</th>
								<th style="width:130px">Nama Penerima</th>
								<th>No.KP</th>
								<th>Alamat Rumah</th>
								<th>Pasangan/Tanggungan</th>
								<th>No.Tel</th>
								<th>Salinan KP</th>
								<th>KP1</th>
								<th>KP2</th>
								<th>Slip Gaji</th>
								<th>Pengundi</th>
								<th>Bantuan</th>

							</tr>
						</thead>
						<tbody>
                            <?php
        	                    $x=1;
                            ?>

                            @foreach ($donations as $donation)
                                <tr>
                                    <td>{{ $x++ }}</td>
                                    <td>{{ $donation->recepient->profile->nama }}</td>
                                    <td>{{ $donation->recepient->no_kp }}</td>
                                    <td>{{ $donation->recepient->profile->alamat }}, {{ $donation->recepient->profile->village->nama }}</td>
                                    <td>
                                        @foreach ($donation->recepient->dependents as $dependent)
                                            {{ $dependent->nama }} ( {{ $dependent->hubungan }} )<br>
                                        @endforeach
                                    </td>
                                    <td>{{ $donation->recepient->profile->no_telefon }}</td>
                                    <td>{{ $donation->salinan_kp }}</td>
                                    <td>{{ $donation->kp1 }}</td>
                                    <td>{{ $donation->kp2 }}</td>
                                    <td>{{ $donation->slip_gaji }}</td>
                                    <td>{{ $donation->recepient->pengundi }}</td>
                                    <td><font size="1">
                                        <?php
                                            $y=1;
                                        ?>
                                        @foreach ($donation->recepient->donations as $donation)
                                            {{ $y++ }}. {{ $donation->refDonation->nama}} <br>
                                        @endforeach</font>
                                    </td>
                                    {{-- <td>{{ $donation->refDonation->nama }}</td> --}}

                                </tr>
                            @endforeach

							{{-- <tr>
								<td>Cicely</td>
								<td>Sigler</td>
								<td><a href="#">Senior Research Officer</a></td>
								<td>15 Mar 1960</td>
								<td>15 Mar 1960</td>
								<td>15 Mar 1960</td>
								<td>15 Mar 1960</td>
								<td>15 Mar 1960</td>
								<td>15 Mar 1960</td>
								<td>15 Mar 1960</td>
								<td><span class="badge badge-info">Pending</span></td>
								<td class="text-center">
									<div class="list-icons">
										<div class="dropdown">
											<a href="#" class="list-icons-item" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<div class="dropdown-menu dropdown-menu-right">
												<a href="#" class="dropdown-item"><i class="icon-file-pdf"></i> Export to .pdf</a>
												<a href="#" class="dropdown-item"><i class="icon-file-excel"></i> Export to .csv</a>
												<a href="#" class="dropdown-item"><i class="icon-file-word"></i> Export to .doc</a>
											</div>
										</div>
									</div>
								</td>
							</tr> --}}
						</tbody>
                    </table>

				</div>
                </div>
                <!-- /content area -->


                <!-- Footer -->
                <div class="navbar navbar-expand-lg navbar-light">
                    <div class="text-center d-lg-none w-100">
                        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                            <i class="icon-unfold mr-2"></i>
                            Footer
                        </button>
                    </div>

                    <div class="navbar-collapse collapse" id="navbar-footer">
                       <span class="navbar-text">
                          &copy; {{ \Carbon\Carbon::now()->year }} <a href="#">eBantuan</a> by

                          <a href="">Pejabat DUN</a>
                      </span>


                </div>
            </div>
            <!-- /footer -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

    @yield('script')

</body>
</html>




{{-- @extends('layouts.backend')

@section('title')
    Laporan
@endsection

@section('top_button')
    <a href="/rujukan/bantuan" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Bantuan IPR Sel.</span></a>
@endsection

@section('breadcrumb')
    <a href="/dashboard" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="#" class="breadcrumb-item">Laporan</a>
    <a href="/rujukan/bantuan" class="breadcrumb-item">Bantuan IPR Selangor</a>
    <span class="breadcrumb-item active">Papar</span>
@endsection

@section('content') --}}
    {{-- <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Laporan Bantuan IPR Selangor</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body"> --}}

            <!-- DOM positioning -->
				{{-- <div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Laporan Bantuan IPR Selangor</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<a class="list-icons-item" data-action="reload"></a>
		                		<a class="list-icons-item" data-action="remove"></a>
		                	</div>
	                	</div>
					</div>

					<div class="card-body">

					</div>

					<table class="table datatable-show-all">
						<thead>
							<tr>
								<th>Bil</th>
								<th>Nama</th>
								<th>No.KP</th>
								<th>Alamat</th>
								<th>Tanggungan</th>
								<th>No.Tel</th>
								<th>KP1</th>
								<th>KP2</th>
								<th>Slip Gaji</th>
								<th>Pengundi</th>
								<th>Status</th>
								<th class="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Marth</td>
								<td><a href="#">Enright</a></td>
								<td>Traffic Court Referee</td>
								<td>22 Jun 1972</td>
								<td>22 Jun 1972</td>
								<td>22 Jun 1972</td>
								<td>22 Jun 1972</td>
								<td>22 Jun 1972</td>
								<td>22 Jun 1972</td>
								<td>22 Jun 1972</td>
								<td><span class="badge badge-success">Active</span></td>
								<td class="text-center">
									<div class="list-icons">
										<div class="dropdown">
											<a href="#" class="list-icons-item" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<div class="dropdown-menu dropdown-menu-right">
												<a href="#" class="dropdown-item"><i class="icon-file-pdf"></i> Export to .pdf</a>
												<a href="#" class="dropdown-item"><i class="icon-file-excel"></i> Export to .csv</a>
												<a href="#" class="dropdown-item"><i class="icon-file-word"></i> Export to .doc</a>
											</div>
										</div>
									</div>
								</td>
							</tr>

							<tr>
								<td>Cicely</td>
								<td>Sigler</td>
								<td><a href="#">Senior Research Officer</a></td>
								<td>15 Mar 1960</td>
								<td>15 Mar 1960</td>
								<td>15 Mar 1960</td>
								<td>15 Mar 1960</td>
								<td>15 Mar 1960</td>
								<td>15 Mar 1960</td>
								<td>15 Mar 1960</td>
								<td><span class="badge badge-info">Pending</span></td>
								<td class="text-center">
									<div class="list-icons">
										<div class="dropdown">
											<a href="#" class="list-icons-item" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<div class="dropdown-menu dropdown-menu-right">
												<a href="#" class="dropdown-item"><i class="icon-file-pdf"></i> Export to .pdf</a>
												<a href="#" class="dropdown-item"><i class="icon-file-excel"></i> Export to .csv</a>
												<a href="#" class="dropdown-item"><i class="icon-file-word"></i> Export to .doc</a>
											</div>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div> --}}
				{{-- <!-- /DOM positioned -->
        </div>

    </div> --}}
{{-- @endsection

@section('script')

@endsection --}}

@endsection
