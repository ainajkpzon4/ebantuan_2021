@if (count($errors) > 0)
    <div class="alert alert-danger alert-styled-left alert-bordered">
        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
        <ul class="list-unstyled">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{{-- Bootstrap Alert --}}
@if (Session::has('successMessage'))
    <div class="alert alert-success alert-styled-left alert-bordered">
        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
        <strong>Success</strong> {{ session('successMessage') }}
    </div>
@endif

@if (Session::has('errorMessage'))
    <div class="alert alert-danger alert-styled-left alert-bordered">
        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
        <strong>Error!</strong> {{ session('errorMessage') }}
    </div>
@endif

{{-- Sweet Alert --}}
@if (Session::has('flashSuccess'))
    <script>
        Swal.fire(
            'Success!',
            '{{ session('flashSuccess') }}',
            'success'
        )
    </script>
    <div class="alert alert-success alert-styled-left alert-bordered">
        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
        <strong>Success</strong> {{ session('flashSuccess') }}
    </div>
@endif
