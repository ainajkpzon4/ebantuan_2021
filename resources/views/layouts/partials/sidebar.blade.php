<?php
use App\User;
use App\Role;
use App\Area;
?>
<div class="sidebar-content">
	<!-- User menu -->
	<div class="sidebar-user">
		<div class="text-center">
			{{-- <div class="media"> --}}
				{{-- <div class="mr-3"> --}}
                    {{-- <a href="#"><img src="{{ Auth::user()->foto }}" width="38" height="38" class="rounded-circle" alt=""></a> --}}
            {{-- <img src="{{ asset('imgs/jata.png') }}" width="120" height="140"> --}}

				{{-- </div> --}}

				{{-- <div class="media-body"> --}}
					{{-- <div class="media-title font-weight-semibold">{{ Auth::user()->name }}</div> --}}
					{{-- <div class="font-size-xs opacity-50 text-uppercase"> --}}
						{{-- <i class="icon-pin font-size-sm"></i> &nbsp; --}}
						{{-- {{ Auth::user()->role }} --}}
					{{-- </div> --}}
				{{-- </div> --}}
			{{-- </div> --}}
		</div>
	</div>
	<!-- /user menu -->


	<!-- Main navigation -->
	<div class="card">
		<ul class="nav nav-sidebar" data-nav-type="accordion">

            {{-- PEMOHON --}}
            {{-- <li class="nav-item-header">
				<div class="text-uppercase font-size-xs line-height-xs">Bantuan</div>
				<i class="icon-menu" title="Pemohon"></i>
            </li>
            <li class="nav-item">
                <a href="/rujukan/bantuan" class="nav-link @if (Request::segment(1) == 'bantuan') active @endif">
                    <i class="icon-stack2"></i>
                    <span>Maklumat Bantuan</span>
                </a>
            </li> --}}

			{{-- URUSETIA --}}
			<li class="nav-item-header">
				<div class="text-uppercase font-size-xs line-height-xs">Penerima</div>
				<i class="icon-menu" title="Urusetia"></i>
            </li>

            <li class="nav-item">
                {{-- <a href="/pemohon" class="nav-link @if (Request::segment(1) == 'pemohon') active @endif"> --}}
                {{-- <li class="nav-item nav-item-submenu @if (Request::segment(1) == 'lookup') nav-item-expanded nav-item-open @endif">
				<a href="#" class="nav-link  @if (Request::segment(1) == 'lookup') active @endif"><i class="icon-user"></i> <span>Pemohon</span></a> --}}
                    {{-- <i class="icon-grid2"></i> --}}
                    {{-- <span>Maklumat Pemohon</span> --}}
                {{-- <ul class="nav nav-group-sub" data-submenu-title="Layouts">
					<li class="nav-item"><a href="/profile" class="nav-link @if (Request::segment(1) == 'pemohon') active @endif">Profail</a></li>
					 <li class="nav-item"><a href="/bantuan" class="nav-link @if (Request::segment(1) == 'scheme') active @endif">Bantuan</a></li> --}}
					{{--<li class="nav-item"><a href="/department" class="nav-link @if (Request::segment(1) == 'department') active @endif">Kampung</a></li>
					<li class="nav-item"><a href="/location" class="nav-link @if (Request::segment(1) == 'location') active @endif">Negeri</a></li>
                    <li class="nav-item"><a href="/role" class="nav-link @if (Request::segment(1) == 'role') active @endif">Peranan</a></li> --}}
				{{-- </ul>
                </a> --}}
            </li>

             <li class="nav-item">
                <a href="/home" class="nav-link @if (Request::segment(1) == 'home') active @endif">
                    <i class="icon-home"></i>
                    <span>Halaman Utama</span>
                </a>
            </li>

            <li class="nav-item">
                <a href="/penerima/status/1" class="nav-link @if (Request::segment(1) == 'penerima') active @endif">
                    <i class="icon-people"></i>
                    <span>Penerima</span>
                </a>
            </li>

            <li class="nav-item nav-item-submenu nav-item-expanded">
				<a href="#" class="nav-link  @if(Request::segment(1) == 'report') active @endif"><i class="icon-chart"></i> <span>Laporan</span></a>

				<ul class="nav nav-group-sub" data-submenu-title="report">
					<li class="nav-item"><a href="/laporan/bantuan/create" class="nav-link @if (Request::segment(1) == 'bantuan') active @endif">Bantuan IPR Selangor</a></li>
					<li class="nav-item"><a href="/laporan/bantuan-khas/create" class="nav-link @if (Request::segment(1) == '/rujukan/dm') active @endif">Bantuan Khas</a></li>
					{{-- <li class="nav-item"><a href="/rujukan/village" class="nav-link @if (Request::segment(1) == 'department') active @endif">Kampung</a></li>
					<li class="nav-item"><a href="/rujukan/category" class="nav-link @if (Request::segment(1) == 'kategori') active @endif">Kategori</a></li> --}}
				</ul>
            </li>

            {{-- PENTADBIR --}}
			<li class="nav-item-header">
				<div class="text-uppercase font-size-xs line-height-xs">Pentadbir</div>
				<i class="icon-menu" title="Pentadbir"></i>
            </li>

			<li class="nav-item nav-item-submenu nav-item-expanded">
				<a href="#" class="nav-link  @if (Request::segment(1) == 'lookup') active @endif"><i class="icon-list"></i> <span>Pengurusan Data</span></a>

				<ul class="nav nav-group-sub" data-submenu-title="Layouts">
					<li class="nav-item"><a href="/rujukan/bantuan" class="nav-link @if (Request::segment(1) == 'bantuan') active @endif">Bantuan IPR</a></li>
					<li class="nav-item"><a href="/rujukan/dm" class="nav-link @if (Request::segment(1) == '/rujukan/dm') active @endif">DM</a></li>
					<li class="nav-item"><a href="/rujukan/village" class="nav-link @if (Request::segment(1) == 'department') active @endif">Kampung</a></li>
					<li class="nav-item"><a href="/rujukan/category" class="nav-link @if (Request::segment(1) == 'kategori') active @endif">Kategori</a></li>
                    {{-- <li class="nav-item"><a href="/role" class="nav-link @if (Request::segment(1) == 'role') active @endif">Peranan</a></li> --}}
				</ul>
            </li>

            <li class="nav-item">
                <a href="/user/profile/" class="nav-link @if (Request::segment(1) == 'user') active @endif">
                    <i class="icon-users"></i>
                    <span>Pengurusan Pengguna</span>
                </a>
            </li>

		</ul>

	</div>
	<!-- /main navigation -->



</div>
