<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>eBantuan</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="/global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <style type="text/css">
        .unread{
            font-weight: bold;
        }
    </style>
    <!-- Core JS files -->
    <script src="/global_assets/js/main/jquery.min.js"></script>
    <script src="/global_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="/global_assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="/global_assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script src="/global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script src="/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>
    {{--    Datatable --}}

	<script src="/global_assets/js/plugins/tables/datatables/extensions/row_reorder.min.js"></script>
	<script src="/global_assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
	{{-- <script src="../../../../global_assets/js/plugins/forms/selects/select2.min.js"></script> --}}

	<script src="assets/js/app.js"></script>
	<script src="/global_assets/js/demo_pages/datatables_extension_row_reorder.js"></script>
    <script src="/global_assets/js/demo_pages/datatables_extension_buttons_init.js"></script>
	<script src="/global_assets/js/demo_pages/datatables_extension_buttons_print.js"></script>


    <script src="/global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="/global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script src="/global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
    <script src="/global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
    <script src="/global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="/global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
	<script src="/global_assets/js/demo_pages/datatables_advanced.js"></script>


    <script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script src="/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script src="/global_assets/js/plugins/forms/styling/switch.min.js"></script>

    <script src="/global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script src="/global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="/global_assets/js/plugins/visualization/echarts/echarts.min.js"></script>

    <script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="/global_assets/js/plugins/pickers/anytime.min.js"></script>
    <script src="/global_assets/js/plugins/pickers/pickadate/picker.js"></script>
    <script src="/global_assets/js/plugins/pickers/pickadate/picker.date.js"></script>
    <script src="/global_assets/js/plugins/pickers/pickadate/picker.time.js"></script>
    <script src="/global_assets/js/plugins/pickers/pickadate/legacy.js"></script>
    <script src="/global_assets/js/plugins/notifications/jgrowl.min.js"></script>

    <!-- Theme JS files -->
    <script src="/global_assets/js/plugins/forms/wizards/steps.min.js"></script>
	<script src="/global_assets/js/plugins/forms/selects/select2.min.js"></script>
	<script src="/global_assets/js/plugins/forms/inputs/inputmask.js"></script>
	<script src="/global_assets/js/plugins/forms/validation/validate.min.js"></script>
	<script src="/global_assets/js/plugins/extensions/cookie.js"></script>

    <script src="/assets/js/app.js"></script>
    <script src="/global_assets/js/demo_pages/form_wizard.js"></script>
	<script src="/global_assets/js/demo_pages/datatables_basic.js"></script>


    <script src="/global_assets/js/demo_pages/dashboard.js"></script>
    <script src="/global_assets/js/demo_charts/pages/dashboard/light/streamgraph.js"></script>
    <script src="/global_assets/js/demo_charts/pages/dashboard/light/sparklines.js"></script>
    <script src="/global_assets/js/demo_charts/pages/dashboard/light/lines.js"></script>
    <script src="/global_assets/js/demo_charts/pages/dashboard/light/areas.js"></script>
    <script src="/global_assets/js/demo_charts/pages/dashboard/light/donuts.js"></script>
    <script src="/global_assets/js/demo_charts/pages/dashboard/light/bars.js"></script>
    <script src="/global_assets/js/demo_charts/pages/dashboard/light/progress.js"></script>
    <script src="/global_assets/js/demo_charts/pages/dashboard/light/heatmaps.js"></script>
    <script src="/global_assets/js/demo_charts/pages/dashboard/light/pies.js"></script>
    <script src="/global_assets/js/demo_charts/pages/dashboard/light/bullets.js"></script>
    <script src="/global_assets/js/demo_pages/datatables_extension_buttons_html5.js"></script>
    <script src="/global_assets/js/demo_pages/form_checkboxes_radios.js"></script>
    <script src="/global_assets/js/demo_pages/form_checkboxes_radios.js"></script>
    <script src="/global_assets/js/demo_pages/extra_sweetalert.js"></script>
    <script src="/global_assets/js/demo_pages/picker_date.js"></script>


<!-- Theme JS files -->
	{{-- <script src="../../../../global_assets/js/plugins/forms/styling/uniform.min.js"></script> --}}
	<script src="/global_assets/js/plugins/notifications/pnotify.min.js"></script>
	<script src="/global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>


    <script src="/global_assets/js/demo_pages/form_multiselect.js"></script>


    @yield('script')


    <style>
        .col-form-label {
            color: #34495e;
        }
    </style>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="/notifications/notifications.js?nocache=1"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>

<body>

    <!-- Main navbar -->
    <div class="navbar navbar-expand-md navbar-light">

        <!-- Header with logos -->
        <div class="navbar-header navbar-light d-none d-md-flex align-items-md-center">
            <div class="navbar-brand navbar-brand-md">
                <a href="/home" class="d-inline-block">
                eBantuan

                {{-- <img src="{{ asset('imgs/jata.png') }}" width="120" height="200"> --}}
                {{-- <img src="{{ asset('imgs/jata.png') }}" width="120" height="200" alt=""> --}}

                </a>
            </div>

            <div class="navbar-brand navbar-brand-xs">
                <a href="/home" class="d-inline-block">

                    {{-- <img src="https://upload.wikimedia.org/wikipedia/commons/2/26/Coat_of_arms_of_Malaysia.svg" alt=""> --}}
                </a>
            </div>
        </div>
        <!-- /header with logos -->


        <!-- Mobile controls -->
        <div class="d-flex flex-1 d-md-none">
            <div class="navbar-brand mr-auto">
                <a href="/home" class="d-inline-block">
                    {{-- <img src="https://upload.wikimedia.org/wikipedia/commons/2/26/Coat_of_arms_of_Malaysia.svg" alt="">
                </a> --}}
            </div>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-bubble-dots3"></i>
            </button>

            <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                <i class="icon-paragraph-justify3"></i>
            </button>
        </div>
        <!-- /mobile controls -->


        <!-- Navbar content -->
        <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                    </a>
                </li>
            </ul>

            <ul class="navbar-nav ml-md-auto">
                <li class="nav-item dropdown dropdown-user">
                    <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                        <img src="/{{ Auth::user()->foto }}" class="rounded-circle mr-2" height="34" alt="">
                        <span>{{ Auth::user()->name }}</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="/user/profile/show/{{ Auth::user()->id }}" class="dropdown-item"><i class="icon-user-plus"></i> Profil</a>
                        <div class="dropdown-divider"></div>
                        <a href="/change-password/" class="dropdown-item"><i class="icon-cog5"></i> Tukar Kata Laluan</a>
                        <a href="/logout" class="dropdown-item"><i class="icon-switch2"></i> Log Keluar</a>
                    </div>
                </li>
            </ul>
        </div>
        <!-- /navbar content -->

    </div>
    <!-- /main navbar -->


    <!-- Page content -->

    <div class="page-content">

        <!-- Main sidebar -->
		 @if (session('mobile') == false)
			  <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

					<!-- Sidebar mobile toggler -->
					<div class="sidebar-mobile-toggler text-center">
						 <a href="#" class="sidebar-mobile-main-toggle">
							  <i class="icon-arrow-left8"></i>
						 </a>
						 Navigation
						 <a href="#" class="sidebar-mobile-expand">
							  <i class="icon-screen-full"></i>
							  <i class="icon-screen-normal"></i>
						 </a>`
					</div>
					<!-- /sidebar mobile toggler -->

					<!-- Sidebar content -->
						@include('layouts.partials.sidebar')
					<!-- /sidebar content -->
			  </div>
        <!-- /main sidebar -->
	 		@endif

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <h4></span>
                                @yield('title')
                        </h4>
                        {{-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> --}}
                    </div>

                    <div class="header-elements d-none">
                        <div class="d-flex justify-content-center">
                            @yield('top_button')
                        </div>
                    </div>
                </div>

                    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                        <div class="d-flex">
                            <div class="breadcrumb">

                                @yield('breadcrumb')
                            </div>

                            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                        </div>
                    </div>
                </div>
                <!-- /page header -->


                <!-- Content area -->
                <div class="content">
                    @include('layouts.partials.notification')
                    @yield('content')
                </div>
                <!-- /content area -->


                <!-- Footer -->
                <div class="navbar navbar-expand-lg navbar-light">
                    <div class="text-center d-lg-none w-100">
                        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                            <i class="icon-unfold mr-2"></i>
                            Footer
                        </button>
                    </div>

                    <div class="navbar-collapse collapse" id="navbar-footer">
                       <span class="navbar-text">
                          &copy; {{ \Carbon\Carbon::now()->year }} <a href="#">eBantuan</a> by

                          <a href="">Pejabat DUN</a>
                      </span>


                </div>
            </div>
            <!-- /footer -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

    @yield('script')

</body>
</html>
