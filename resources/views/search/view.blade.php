@extends('layouts.backend')

@section('breadcrumb')
    <a href="/home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="/search/show/{{ $search }}/{{ $recepient->id }}" class="breadcrumb-item active">Maklumat Pemohon</a>
    {{-- <span class="breadcrumb-item active">Senarai Pemohon</span> --}}
@endsection

@section('content')

<div class="card">

    <div class="card-body">
        <!-- Info blocks -->
				<div class="row">
					<div class="col-lg-6">
						<div class="card">
							<div class="card-body text-center">
                            <h5 class="card-title bg-success">Maklumat Penerima</h5>
							<i class="icon-user icon-2x text-success-400 border-success-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
                                <p class="mb-3">{{ $recepient->profile->nama }} - @if($search==$recepient->no_kp) <b> @endif {{ $recepient->no_kp }}</b></p>
                                {{-- <p class="mb-3">@if($search==$recepient->no_kp) <b> @endif {{ $recepient->no_kp }}</b></p> --}}
                                <a href="#" data-toggle="modal" data-target="#BantuanModal" class="btn bg-success-400">Senarai Bantuan</a>

							</div>
						</div>
                    </div>
                    {{-- Modal Penerima --}}
                    <div class="modal fade" id="BantuanModal" tabindex="-1" role="dialog" aria-labelledby="pengalamanTerdahuluModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-light">
                                    <h5 class="modal-title" id="BantuanModalLabel">Senarai Bantuan Penerima</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                </div>
                                <div class="modal-body">

                                    <table class="table table-xs">
                                        <thead>
                                            <tr class="bg-secondary">
                                                <th>Nama Bantuan IPR Sel.</th>
                                                <th>Tarikh Mohon</th>
                                                <th>Tarikh Lulus</th>
                                                <th>Status Kelulusan</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            @if($recepient->donations->count() == 0)
                                                <td>-Tiada maklumat-</td>
                                            @endif
                                        </tr>
                                        @foreach ($recepient->donations as $donation)
                                        <tr>
                                            <td>{{ $donation->refDonation->nama }}</td>
                                            <td>
                                                @if(!empty($donation->tarikh_mohon))
                                                    {{ date('d-m-Y', strtotime($donation->tarikh_mohon)) }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                @if(!empty($donation->tarikh_lulus))
                                                    {{ date('d-m-Y', strtotime($donation->tarikh_lulus)) }}
                                                @else
                                                    -
                                                @endif
                                            </td>

                                            </td>
                                            <td>
                                                @if ($donation->status_kelulusan == 'Lulus')
                                                    <span class="badge badge-success text-uppercase">{{ $donation->status_kelulusan }}</span>
                                                @elseif($donation->status_kelulusan == 'Tidak Lulus')
                                                    <span class="badge badge-danger text-uppercase">{{ $donation->status_kelulusan }}</span>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                    {{-- <h5>Bantuan Khas</h5> --}}

                                    <table class="table table-xs">
                                        <thead>
                                            <tr class="bg-secondary">
                                                <th>Bantuan khas</th>
                                                <th>Kategori</th>
                                                <th>Tarikh Serahan</th>
                                                {{-- <th>Status</th> --}}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                @if($recepient->otherDonations->count() == 0)
                                                    <td>-Tiada maklumat-</td>
                                                @endif
                                            </tr>
                                                @foreach ($recepient->otherDonations as $otherDonation)
                                                <tr>
                                                    <td>{{ $otherDonation->nama }}</td>
                                                    <td>{{ $otherDonation->category->nama }}</td>
                                                    <td>
                                                        @if(!empty($otherDonation->tarikh_serahan))
                                                            {{ date('d-m-Y', strtotime($otherDonation->tarikh_serahan)) }}
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    {{-- <td><span class="badge badge-success">Aktif</span></td> --}}
                                                </tr>
                                                @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>

					<div class="col-lg-6">
						<div class="card">
							<div class="card-body text-center">
                                <h5 class="card-title bg-blue">Maklumat Tanggungan</h5>
                                <i class="icon-users4 icon-2x text-blue border-blue border-3 rounded-round p-3 mb-3 mt-1"></i>
                                @if($recepient->dependents->count() <= 0)
                                <br>-Tiada Maklumat-<br>
                                @endif
                                <?php $x=1 ?>
                                @foreach ($recepient->dependents as $dependent)
                                <p class="mb-3">{{ $x++.'. '.$dependent->nama }} ({{ $dependent->hubungan  }})@if($search==$dependent->no_kp) <b> @endif - {{ $dependent->no_kp }}</b></p>

                                @endforeach
                                {{-- @if($recepient->dependents->count() > 0)
                                    <a href="#" data-toggle="modal" data-target="#BantuanTanggunganModal" class="btn bg-success-400">Senarai Bantuan</a>
                                @else
                                    <br><br><br>
                                @endif --}}
							</div>
						</div>
                    </div>

                     {{-- Modal Tanggungan --}}
                    {{-- <div class="modal fade" id="BantuanTanggunganModal" tabindex="-1" role="dialog" aria-labelledby="pengalamanTerdahuluModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-light">
                                    <h4 class="modal-title" id="BantuanModalLabel">Senarai Bantuan Tanggungan</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                </div>
                                <div class="modal-body">

                                    <table class="table table-xs">
                                        <thead>
                                            <tr class="bg-secondary">
                                                <th>Nama Bantuan IPR Sel.</th>
                                                <th>Tarikh Mohon</th>
                                                <th>Tarikh Lulus</th>
                                                <th>Status Kelulusan</th>
                                            </tr>
                                        </thead>
                                        @foreach ($recepient->dependents as $dependent)

                                        <tbody>

                                            @foreach ($dependent->dependentDonations as $dependentDonation)
                                            <tr>
                                                <td>{{ $dependentDonation->refDonation->nama }}<br><small>{{$dependent->nama}}</small></td>
                                                <td>
                                                @if(!empty($dependentDonation->tarikh_mohon))
                                                    {{ date('d-m-Y', strtotime($donation->tarikh_mohon)) }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                @if(!empty($dependentDonation->tarikh_lulus))
                                                    {{ date('d-m-Y', strtotime($donation->tarikh_lulus)) }}
                                                @else
                                                    -
                                                @endif
                                            <td>
                                                @if ($dependentDonation->status_kelulusan == 'Lulus')
                                                    <span class="badge badge-success text-uppercase">{{ $dependentDonation->status_kelulusan }}</span>
                                                @elseif($dependentDonation->status_kelulusan == 'Tidak Lulus')
                                                    <span class="badge badge-danger text-uppercase">{{ $dependentDonation->status_kelulusan }}</span>
                                                @endif
                                            </td>
                                            @endforeach
                                        </tbody>
                                        @endforeach

                                    </table>

                                    <table class="table table-xs">
                                        <thead>
                                            <tr class="bg-secondary">
                                                <th>Bantuan Khas</th>
                                                <th>Kategori Bantuan</th>
                                                <th>Tarikh Serahan</th>

                                            </tr>
                                        </thead>
                                        @foreach ($recepient->dependents as $dependent)
                                        <tbody>
                                            @foreach ($dependent->dependentOtherDonations as $dependentOtherDonation)
                                                <tr>
                                                <td>{{ $dependentOtherDonation->nama }}<br><small>{{$dependent->nama}}</small></td>
                                                <td>{{ $dependentOtherDonation->category->nama }}</td>
                                                <td>
                                                    @if(!empty($dependentOtherDonation->tarikh_serahan))
                                                        {{ date('d-m-Y', strtotime($dependentOtherDonation->tarikh_serahan)) }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>


                                            </tr>
                                            @endforeach

                                        </tbody>
                                         @endforeach
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div> --}}
				</div>
				<!-- /info blocks -->
    </div>
</div>



@endsection
