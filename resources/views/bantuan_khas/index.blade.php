@extends('layouts.backend')

{{-- @section('title')
    Urusetia
@endsection --}}
@section('top_button')
    {{-- <a href="/penerima" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Penerima</span></a> --}}
    <a href="/penerima/search" class="btn btn-link btn-float text-default"><i class="icon-plus-circle2 text-primary"></i> <span>Tambah Penerima</span></a>
@endsection

@section('breadcrumb')
    <a href="/home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="/penerima/status/1" class="breadcrumb-item">Senarai Penerima</a>
    <span class="breadcrumb-item active">Bantuan Khas</span>

@endsection

@section('content')

<div class="card">

    <div class="card-body">

        <ul class="nav nav-tabs nav-tabs-solid nav-justified rounded bg-light">
            <li class="nav-item"><a href="/penerima/edit/{{ $recepient->no_kp}}" class="nav-link">Penerima</a></li>
            <li class="nav-item"><a href="/tanggungan/{{ $recepient->no_kp}}" class="nav-link">Tanggungan</a></li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link rounded-right dropdown-toggle active" data-toggle="dropdown">Bantuan</a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="/bantuan/{{ $recepient->no_kp }}" class="dropdown-item">Bantuan IPR Selangor</a>
                    <a href="/bantuan_khas/{{ $recepient->no_kp }}" class="dropdown-item active">Bantuan Khas</a>
                </div>
            </li>
        </ul>
        <div class="card">
            <div class="card-body">
                <h5>Bantuan Khas</h5>
                <form method="POST">
                    @csrf
                    <div class="col-md-3 form-group-feedback form-group-feedback-left">
                        <input type="text" class="form-control form-control-sm alpha-grey" name="tahun" placeholder="Cth : 2020"><button type="submit" class="btn btn-primary btn-sm">Carian</button>
                        <div class="form-control-feedback form-control-feedback-sm">
                            <i class="icon-search4 text-muted"></i>
                        </div>
                    </div>
                </form>
            </div>

            {{-- Index Recepient --}}
            <table class="table table-xs">
                <thead>
                    <tr>
                        <th style="width:25%">Bantuan</th>
                        <th style="width:13%">Tarikh Serahan</th>
                        {{-- <th style="width:13%">Kategori</th>
                        <th style="width:19%">Nota</th> --}}
                        <th style="width:15%">Status</th>
                        <th style="width:15%">Tindakan</th>
                    </tr>
                </thead>
                <tbody>

                    <td><button type="button" class="btn bg-blue-600 btn-labeled btn-labeled-left btn-sm" data-toggle="modal" data-target="#modal_recepient_add"><b><i class="icon-plus3"></i></b>Penerima : {{ $recepient->profile->nama }}</button></td>
            {{-- Index Recepient --}}
                    @foreach ($otherDonations as $otherDonation)
                        <tr>
                            <td>{{ $otherDonation->nama }}</td>

                            <td style="width:13%">
                                @if(!empty($otherDonation->tarikh_serahan))
                                    {{ date('d-m-Y', strtotime($otherDonation->tarikh_serahan)) }}
                                @else
                                    -
                                @endif
                            </td>
                            {{-- <td style="width:13%">{{ $otherDonation->category->nama }}</td>
                            <td style="width:19%">
                                <button type="button" class="btn btn-light" data-popup="popover" data-trigger="hover" data-content="{{ $otherDonation->sebab}}">Nota <i class="icon-play3 ml-2"></i></button>
                            </td> --}}
                            <td><span class="badge badge-success">Aktif</span></td>
                            <td>
                                <form class="delete" action="/bantuan_khas/destroy/{{ $otherDonation->id }}" method="POST">
                                     @csrf
									{{-- <button type="button" class="btn btn-light delete">Launch <i class="icon-play3 ml-2"></i></button> --}}
                                    <a href="#" data-toggle="modal" data-target="#recepientOtherDonation-{{ $otherDonation->id }}" class="btn bg-primary-600 badge-icon rounded-round" title="Papar"><i class="icon-list"></i></a>
                                    <a href="#" data-toggle="modal" data-target="#modal_edit_recepient_{{ $otherDonation->id }}" class="btn bg-info-600 badge-icon rounded-round" title="Kemaskini"><i class="icon-pencil"></i></a>
                                    <input type="hidden" name="_method" value="DELETE">
                                    {{ csrf_field() }}
                                    <button type="submit" id="confirm" class="btn bg-danger-600 badge-icon rounded-round" title="Hapus"><i class="icon-trash"></i></button>

                                </form>
                            </td>
                        </tr>

                    @endforeach
                </tbody>
            </table>
            {{-- END Index Recepient --}}
<hr>

            {{-- Index Dependent --}}
            {{-- <div class="card-body">
                <h6>Maklumat Tanggungan</h6>
            </div>

            @foreach ($recepient->dependents as $dependent)
            <table class="table table-xs">
                <tbody>

                    <td><button type="button" class="btn bg-blue-600 btn-labeled btn-labeled-left btn-sm" data-toggle="modal" data-placeholder="Tambah Bantuan" data-target="#modal_add_dependent_{{ $dependent->id }}"><b><i class="icon-plus3"></i></b>{{ $dependent->nama }} ({{ $dependent->hubungan }})</button></td>
                        @foreach ($dependent->dependentOtherDonations as $dependentOtherDonation)
                        <tr>
                            <td style="width:25%">{{ $dependentOtherDonation->nama }}</td>
                            <td style="width:13%">
                                @if(!empty($dependentOtherDonation->tarikh_serahan))
                                    {{ date('d-m-Y', strtotime($dependentOtherDonation->tarikh_serahan)) }}
                                @else
                                    -
                                @endif
                            </td>

                            <td style="width:15%"><span class="badge badge-success">Aktif</span></td>
                            <td style="width:15%">
                                <form class="delete" action="/bantuan_khas/tanggungan/destroy/{{ $dependentOtherDonation->id }}" method="POST">
                                    <a href="#" data-toggle="modal" data-target="#dependentOtherDonation-{{ $dependentOtherDonation->id }}" class="btn bg-primary-600 badge-icon rounded-round" title="Papar"><i class="icon-list"></i></a>
                                    <a href="#" data-toggle="modal" data-target="#modal_edit_dependent_{{ $dependentOtherDonation->id }}" class="btn bg-info-600 badge-icon rounded-round" title="Kemaskini"><i class="icon-pencil"></i></a>

                                    <input type="hidden" name="_method" value="DELETE">
                                    {{ csrf_field() }}
                                    <button type="submit" id="confirm" class="btn bg-danger-600 badge-icon rounded-round delete" title="Padam"><i class="icon-trash"></i></button>

                                </form>
                            </td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
             @endforeach --}}
            {{-- END Index Dependent --}}



            {{-- @if($errors->any())
                <script>
                $(function() {
                    id = {{ $dependent->id }};
                    $('#modal_add_dependent_' + id).modal({
                        show: true
                    });
                });
                </script>
            @endif --}}

            <!-- Modal with modal_add_donation_recepient -->
            <div id="modal_recepient_add" class="modal fade" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Tambah Bantuan Khas</h5>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <form action="/bantuan_khas/create/{{ $recepient->no_kp }}" method="post" class="form-horizontal">
                            @csrf
                            <div class="modal-body">
                                <td><span type="text" class="btn bg-blue-600 btn-labeled btn-labeled-left btn-sm"><b><i class="icon-user"></i></b> {{ $recepient->profile->nama }} ({{ $recepient->no_kp }})</span></td>
                                <br>
                                    <span class="muted">Tanda </span> <span class="text-danger">*</span> <span class="muted">wajib di isi. </span>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-form-label col-sm-3">Nama Bantuan <span class="text-danger">*</span></label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" name="nama">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-3">Kategori Bantuan <span class="text-danger">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="kategori" data-placeholder="Pilih Kategori" class="form-control form-control-select2 @if ($errors->has('kategori')) border-danger @endif" data-fouc>
                                            <option></option>
                                            @foreach ($categories as $category)
                                            <option value="{{ $category->id }}" @if(old('category') == $category->id) selected @endif>{{ $category->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-3">Tanggungan (Menerima)</label>
                                    <div class="col-sm-9">
                                        <select name="dependent" data-placeholder="Pilih" class="form-control form-control-select2 @if ($errors->has('dependent')) border-danger @endif" data-fouc>
                                            <option></option>
                                            @foreach ($dependents as $dependent)
                                            <option value="{{ $dependent->id }}" @if(old('dependent')==$dependent->id) selected @endif>{{ $dependent->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-3">Tarikh Serahan <span class="text-danger">*</span></label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="date" name="tarikh_serahan">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-3">Dokumen </label>
                                    <div class="col-sm-9">
                                        <div class="form-group mb-3 mb-md-2">
                                            <div class="form-check form-check-inline form-check-right">
                                                <label class="form-check-label">
                                                    Salinan KP
                                                    <input type="checkbox" class="form-check-input-styled" name="salinan_kp" value="1" data-fouc>
                                                </label>
                                            </div>

                                            <div class="form-check form-check-inline form-check-right">
                                                <label class="form-check-label">
                                                    KP1
                                                    <input type="checkbox" class="form-check-input-styled" name="kp1" value="1" data-fouc>
                                                </label>
                                            </div>

                                            <div class="form-check form-check-inline form-check-right">
                                                <label class="form-check-label">
                                                    KP2
                                                    <input type="checkbox" class="form-check-input-styled" name="kp2" value="1" data-fouc>
                                                </label>
                                            </div>

                                            <div class="form-check form-check-inline form-check-right">
                                                <label class="form-check-label">
                                                    Slip Gaji
                                                    <input type="checkbox" class="form-check-input-styled" name="slip_gaji" value="1" data-fouc>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-3">Nota Tambahan</label>
                                    <div class="col-sm-9">
                                        <textarea rows="3" cols="5" name="sebab" class="form-control" placeholder="Masukkan nota tambahan anda di sini">{{ old('nota') }}</textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-3">Status</label>
                                    <div class="col-sm-9">
                                        <select name="status" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                            <option value="Aktif" @if(old('status')=='Aktif' ) selected @endif>Aktif</option>
                                            <option value="Tidak Aktif" @if(old('status')=='Tidak Aktif' ) selected @endif>Tidak Aktif</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn bg-primary">Simpan</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <!-- /modal with modal_add_donation_recepient -->

            <!-- Modal with modal_add_donation_dependent -->
            {{-- @foreach ($recepient->dependents as $dependent)
                <div id="modal_add_dependent_{{ $dependent->id }}" class="modal fade" tabindex="-1">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Tambah Bantuan Khas</h5>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>

                            <form action="/bantuan_khas/tanggungan/create/{{ $recepient->no_kp }}" method="post" class="form-horizontal">
                                @csrf
								<div class="modal-body">
                                    <td><span type="text" class="btn bg-blue-600 btn-labeled btn-labeled-left btn-sm"><b><i class="icon-user"></i></b> {{ $dependent->nama }} ({{ $recepient->no_kp }})</span></td>
                                    <br>
                                    <span class="muted">Tanda </span> <span class="text-danger">*</span> <span class="muted">wajib di isi. </span>
                                    <hr>
                                    <input type="hidden" name="dependentId" value="{{$dependent->id}}">

									<div class="form-group row">
                                        <label class="col-form-label col-sm-3">Nama Bantuan <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
											<input class="form-control" type="text" name="nama">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-sm-3">Kategori Bantuan <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <select name="kategori" data-placeholder="Pilih Kategori" class="form-control form-control-select2 @if ($errors->has('kategori')) border-danger @endif" data-fouc>
                                                <option></option>
                                                @foreach ($categories as $category)
                                                <option value="{{ $category->id }}" @if(old('category') == $category->id) selected @endif>{{ $category->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

									<div class="form-group row">
										<label class="col-form-label col-sm-3">Tarikh Serahan <span class="text-danger">*</span></label>
										<div class="col-sm-9">
											<input class="form-control" type="date" name="tarikh_serahan">
										</div>
                                    </div>

                                    <div class="form-group row">
										<label class="col-form-label col-sm-3">Dokumen </label>
										<div class="col-sm-9">
                                            <div class="form-group mb-3 mb-md-2">
                                                <div class="form-check form-check-inline form-check-right">
                                                    <label class="form-check-label">
                                                        Salinan KP
                                                        <input type="checkbox" class="form-check-input-styled" name="salinan_kp" value="1" data-fouc>
                                                    </label>
                                                </div>

                                                <div class="form-check form-check-inline form-check-right">
                                                    <label class="form-check-label">
                                                        KP1
                                                        <input type="checkbox" class="form-check-input-styled" name="kp1" value="1" data-fouc>
                                                    </label>
                                                </div>

                                                <div class="form-check form-check-inline form-check-right">
                                                    <label class="form-check-label">
                                                        KP2
                                                        <input type="checkbox" class="form-check-input-styled" name="kp2" value="1" data-fouc>
                                                    </label>
                                                </div>

                                                <div class="form-check form-check-inline form-check-right">
                                                    <label class="form-check-label">
                                                        Slip Gaji
                                                        <input type="checkbox" class="form-check-input-styled" name="slip_gaji" value="1" data-fouc>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-sm-3">Nota Tambahan:</label>
										<div class="col-sm-9">
                                            <textarea rows="5" cols="5" name="sebab" class="form-control" placeholder="Masukkan nota tambahan anda di sini">{{ old('nota') }}</textarea>
										</div>
                                    </div>

									<div class="form-group row">
                                        <label class="col-form-label col-sm-3">Status</label>
                                        <div class="col-sm-9">
                                            <select name="status" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                                <option value="Aktif" @if(old('status')=='Aktif' ) selected @endif>Aktif</option>
                                                <option value="Tidak Aktif" @if(old('status')=='Tidak Aktif' ) selected @endif>Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
                                    <button type="submit" class="btn bg-primary">Simpan</button>
                                </div>
                            </form>


						</div>
					</div>
                </div>
            @endforeach --}}
            <!-- /modal with modal_add_donation_dependent -->


            {{-- Modal : View Recepient Other Donation --}}
            @foreach ($recepient->otherDonations as $otherDonation)
            <div class="modal fade" id="recepientOtherDonation-{{ $otherDonation->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <h6 class="modal-title">Maklumat Bantuan Khas (Penerima)</h6>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="card">
                            <div class="card-body">

                                <table class="table table-sm">
                                        <tr>
                                            <td style="width: 15%; text-align: right">Nama Bantuan</td>
                                            <td style="width: 5%">:</td>
                                            <td>{{ $otherDonation->nama }}</td>
                                        </tr>

                                        <tr>
                                            <td style="width: 15%; text-align: right">Tanggungan</td>
                                            <td style="width: 5%">:</td>
                                            @if(!empty($otherDonation->dependent_id))
                                            <td>{{ $otherDonation->dependent->nama }}</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td style="width: 40%; text-align: right">Tarikh Serahan</td>
                                            <td style="width: 5%">:</td>
                                            <td>
                                                @if(!empty($otherDonation->tarikh_serahan))
                                                    {{ date('d-m-Y', strtotime($otherDonation->tarikh_serahan)) }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="width: 15%; text-align: right">Dokumen</td>
                                            <td style="width: 5%">:</td>
                                            <td>


                                            <div class="form-check form-check-inline form-check-right">
                                                <label class="form-check-label">
                                                    Salinan KP
                                                    <input type="checkbox" class="form-check-input-styled" @if($otherDonation->salinan_kp == '1') checked @endif disabled data-fouc>
                                                </label>
                                            </div>

                                            <div class="form-check form-check-inline form-check-right">
                                                <label class="form-check-label">
                                                    KP1
                                                    <input type="checkbox" class="form-check-input-styled" @if($otherDonation->kp1 == '1') checked @endif disabled data-fouc>
                                                </label>
                                            </div>

                                            <div class="form-check form-check-inline form-check-right">
                                                <label class="form-check-label">
                                                    KP2
                                                    <input type="checkbox" class="form-check-input-styled" @if($otherDonation->kp2 == '1') checked @endif disabled data-fouc>
                                                </label>
                                            </div>

                                            <div class="form-check form-check-inline form-check-right">
                                                <label class="form-check-label">
                                                    Slip Gaji
                                                    <input type="checkbox" class="form-check-input-styled" @if($otherDonation->slip_gaji == '1') checked @endif disabled data-fouc>
                                                </label>
                                            </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="width: 15%; text-align: right">Kategori</td>
                                            <td style="width: 5%">:</td>
                                            <td>{{ $otherDonation->category->nama }}</td>
                                        </tr>

                                        <tr>
                                            <td style="width: 15%; text-align: right">Sebab</td>
                                            <td style="width: 5%">:</td>
                                            <td>{{ $otherDonation->sebab }}</td>
                                        </tr>

                                        <tr>
                                            <td style="width: 15%; text-align: right">Status</td>
                                            <td style="width: 5%">:</td>
                                            <td>{{ $otherDonation->status }}</td>
                                        </tr>
                                    </table>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            @endforeach


            {{-- Modal : View Recepient Other Donation --}}
             @foreach ($recepient->dependents as $dependent)
                @foreach ($dependent->dependentOtherDonations as $dependentOtherDonation)
                    <div class="modal fade" id="dependentOtherDonation-{{ $dependentOtherDonation->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header bg-primary">
                                    <h6 class="modal-title">Maklumat Bantuan Khas (Tanggungan)</h6>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="card">
                                    <div class="card-body">

                                        <table class="table table-sm">
                                                <tr>
                                                    <td style="width: 15%; text-align: right">Nama Bantuan</td>
                                                    <td style="width: 5%">:</td>
                                                    <td>{{ $dependentOtherDonation->nama }}</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 40%; text-align: right">Tarikh Serahan</td>
                                                    <td style="width: 5%">:</td>
                                                    <td>
                                                        @if(!empty($dependentOtherDonation->tarikh_serahan))
                                                            {{ date('d-m-Y', strtotime($dependentOtherDonation->tarikh_serahan)) }}
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width: 15%; text-align: right">Dokumen</td>
                                                    <td style="width: 5%">:</td>
                                                    <td>


                                                        <div class="form-check form-check-inline form-check-right">
                                                            <label class="form-check-label">
                                                                Salinan KP
                                                                <input type="checkbox" class="form-check-input-styled" @if($dependentOtherDonation->salinan_kp == '1') checked @endif disabled data-fouc>
                                                            </label>
                                                        </div>

                                                        <div class="form-check form-check-inline form-check-right">
                                                            <label class="form-check-label">
                                                                KP1
                                                                <input type="checkbox" class="form-check-input-styled" @if($dependentOtherDonation->kp1 == '1') checked @endif disabled data-fouc>
                                                            </label>
                                                        </div>

                                                        <div class="form-check form-check-inline form-check-right">
                                                            <label class="form-check-label">
                                                                KP2
                                                                <input type="checkbox" class="form-check-input-styled" @if($dependentOtherDonation->kp2 == '1') checked @endif disabled data-fouc>
                                                            </label>
                                                        </div>

                                                        <div class="form-check form-check-inline form-check-right">
                                                            <label class="form-check-label">
                                                                Slip Gaji
                                                                <input type="checkbox" class="form-check-input-styled" @if($dependentOtherDonation->slip_gaji == '1') checked @endif disabled data-fouc>
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width: 15%; text-align: right">Kategori</td>
                                                    <td style="width: 5%">:</td>
                                                    <td>{{ $dependentOtherDonation->category->nama }}</td>
                                                </tr>

                                                <tr>
                                                    <td style="width: 15%; text-align: right">Sebab</td>
                                                    <td style="width: 5%">:</td>
                                                    <td>{{ $dependentOtherDonation->sebab }}</td>
                                                </tr>

                                                <tr>
                                                    <td style="width: 15%; text-align: right">Status</td>
                                                    <td style="width: 5%">:</td>
                                                    <td>{{ $dependentOtherDonation->status }}</td>
                                                </tr>
                                            </table>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
            @endforeach


           <!-- EDIT RECEPIENT DONATION
            modal_edit_donation_recepient -->
            @foreach ($recepient->otherDonations as $otherDonation)
            <div id="modal_edit_recepient_{{ $otherDonation->id }}" class="modal fade" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Kemaskini Bantuan Khas</h5>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <form action="/bantuan_khas/edit/{{ $otherDonation->id }}" method="post" class="form-horizontal">
                            @csrf
                            <div class="modal-body">
                                <td><span type="text" class="btn bg-blue-600 btn-labeled btn-labeled-left btn-sm"><b><i class="icon-user"></i></b> {{ $recepient->profile->nama }} ({{ $recepient->no_kp }})</span></td>
                                <br>
                                <span class="muted">Tanda </span> <span class="text-danger">*</span> <span class="muted">wajib di isi. </span>
                                <hr>
                                <div class="form-group row">
                                    <label class="col-form-label col-sm-3">Nama Bantuan <span class="text-danger">*</span></label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" name="nama" value="{{ old('nama', $otherDonation->nama) }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-3">Kategori Bantuan <span class="text-danger">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="kategori" data-placeholder="Pilih Kategori" class="form-control form-control-select2 @if ($errors->has('kategori')) border-danger @endif" data-fouc>
                                            <option>-Sila Pilih-</option>
                                            @foreach ($categories as $category)
                                            <option value="{{ $category->id }}" @if(old('category', $otherDonation->category_id) == $category->id) selected @endif>{{ $category->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-3">Tanggungan (Menerima)</label>
                                    <div class="col-sm-9">
                                        <select name="dependent" data-placeholder="Pilih" class="form-control form-control-select2 @if ($errors->has('dependent')) border-danger @endif" data-fouc>
                                            <option></option>
                                            @foreach ($dependents as $dependent)
                                            <option value="{{ $dependent->id }}" @if(old('dependent', $otherDonation->dependent_id )==$dependent->id) selected @endif>{{ $dependent->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-3">Tarikh serahan <span class="text-danger">*</span></label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="date" name="tarikh_serahan" value="{{ old( 'tarikh_serahan', $otherDonation->tarikh_serahan) }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-3">Dokumen </label>
                                    <div class="col-sm-9">
                                        <div class="form-group mb-3 mb-md-2">

                                            <div class="form-check form-check-inline form-check-right">
                                                <label class="form-check-label">
                                                    Salinan KP
                                                    <input type="checkbox" class="form-check-input-styled" name="salinan_kp" value="1" @if($otherDonation->salinan_kp == true) checked @endif data-fouc>
                                                </label>
                                            </div>

                                            <div class="form-check form-check-inline form-check-right">
                                                <label class="form-check-label">
                                                    KP1
                                                    <input type="checkbox" class="form-check-input-styled" name="kp1" value="1" @if($otherDonation->kp1 == true) checked @endif data-fouc>
                                                </label>
                                            </div>

                                            <div class="form-check form-check-inline form-check-right">
                                                <label class="form-check-label">
                                                    KP2
                                                    <input type="checkbox" class="form-check-input-styled" name="kp2" value="1" @if($otherDonation->kp2 == true) checked @endif data-fouc>
                                                </label>
                                            </div>

                                            <div class="form-check form-check-inline form-check-right">
                                                <label class="form-check-label">
                                                    Slip Gaji
                                                    <input type="checkbox" class="form-check-input-styled" name="slip_gaji" value="1" @if($otherDonation->slip_gaji == true) checked @endif data-fouc>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-3">Nota</label>
                                    <div class="col-sm-9">
                                        <textarea rows="3" cols="5" name="nota" class="form-control" placeholder="Masukkan nota tambahan anda di sini">{{ old('nota', $otherDonation->sebab ) }}</textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-form-label col-sm-3">Status</label>
                                    <div class="col-sm-9">
                                        <select name="status" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                            <option value="Aktif" @if(old('status', $otherDonation->status)=='Aktif' ) selected @endif>Aktif</option>
                                            <option value="Tidak Aktif" @if(old('status',$otherDonation->status)=='Tidak Aktif' ) selected @endif>Tidak Aktif</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
                                <button type="submit" class="btn bg-primary">Kemaskini</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            @endforeach
            <!-- EDIT
            /modal_add_donation_recepient -->

            <!-- EDIT DEPENDENT DONATION
            modal_edit_donation_dependent -->
            {{-- @foreach ($recepient->dependents as $dependent)
                @foreach ($dependent->dependentOtherDonations as $dependentOtherDonation)
                    <div id="modal_edit_dependent_{{ $dependentOtherDonation->id }}" class="modal fade" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Kemaskini Bantuan Khas</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <form action="/bantuan_khas/edit/tanggungan/{{ $dependentOtherDonation->id }}" method="post" class="form-horizontal">
                                    @csrf
                                    <div class="modal-body">
                                        <td><span type="text" class="btn bg-blue-600 btn-labeled btn-labeled-left btn-sm"><b><i class="icon-user"></i></b> {{ $dependent->nama }} ({{ $dependent->no_kp }})</span></td>
                                        <br>
                                        <span class="muted">Tanda </span> <span class="text-danger">*</span> <span class="muted">wajib di isi. </span>
                                        <hr>
                                        <input type="hidden" name="dependentId" value="{{$dependent->id}}">
                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3">Nama Bantuan <span class="text-danger">*</span></label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" name="nama" value="{{ old('nama', $dependentOtherDonation->nama) }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3">Kategori Bantuan <span class="text-danger">*</span></label>
                                            <div class="col-sm-9">
                                                <select name="kategori" data-placeholder="Pilih Kategori" class="form-control form-control-select2 @if ($errors->has('kategori')) border-danger @endif" data-fouc>
                                                    <option>-Sila Pilih-</option>
                                                    @foreach ($categories as $category)
                                                    <option value="{{ $category->id }}" @if(old('category', $dependentOtherDonation->category_id) == $category->id) selected @endif>{{ $category->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3">Tarikh Serahan <span class="text-danger">*</span></label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="date" name="tarikh_serahan" value="{{ old('tarikh_serahan', $dependentOtherDonation->tarikh_serahan )}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3">Dokumen </label>
                                            <div class="col-sm-9">
                                                <div class="form-group mb-3 mb-md-2">

                                                    <div class="form-check form-check-inline form-check-right">
                                                        <label class="form-check-label">
                                                            Salinan KP
                                                            <input type="checkbox" class="form-check-input-styled" name="salinan_kp" value="1" @if($dependentOtherDonation->salinan_kp == true) checked @endif data-fouc>
                                                        </label>
                                                    </div>

                                                    <div class="form-check form-check-inline form-check-right">
                                                        <label class="form-check-label">
                                                            KP1
                                                            <input type="checkbox" class="form-check-input-styled" name="kp1" value="1" @if($dependentOtherDonation->kp1 == true) checked @endif data-fouc>
                                                        </label>
                                                    </div>

                                                    <div class="form-check form-check-inline form-check-right">
                                                        <label class="form-check-label">
                                                            KP2
                                                            <input type="checkbox" class="form-check-input-styled" name="kp2" value="1" @if($dependentOtherDonation->kp2 == true) checked @endif data-fouc>
                                                        </label>
                                                    </div>

                                                    <div class="form-check form-check-inline form-check-right">
                                                        <label class="form-check-label">
                                                            Slip Gaji
                                                            <input type="checkbox" class="form-check-input-styled" name="slip_gaji" value="1" @if($dependentOtherDonation->slip_gaji == true) checked @endif data-fouc>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3">Nota</label>
                                            <div class="col-sm-9">
                                                <textarea rows="5" cols="5" name="nota" class="form-control" placeholder="Masukkan nota tambahan anda di sini">{{ old('nota', $dependentOtherDonation->sebab) }}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3">Status</label>
                                            <div class="col-sm-9">
                                                <select name="status" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                                    <option value="Aktif" @if(old('status', $dependentOtherDonation->status)=='Aktif' ) selected @endif>Aktif</option>
                                                    <option value="Tidak Aktif" @if(old('status', $dependentOtherDonation->status)=='Tidak Aktif' ) selected @endif>Tidak Aktif</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
                                        <button type="submit" class="btn bg-primary">Kemaskini</button>
                                    </div>
                                </form>


                            </div>
                        </div>
                    </div>
                @endforeach
            @endforeach --}}
            <!-- /modal_add_donation_dependent -->

        </div>

    </div>
</div>

@endsection

@section('script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>

    <script>
        $(".delete").on("submit", function(){
            return confirm("Adakah anda pasti untuk menghapus rekod?");
        });


//         $('.delete').click(function(event) {
//       var form =  $(this).closest("form");
//       var name = $(this).data("name");
//       event.preventDefault();
//       swal({
//           title: `Are you sure you want to delete ${name}?`,
//           text: "If you delete this, it will be gone forever.",
//           icon: "warning",
//           buttons: true,
//           dangerMode: true,
//       })
//       .then((willDelete) => {
//         if (willDelete) {
//           form.submit();
//         }
//       });
//   });
    </script>
@endsection
