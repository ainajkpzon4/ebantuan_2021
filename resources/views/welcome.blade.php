
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>eBantuan - Pusat Khidmat Rakyat (Penyelaras) - Dun Gombak Setia</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="../../../../global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="../../../../global_assets/js/main/jquery.min.js"></script>
	<script src="../../../../global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="../../../../global_assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="../../../../global_assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script src="assets/js/app.js"></script>
	<script src="../../../../global_assets/js/demo_pages/login.js"></script>
	<!-- /theme JS files -->

</head>

<body>
	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Content area -->
			<div class="content d-flex justify-content-center align-items-center">

				<!-- Login card -->
				{{-- <form class="login-form" action="index.html"> --}}
                    <form class="login-form" method="POST" action="{{ route('login') }}">
                        @csrf
					<div class="card mb-0">
                        <div class="text-center">
                            <div class="card-header bg-info">
                                <h5><b>PUSAT KHIDMAT RAKYAT (PENYELARAS)</b></h5>
                                <h6>DUN GOMBAK SETIA</h6>
                            </div>
                        </div>
						<div class="card-body">
							<div class="text-center mb-3">

                                <img src="{{ asset('imgs/jata.png') }}" width="120" height="140">
								{{-- <i class="icon-people icon-2x text-warning-400 border-warning-400 border-3 rounded-round p-3 mb-3 mt-1"></i> --}}
								<h6 class="mb-0">Login ke akaun anda</h6>
								{{-- <span class="d-block text-muted">Sila Masukkan ID dan Katalaluan anda</span> --}}
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="text" name="no_kp" id="no_kp" value="{{ old('no_kp') }}" class="form-control @error('no_kp') is-invalid @enderror" placeholder="No.Pengenalan" autofocus>
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
                                </div>
                                @error('no_kp')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Katalaluan">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
                                </div>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            </div>

							<div class="form-group d-flex align-items-center">
								{{-- <div class="form-check mb-0">
									<label class="form-check-label">
										<input type="checkbox" name="remember" class="form-input-styled" checked data-fouc>
										Remember
									</label>
								</div> --}}

								<a href="/password/reset" class="ml-auto">Lupa Kata laluan?</a>
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-dark btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
							</div>

							{{-- <div class="form-group text-center text-muted content-divider">
								<span class="px-2">or sign in with</span>
							</div>

							<div class="form-group text-center">
								<button type="button" class="btn btn-outline bg-indigo border-indigo text-indigo btn-icon rounded-round border-2"><i class="icon-facebook"></i></button>
								<button type="button" class="btn btn-outline bg-pink-300 border-pink-300 text-pink-300 btn-icon rounded-round border-2 ml-2"><i class="icon-dribbble3"></i></button>
								<button type="button" class="btn btn-outline bg-slate-600 border-slate-600 text-slate-600 btn-icon rounded-round border-2 ml-2"><i class="icon-github"></i></button>
								<button type="button" class="btn btn-outline bg-info border-info text-info btn-icon rounded-round border-2 ml-2"><i class="icon-twitter"></i></button>
							</div> --}}

							{{-- <div class="form-group text-center text-muted content-divider">
								<span class="px-2">Login kali pertama?</span>
							</div> --}}

							{{-- <div class="form-group">
								<a href="/register" class="btn btn-light btn-block">Pendaftaran Pengguna</a>
							</div> --}}

							{{-- <span class="form-text text-center text-muted">By continuing, you're confirming that you've read our <a href="#">Terms &amp; Conditions</a> and <a href="#">Cookie Policy</a></span> --}}
						</div>
					</div>
				</form>
                <!-- /login card -->


			</div>
			<!-- /content area -->
        </div>
	</div>
</body>
</html>
