@extends('layouts.backend')

{{-- @section('title')
    Urusetia
@endsection --}}

@section('top_button')
    <a href="/penerima/status/1" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Penerima</span></a>
    <a href="/tanggungan/search/{{ $recepient->no_kp }}" class="btn btn-link btn-float text-default"><i class="icon-plus-circle2 text-primary"></i> <span>Tambah Tanggungan</span></a>
@endsection

@section('breadcrumb')
    <a href="/home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="/tanggungan" class="breadcrumb-item">Senarai Tanggungan</a>
    <span class="breadcrumb-item active">Tambah Tanggungan</span>
@endsection

@section('content')

<div class="card">

    <div class="card-body">
        <ul class="nav nav-tabs nav-tabs-solid nav-justified rounded bg-light">
            <li class="nav-item"><a href="/penerima/show/{{ $recepient->no_kp}}" class="nav-link">Penerima</a></li>
            <li class="nav-item"><a href="/tanggungan/{{ $recepient->no_kp}}" class="nav-link rounded-left active">Tanggungan</a></li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link rounded-right dropdown-toggle" data-toggle="dropdown">Bantuan</a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="/bantuan/{{ $recepient->no_kp }}" class="dropdown-item">Bantuan IPR Selangor</a>
                    <a href="/bantuan_khas/{{ $recepient->no_kp }}" class="dropdown-item">Bantuan Khas</a>
                </div>
            </li>
        </ul>
        <span class="text-muted">Tanda (</span><span class="text-danger"> *</span><span class="text-muted"> ) wajib diisi.</span>
        <br><br>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="tanggungan">

                <form method="post" enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">

                            <fieldset>
                                <legend class="font-weight-semibold"><i class="icon-reading mr-2"></i> Tambah Tanggungan</legend>

                                <div class="form-group">
                                    <label>No. Kad Pengenalan / Mykid Anak: <span class="text-danger">*</span></label>
                                    <input type="text" name="no_kp" value="{{ old('no_kp',$dependentId) }}" maxlength="12" size="12" class="form-control" placeholder="901013101234" readonly>
                                    <input type="hidden" id="no_kp_penerima" name="no_kp_penerima" value="{{ $recepientId }}"
                                </div>

                                <div class="form-group">
                                    <label>Nama: <span class="text-danger">*</span></label>
                                    <input type="text" name="nama" value="{{ old('nama') }}" class="form-control" placeholder="Mohd Roslee bin Ahmad">
                                </div>

                                <div class="form-group">
                                    <label>Hubungan: <span class="text-danger">*</span></label>
                                    <select name="hubungan" data-placeholder="Pilih hubungan dengan penerima" class="form-control form-control-select2" data-fouc>
                                        <option></option>
                                        <option value="Isteri" @if(old('hubungan')=='Isteri') selected @endif>Isteri</option>
                                        <option value="Suami" @if(old('hubungan')=='Suami') selected @endif>Suami</option>
                                        <option value="Anak Kandung" @if(old('hubungan')=='Anak Kandung') selected @endif>Anak Kandung</option>
                                        <option value="Anak Angkat" @if(old('hubungan')=='Anak Angkat') selected @endif>Anak Angkat</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Pilih jantina: <span class="text-danger">*</span></label>
                                    <select name="jantina" data-placeholder="Pilih jantina" class="form-control form-control-select2" data-fouc>
                                        <option></option>
                                        <option value="L" @if(old('jantina', $jantina)=='L') selected @endif>LELAKI</option>
                                        <option value="P" @if(old('jantina', $jantina)=='P') selected @endif>PEREMPUAN</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Umur (Tahun):</label>
                                    <input type="text" name="umur" value="{{ old('umur') }}" class="form-control" placeholder="Contoh : 16">
                                </div>
                            </fieldset>
                        </div>

                        <div class="col-md-6">
                            <fieldset>
                                <legend class="font-weight-semibold"><i class="icon-stack mr-2"></i> Maklumat Lain </legend>
                                <div class="form-group">
                                    <label>Kad OKU:</label>
                                    <select name="kad_oku" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                        <option></option>
                                        <option value="Y">YA</option>
                                        <option value="T">TIDAK</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Pendidikan (sedang diikuti) <span class="text-danger">*</span></label>
                                    <select id="pendidikan" name="pendidikan" data-placeholder="Sila Pilih" class="form-control form-control-select2" onchange="hideShowEducation()" data-fouc>
                                        <option></option>
                                        @foreach ($refEducations as $refEducation)
                                            <option value="{{ $refEducation->id }}" @if(old('pendidikan') == $refEducation->id) selected @endif>{{ $refEducation->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div style="display: none" id="jurusan_semesta">
                                    <div class="form-group">
                                        <label>Jurusan <span class="text-danger">*</span></label>
                                        <input type="text" name="jurusan" value="{{ old('jurusan') }}" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label>Semesta <span class="text-danger">*</span></label>
                                        <input type="text" name="semesta" value="{{ old('semesta') }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Nota Tambahan:</label>
                                    <textarea rows="5" name="nota" cols="5" class="form-control" placeholder="Masukkan nota tambahan anda di sini">{{ old('nota')}}</textarea>
                                </div>
                        </div>

                    </div>

                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Simpan <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>
function hideShowEducation() {
  var edu = document.getElementById("pendidikan").value;
  var jurusan = document.getElementById("jurusan");
  var semesta = document.getElementById("semesta");

  if (edu == "4" || edu == "5" || edu == "6" || edu == "7" || edu == "8") {
    jurusan_semesta.style.display = "block";
  } else {
    jurusan_semesta.style.display = "none";
  }
}

function checkId(nokp){
    if(nokp.length < 12 ){
        // alert(nokp);
    }else if(nokp.length > 12 ){
        alert('lebih');
    }else if(nokp.length == 12){
        var recepientId = document.getElementById("no_kp_penerima").value;
        // alert(no_kp_penerima);
        window.location.href = "/tanggungan/search/" + nokp + "/" + recepientId;
    }
}

</script>

@endsection
