@extends('layouts.backend')

{{-- @section('title')
    Urusetia
@endsection --}}

@section('top_button')
    <a href="/penerima/status/1" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Penerima</span></a>
    <a href="/penerima/create" class="btn btn-link btn-float text-default"><i class="icon-plus-circle2 text-primary"></i> <span>Tambah Penerima</span></a>
@endsection

@section('breadcrumb')
    <a href="/home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="/penerima/status/1" class="breadcrumb-item">Senarai Penerima</a>
    {{-- <span class="breadcrumb-item active">Senarai Pemohon</span> --}}
@endsection

@section('content')

{{-- <div class="card">
    <div class="card-body">
        <div class="alpha-primary">
            <h5>Maklumat Penerima</h5>
        </div>
            <table class="table table-xs">
                <thead>
                    <tr>
                        <th style="width:35%">Nama</th>
                        <th style="width:20%">Nokp</th>
                        <th style="width:15%">Status</th>
                        <th style="width:10%">Bantuan</th>
                        <th style="width:20%">Tindakan</th>
                    </tr>
                </thead>
                <tbody>

                <tr>
                    <td><a href="">{{ $recepient->profile->nama }}</a></td>
                    <td>{{ $recepient->no_kp }}</a></td>
                    <td><span class="badge badge-success">Aktif</span></td>
                     <td>
                        <a href="#" data-toggle="modal" data-target="#tambahBantuanAdd" title="Tambah Bantuan"><i class="icon-list"></i></a>

                    </td>


                    <td>
                        <form class="delete" action="/applicant/profile/treatment/destroy/{{ $recepient->id }}" method="POST">

                            <a href="#" data-toggle="modal" data-target="#perubatanModalEdit-{{ $recepient->id }}" class="btn bg-info-600 badge-icon rounded-round" title="Kemaskini"><i class="icon-pencil"></i></a>
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}
                            <button type="submit" id="confirm" class="btn bg-danger-600 badge-icon rounded-round" title="Padam-b"><i class="icon-trash"></i></button>

                        </form>
                    </td>
                </tr>

                </tbody>
            </table>
    </div>
</div> --}}


{{-- Maklumat basic penerima --}}
{{-- <div class="card">
    <div class="card-header bg-primary header-elements-inline">
        <h5 class="card-title">Maklumat Penerima</h5>
    </div>
    <div class="table-responsive">
        <table class="table table-sm">
            <tr>
                <td style="width: 40%; text-align: right">Nama</td>
                <td style="width: 5%">:</td>
                <td>{{ $recepient->profile->nama }}</td>
            </tr>
            <tr>
                <td style="width: 15%; text-align: right">No.Kad Pengenalan</td>
                <td style="width: 5%">:</td>
                <td>{{ $recepient->no_kp }}</td>
            </tr>
            <tr>
                <td style="width: 15%; text-align: right">Alamat</td>
                <td style="width: 5%">:</td>
                <td>{{ $recepient->profile->alamat }} , {{ $recepient->profile->bandar }}, {{ $recepient->profile->negeri }}</td>
            </tr>
            <tr>
                <td style="width: 15%; text-align: right">DM</td>
                <td style="width: 5%">:</td>
                <td>{{ $recepient->profile->Dm->nama }}</td>
            </tr>


        </table>
    </div>
</div> --}}


<div class="card-body">
    <ul class="nav nav-tabs nav-tabs-solid nav-justified rounded bg-light">
        <li class="nav-item"><a href="/penerima/edit/{{ $recepient->no_kp}}" class="nav-link">Penerima</a></li>
        <li class="nav-item"><a href="/tanggungan/{{ $recepient->no_kp}}" class="nav-link rounded-left active" data-toggle="tab">Tanggungan</a></li>
        <li class="nav-item dropdown">
            <a href="#" class="nav-link rounded-right dropdown-toggle" data-toggle="dropdown">Bantuan</a>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="/bantuan/{{ $recepient->no_kp }}" class="dropdown-item">Bantuan IPR Selangor</a>
                <a href="/bantuan_khas/{{ $recepient->no_kp }}" class="dropdown-item">Bantuan Khas</a>
            </div>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane fade show active" id="tanggungan">

            <form method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">

                                <fieldset>
                                    <legend class="font-weight-semibold"><i class="icon-reading mr-2"></i> Tambah Tanggungan</legend>

                                    <div class="form-group">
                                        <label>No. Kad Pengenalan / Mykid :</label>
                                        <input type="number" name="no_kp" value="{{ old('no_kp') }}" class="form-control no_kp" placeholder="901013101234" onkeyup="checkId(this.value)" autocomplete="off">
                                        <input type="hidden" id="no_kp_penerima" name="no_kp_penerima" value="{{ $recepientId }}">
                                    </div>
                                    {{-- div lain dekat create --}}
                    </form>


        </div>
    </div>


<div class="card">
    {{-- <div class="card-header header-elements-inline">
        <div class="header-elements">
            <div class="list-icons">
                <a href="{{ URL::previous() }}"><i class="icon-arrow-left8"></i></a>

            </div>
        </div>
    </div> --}}
    {{-- <div class="card-body">

        <div class="tab-content">
            <div class="tab-pane fade show active" id="peribadi">

                        <form method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">

                                    <fieldset>
                                        <legend class="font-weight-semibold"><i class="icon-reading mr-2"></i> Tambah Tanggungan</legend>

                                        <div class="form-group">
                                            <label>No. Kad Pengenalan / Mykid :</label>
                                            <input type="text" name="no_kp" value="{{ old('no_kp') }}" maxlength="12" size="12" class="form-control" placeholder="901013101234" onkeyup="checkId(this.value)">
                                            <input type="hidden" id="no_kp_penerima" name="no_kp_penerima" value="{{ $recepientId }}">
                                        </div>

                        </form>

            </div>

        </div>
    </div> --}}
</div>



@endsection

@section('script')

<script>
function checkId(nokp){

    $('.no_kp').keypress(function(e){
    if ( e.which == 13 ) return false;
    });

    if(nokp.length < 12 ){
        // alert(nokp);
    }else if(nokp.length > 12 ){
        alert('No.Kad Pengenalan/MyKid yang dimasukkan lebih 12 Digit');
        return false;
    }else if(nokp.length == 12){
        var recepientId = document.getElementById("no_kp_penerima").value;
        // alert(no_kp_penerima);
        window.location.href = "/tanggungan/create/" + nokp + "/" + recepientId;
    }
}

</script>

@endsection
