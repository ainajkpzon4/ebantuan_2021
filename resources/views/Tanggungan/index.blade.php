@extends('layouts.backend')

{{-- @section('title')
    Urusetia
@endsection --}}

@section('top_button')
    {{-- <a href="/penerima" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Penerima</span></a> --}}
    <a href="/tanggungan/search/{{ $recepientId }}" class="btn btn-link btn-float text-default"><i class="icon-plus-circle2 text-primary"></i> <span>Tambah Tanggungan</span></a>
@endsection

@section('breadcrumb')
    <a href="/home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="/penerima/status/1" class="breadcrumb-item">Senarai Penerima</a>
    {{-- <a href="/penerima/edit/{{ $recepientId }}" class="breadcrumb-item">Maklumat Penerima</a> --}}
    <span class="breadcrumb-item active">Maklumat Tanggungan</span>
@endsection

@section('content')

<div class="card">

    <div class="card-body">
        <ul class="nav nav-tabs nav-tabs-solid nav-justified rounded bg-light">
            <li class="nav-item"><a href="/penerima/edit/{{ $recepient->no_kp}}" class="nav-link">Penerima</a></li>
            <li class="nav-item"><a href="/tanggungan/{{ $recepient->no_kp}}" class="nav-link  rounded-left active">Tanggungan</a></li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link rounded-right dropdown-toggle" data-toggle="dropdown">Bantuan</a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="/bantuan/{{ $recepient->no_kp }}" class="dropdown-item">Bantuan IPR Selangor</a>
                    <a href="/bantuan_khas/{{ $recepient->no_kp }}" class="dropdown-item">Bantuan Khas</a>
                </div>
            </li>
        </ul>
        <div class="header-elements-inline">
            <h5>Maklumat Tanggungan</h5>
            {{-- <a href="/tanggungan/search/{{ $recepientId }}" class="btn btn-link text-default"><i class="icon-user-plus text-primary"></i> <span>Tambah Tanggungan</span></a> --}}
        </div>

        <table class="table table-xs">
            <thead class="bg-blue-600">
                <tr>
                    <th style="width:25%">Nama</th>
                    <th style="width:15%">Nokp</th>
                    <th style="width:15%">hubungan</th>
                    <th style="width:15%">Status</th>
                    {{-- <th style="width:15%">Bantuan</th> --}}
                    <th style="width:15%">Tindakan</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($dependents as $dependent)
            <tr>
                <td>{{ $dependent->nama }}</td>
                <td>{{ $dependent->no_kp }}</a></td>
                <td>{{ $dependent->hubungan }}</a></td>
                <td>
                    @if($dependent->status == 'Aktif')
                        <span class="badge badge-success">Aktif</span>
                    @elseif(($dependent->status == 'Tidak Aktif'))
                        <span class="badge badge-warning">Tidak Aktif</span>
                    @endif
                </td>
                {{-- <td>

                    <a href="#" data-toggle="modal" data-target="#bantuanListModal-{{ $dependent->id }}" title="Senarai Bantuan"><i class="icon-list"></i></a>
                </td> --}}

                <td>
                    <form class="delete" action="/tanggungan/destroy/{{ $dependent->id }}" method="POST">
                        <a href="#" data-toggle="modal" data-target="#dependentShowModal-{{ $dependent->id }}" class="btn bg-info-600 badge-icon rounded-round" title="Papar"><i class="icon-list"></i></a>
                        <a href="/tanggungan/edit/{{ $dependent->id }}" class="btn bg-info-600 badge-icon rounded-round" title="Kemaskini"><i class="icon-pencil7"></i></a>
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}
                        <button type="submit" id="confirm" class="btn bg-danger-600 badge-icon rounded-round" title="Hapus"><i class="icon-trash"></i></button>
                    </form>
                </td>
            </tr>

            @endforeach
            </tbody>
        </table>

    </div>

</div>
{{-- MODAL------------------------------------------------------------------------------------------------------- --}}

{{-- Modal : View Dependent --}}
@foreach ($dependents as $dependent)
<div class="modal fade" id="dependentShowModal-{{ $dependent->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h6 class="modal-title">Maklumat Tanggungan</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="card">
                <div class="card-body">

                     <table class="table table-sm">
                            <tr>
                                <td style="width: 40%; text-align: right">Nama</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $dependent->nama }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">No.Kad Pengenalan</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $dependent->no_kp }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">Hubungan</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $dependent->hubungan }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">Jantina</td>
                                <td style="width: 5%">:</td>
                                <td>
                                    @if($dependent->jantina == 'L')
                                    Lelaki
                                    @elseif($dependent->jantina == 'P')
                                    Perempuan
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">Umur</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $dependent->umur }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">Kad OKU</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $dependent->kad_oku }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">Pendidikan (Sedang Diikuti)</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $dependent->refEducation->nama }}</td>
                            </tr>
                            @if($dependent->ref_education_id == '5' || $dependent->ref_education_id == '6' || $dependent->ref_education_id == '7' || $dependent->pendidikan == '8')
                            <tr>
                                <td style="width: 15%; text-align: right">Jurusan</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $dependent->jurusan }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">Semesta</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $dependent->semesta }}</td>
                            </tr>
                            @endif
                            <tr>
                                <td style="width: 15%; text-align: right">Nota</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $dependent->nota }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">Status</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $dependent->status }}</td>
                            </tr>
                        </table>

                </div>

            </div>

        </div>
    </div>
</div>
@endforeach

{{-- Modal : Edit Dependent --}}
{{-- @foreach ($dependents as $dependent)
<div class="modal fade" id="dependentEditModal-{{ $dependent->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h6 class="modal-title">Kemaskini Maklumat Tanggungan</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="card">
                <div class="card-body">
                            <span class="text-muted">Tanda (</span><span class="text-danger"> *</span><span class="text-muted"> ) wajib diisi.</span>

                   <form action="/tanggungan/edit/{{ $dependent->id }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <fieldset>
                                <legend class="font-weight-semibold"><i class="icon-reading mr-2"></i> Kemaskini Tanggungan</legend>

                                <div class="form-group">
                                    <label>No. Kad Pengenalan / Mykid Anak:</label>
                                    <input type="text" name="no_kp" value="{{ old('no_kp',$dependent->no_kp) }}" maxlength="12" size="12" class="form-control" placeholder="901013101234" readonly>
                                    <input type="hidden" id="no_kp_penerima" name="no_kp_penerima" value="{{ $recepientId }}">
                                </div>

                                <div class="form-group">
                                    <label>Nama:</label>
                                    <input type="text" name="nama" value="{{ old('nama', $dependent->nama) }}" class="form-control" placeholder="Mohd Roslee bin Ahmad">
                                </div>

                                <div class="form-group">
                                    <label>Hubungan:</label>
                                    <select name="hubungan" data-placeholder="Pilih hubungan dengan penerima" class="form-control form-control-select2" data-fouc>
                                        <option></option>
                                        <option value="Isteri" @if(old('hubungan', $dependent->hubungan)=='Isteri') selected @endif>Isteri</option>
                                        <option value="Suami" @if(old('hubungan', $dependent->hubungan)=='Suami') selected @endif>Suami</option>
                                        <option value="Anak Kandung" @if(old('hubungan', $dependent->hubungan)=='Anak Kandung') selected @endif>Anak Kandung</option>
                                        <option value="Anak Angkat" @if(old('hubungan', $dependent->hubungan)=='Anak Angkat') selected @endif>Anak Angkat</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Pilih jantina:</label>
                                    <select name="jantina" data-placeholder="Pilih jantina" class="form-control form-control-select2" data-fouc>
                                        <option></option>
                                        <option value="L" @if(old('jantina', $dependent->jantina)=='L') selected @endif>LELAKI</option>
                                        <option value="P" @if(old('jantina', $dependent->jantina)=='P') selected @endif>PEREMPUAN</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Umur (Tahun):</label>
                                    <input type="text" name="umur" value="{{ old('umur', $dependent->umur) }}" class="form-control" placeholder="Contoh : 16">

                                </div>
                            </fieldset>
                        </div>

                        <div class="col-md-6">

                            <fieldset>
                                <legend class="font-weight-semibold"><i class="icon-stack mr-2"></i> Maklumat Lain</legend>
                                <div class="form-group">
                                    <label>Kad OKU:</label>
                                    <select name="kad_oku" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                        <option></option>
                                        <option value="Y" @if(old('kad_oku', $dependent->kad_oku) == 'Y') selected @endif>YA</option>
                                        <option value="T" @if(old('kad_oku', $dependent->kad_oku) == 'T') selected @endif>TIDAK</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Pendidikan (sedang diikuti) <span class="text-danger">*</span></label>
                                    <select id="pendidikan" name="pendidikan" data-placeholder="Sila Pilih" class="form-control form-control-select2" onchange="hideShowEducation()" data-fouc>
                                        <option></option>
                                        @foreach ($refEducations as $refEducation)
                                            <option value="{{ $refEducation->id }}" @if(old('pendidikan') == $refEducation->id) selected @endif>{{ $refEducation->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div style="display: none" id="jurusan_semesta">
                                    <div class="form-group">
                                        <label>Jurusan <span class="text-danger">*</span></label>
                                        <input type="text" name="jurusan" value="{{ old('jurusan') }}" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label>Semesta <span class="text-danger">*</span></label>
                                        <input type="text" name="semesta" value="{{ old('semesta') }}" class="form-control">
                                    </div>
                                </div>
                                    <div class="form-group">
                                    <label>Nota Tambahan:</label>
                                    <textarea rows="5" name="nota" cols="5" class="form-control" placeholder="Masukkan nota tambahan anda di sini">{{ old('nota',  $dependent->nota)}}</textarea>
                                </div>

                                <div class="form-group">
                                    <label>Status:</label>
                                    <select name="status" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                        <option></option>
                                        <option value="Aktif" @if(old('status', $dependent->status) == 'Aktif') selected @endif>Aktif</option>
                                        <option value="Tidak Aktif" @if(old('status', $dependent->status) == 'Tidak Aktif') selected @endif>Tidak Aktif</option>
                                    </select>
                                </div>
                        </div>

                    </div>

                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Kemaskini <i class="icon-paperplane ml-2"></i></button>
                    </div>
                </form>
                </div>

            </div>

        </div>
    </div>

</div>
@endforeach --}}

@endsection



@section('script')

    <script>
        $(".delete").on("submit", function(){
            return confirm("Adakah anda pasti untuk menghapus rekod?");
        });
    </script>
@endsection


{{-- Testing Beza Code --}}
