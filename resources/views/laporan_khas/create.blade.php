@extends('layouts.backend')

@section('title')
    Laporan
@endsection

{{-- @section('top_button')
    <a href="/rujukan/bantuan" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Bantuan IPR Sel.</span></a>
    <a href="/rujukan/bantuan/create" class="btn btn-link btn-float text-default"><i class="icon-plus-circle2 text-primary"></i> <span>Tambah Bantuan</span></a>
@endsection --}}

@section('breadcrumb')
    <a href="/home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <span class="breadcrumb-item active">Laporan Bantuan Khas</span>
@endsection

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Laporan</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form method="POST" enctype="multipart/form-data">
                @csrf
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Carian</legend>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2 text-right">Tahun</label>
                            <div class="col-lg-6">
                                <input name="tahun" type="text" class="form-control" placeholder="" value="{{ old('tahun') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2 text-right">Kategori Bantuan</label>
                            <div class="col-lg-6">
                                <select name="donation" class="form-control form-control-lg">
                                    <option>Pilih Kategori</option>
                                    @foreach ($catagories as $item)
                                        <option value="{{ $item->id }}">{{ $item->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{-- <label class="col-form-label col-lg-2">Semua</label> --}}
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input name="all" type="checkbox" class="form-check-input-styled" data-fouc>
                                    Keseluruhan
                                </label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-8 text-center">
                                <button type="submit" class="btn btn-primary"> Jana Laporan </button>
                            </div>
                        </div>
                </fieldset>
            </form>
        </div>

@endsection

