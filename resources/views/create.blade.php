@extends('layouts.backend')

@section('title')
    Pemohon
@endsection

@section('top_button')
    <a href="/pemohon" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Kembali ke Senarai Pemohon</span></a>
@endsection

@section('breadcrumb')
    <a href="/dashboard" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="/pemohon" class="breadcrumb-item active">Senarai Pemohon</a>
    {{-- <a href="/applicant/{{ $applicant ?? ''->id }}/applicant-list" class="breadcrumb-item active">Senarai Pemohon</a> --}}
    <span class="breadcrumb-item active">Menambah Profail Baru</span>
@endsection

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Menambah Profail Baru</h5>
            <div class="header-elements">
            <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            @include('pemohon.create')
        </div>

    </div>
@endsection


