@extends('layouts.backend')

{{-- @section('title')
    Urusetia
@endsection --}}

@section('top_button')
    {{-- <a href="/penerima" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Penerima</span></a> --}}
    <a href="/tanggungan/search/{{ $recepient->no_kp }}" class="btn btn-link btn-float text-default"><i class="icon-plus-circle2 text-primary"></i> <span>Tambah Penerima</span></a>
@endsection

@section('breadcrumb')
    <a href="/penerima" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <span class="breadcrumb-item active">Maklumat Penerima</span>


@endsection

@section('content')
<!-- Rounded solid bordered tabs -->
<div class="card">
    {{-- <div class="card-header header-elements-inline">
        <h6 class="card-title">Bordered/rounded justified</h6>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div> --}}

    <div class="card-body">
        <ul class="nav nav-tabs nav-tabs-solid nav-justified rounded bg-light">
            <li class="nav-item"><a href="#penerima" class="nav-link rounded-left active" data-toggle="tab">Penerima</a></li>
            <li class="nav-item"><a href="/tanggungan/{{ $recepient->no_kp}}" class="nav-link">Tanggungan</a></li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link rounded-right dropdown-toggle" data-toggle="dropdown">Bantuan</a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#bantuan" class="dropdown-item" data-toggle="tab">Bantuan IPR Selangor</a>
                    <a href="#bantuan-khas" class="dropdown-item" data-toggle="tab">Bantuan Khas</a>
                </div>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade show active" id="penerima">
                {{-- VIEW PENERIMA --}}
                <div class="card">
                    {{-- <div class="card-header bg-primary header-elements-inline">
                        <h5 class="card-title">Maklumat Penerima</h5>
                    </div> --}}
                    <div class="table-responsive">
                        <table class="table table-sm">
                            <tr>
                                <td style="width: 40%; text-align: right">Nama</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $recepient->profile->nama }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">No.Kad Pengenalan</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $recepient->no_kp }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">Jantina</td>
                                <td style="width: 5%">:</td>
                                <td>
                                    @if($recepient->profile->jantina == 'L')
                                    Lelaki
                                    @elseif($recepient->profile->jantina == 'P')
                                    Perempuan
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">Tarikh Lahir</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $recepient->profile->tarikh_lahir }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">Bangsa</td>
                                <td style="width: 5%">:</td>
                                <td>
                                    @if($recepient->profile->bangsa == 'M')
                                    Melayu
                                    @elseif($recepient->profile->bangsa == 'C')
                                    Cina
                                    @elseif($recepient->profile->bangsa == 'I')
                                    India
                                    @elseif($recepient->profile->bangsa == 'L')
                                    Lain-lain
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">Warganegara</td>
                                <td style="width: 5%">:</td>
                                <td>
                                    @if($recepient->profile->warganegara == 'M')
                                    Malaysia
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">Agama</td>
                                <td style="width: 5%">:</td>
                                <td>
                                    @if($recepient->profile->agama == 'I')
                                    Islam
                                    @elseif($recepient->profile->agama == 'B')
                                    Budha
                                    @elseif($recepient->profile->agama == 'K')
                                    Kristian
                                    @elseif($recepient->profile->agama == 'H')
                                    Hindu
                                    @elseif($recepient->profile->agama == 'L')
                                    Lain-lain
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">Status Perkahwinan</td>
                                <td style="width: 5%">:</td>
                                <td>
                                    @if($recepient->profile->status_perkahwinan == 'B')
                                    Bujang
                                    @elseif($recepient->profile->status_perkahwinan == 'K')
                                    Berkahwin
                                    @elseif($recepient->profile->status_perkahwinan == 'P')
                                    Pernah Berkahwin
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">E-mel</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $recepient->profile->emel }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">Alamat</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $recepient->profile->alamat }} , {{ $recepient->profile->poskod }}, {{ $recepient->profile->bandar }}, {{ $recepient->profile->negeri }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">Kampung</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $recepient->profile->village->nama }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">DM</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $recepient->profile->Dm->nama }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">DUN</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $recepient->profile->dun->nama }}</td>
                            </tr>
                            <tr>
                                <td style="width: 15%; text-align: right">Status</td>
                                <td style="width: 5%">:</td>
                                <td>{{ $recepient->status }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                {{-- END VIEW PENERIMA --}}
            </div>

        </div>
    </div>
</div>
<!-- /rounded solid bordered tabs -->



@endsection

@section('script')
    <script>
        $(".delete").on("submit", function(){
            return confirm("Adakah anda pasti?");
        });

    </script>

    {{-- <script>
    function checkId(nokp){
        if(nokp.length < 12 ){
            // alert(nokp);
        }else if(nokp.length > 12 ){
            alert('lebih');
        }else if(nokp.length == 12){
            var recepientId = document.getElementById("no_kp_penerima").value;
            // alert(no_kp_penerima);
            window.location.href = "/tanggungan/create/" + nokp + "/" + recepientId;
        }
    }

    </script> --}}

@endsection
