@extends('layouts.backend')

@section('title')
    Urusetia
@endsection

@section('top_button')
    {{-- <a href="/penerima" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Penerima</span></a> --}}
    <a href="/penerima/search" class="btn btn-link btn-float text-default"><i class="icon-plus-circle2 text-primary"></i> <span>Tambah Penerima</span></a>
@endsection

@section('breadcrumb')
    <a href="/home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <span class="breadcrumb-item active">Senarai Penerima</span>
@endsection

@section('content')

    <div class="card">
        {{-- <div class="card-header bg-info header-elements-inline">
            <h5 class="card-title">Senarai Penerima Bantuan</h5>
        </div> --}}

        <div class="card-header header-elements-inline">
            <h5 class="card-title">Senarai Penerima Bantuan</h5>
            <span><a href="/penerima/status/1">AKTIF</a>  | <a href="/penerima/status/0">TIDAK AKTIF</a></span>

            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <table class="table datatable-row-full">
            <thead>
                <tr>
                    <th>#</th>
                    <th>(Bil.)Nama Penuh</th>
                    <th>No.KP</th>
                    <th>Kampung</th>
                    <th>DM</th>
                    <th>Status</th>
                    <th class="text-center">Tindakan</th>
                </tr>
            </thead>

            <tbody>
                <?php $x=1; ?>
                @foreach ($recepients as $recepient)
                    <tr>
                    <td></td>
                    {{-- <td><button type="button" class="btn btn-light" data-toggle="modal" data-target="#modal_iconified">Launch <i class="icon-play3 ml-2"></i></button></td> --}}
                    <td><a href="#" data-toggle="modal" data-target="#modal_recepient_view">({{ $x++ }}) {{ $recepient->profile->nama }}</a></td>
                    <td>{{ $recepient->no_kp }}</td>
                    {{-- <td>{{ $recepient->profile->jantina }}</td> --}}
                    <td>
                       @if(!empty($recepient->profile->dm))
                            {{ $recepient->profile->dm->nama }}
                        @endif
                    </td>

                    <td>
                        @if(!empty($recepient->profile->village_id))
                            {{ $recepient->profile->village->nama }}
                        @endif
                    </td>
                    <td>
                        @if($recepient->status == 'Aktif')
                            <span class="badge badge-success">Aktif</span>
                        @elseif(($recepient->status == 'Tidak Aktif'))
                            <span class="badge badge-warning">Tidak Aktif</span>
                        @endif
                    </td>
                    <td class="text-center">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item caret-0 dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="/penerima/edit/{{ $recepient->no_kp }}" class="dropdown-item"><i class="icon-pencil7"></i> Edit Penerima </a>
                                    <a href="/tanggungan/{{ $recepient->no_kp }}" class="dropdown-item"><i class="icon-users"></i> Urus tanggungan</a>
                                    <a href="/bantuan/{{ $recepient->no_kp }}" class="dropdown-item"><i class="icon-gift"></i> Urus bantuan</a>
                                    {{-- <a href="#" class="dropdown-item"><i class="icon-users"></i> Tanggungan</a>
                                    <a href="#" class="dropdown-item"><i class="icon-gift"></i> Bantuan</a> --}}
                                    {{-- <div class="dropdown-divider"></div> --}}
                                    {{-- <form class="delete" action="/penerima/destroy/{{ $recepient->no_kp }}" method="DELETE">
                                    {{ csrf_field() }}

                                        <button type="submit" class="icon-bin" title="Padam-b"></button>
                                    </form> --}}
                                </div>




                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>

     <!-- Iconified modal -->
            @foreach ($recepients as $recepient)

				<div id="modal_recepient_view" class="modal fade" tabindex="-1">
					<div class="modal-dialog">
						<div class="modal-content">
                            <div class="modal-header">
								{{-- <h5 class="modal-title"><i class="icon-menu7 mr-2"></i> &nbsp;Modal with icons</h5> --}}
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
							<div class="modal-body">
								<div class="alert alert-info alert-dismissible alert-styled-left border-top-0 border-bottom-0 border-right-0">
					                <span class="font-weight-semibold">Maklumat Penerima</span>.
					            </div>

                                <table class="table table-sm">
                                    <tr>
                                        <td style="width: 40%; text-align: right">Nama</td>
                                        <td style="width: 5%">:</td>
                                        <td>{{ $recepient->profile->nama }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right">No.Kad Pengenalan</td>
                                        <td style="width: 5%">:</td>
                                        <td>{{ $recepient->no_kp }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right">Jantina</td>
                                        <td style="width: 5%">:</td>
                                        <td>
                                            @if($recepient->profile->jantina == 'L')
                                            Lelaki
                                            @elseif($recepient->profile->jantina == 'P')
                                            Perempuan
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right">Tarikh Lahir</td>
                                        <td style="width: 5%">:</td>
                                        <td>{{ $recepient->profile->tarikh_lahir }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right">Bangsa</td>
                                        <td style="width: 5%">:</td>
                                        <td>
                                            @if($recepient->profile->bangsa == 'M')
                                            Melayu
                                            @elseif($recepient->profile->bangsa == 'C')
                                            Cina
                                            @elseif($recepient->profile->bangsa == 'I')
                                            India
                                            @elseif($recepient->profile->bangsa == 'L')
                                            Lain-lain
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right">Warganegara</td>
                                        <td style="width: 5%">:</td>
                                        <td>
                                            @if($recepient->profile->warganegara == 'M')
                                            Malaysia
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right">Agama</td>
                                        <td style="width: 5%">:</td>
                                        <td>
                                            @if($recepient->profile->agama == 'I')
                                            Islam
                                            @elseif($recepient->profile->agama == 'B')
                                            Budha
                                            @elseif($recepient->profile->agama == 'K')
                                            Kristian
                                            @elseif($recepient->profile->agama == 'H')
                                            Hindu
                                            @elseif($recepient->profile->agama == 'L')
                                            Lain-lain
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right">Status Perkahwinan</td>
                                        <td style="width: 5%">:</td>
                                        <td>
                                            @if($recepient->profile->status_perkahwinan == 'B')
                                            Bujang
                                            @elseif($recepient->profile->status_perkahwinan == 'K')
                                            Berkahwin
                                            @elseif($recepient->profile->status_perkahwinan == 'P')
                                            Pernah Berkahwin
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right">E-mel</td>
                                        <td style="width: 5%">:</td>
                                        <td>{{ $recepient->profile->emel }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right">Alamat</td>
                                        <td style="width: 5%">:</td>
                                        <td>{{ $recepient->profile->alamat }} , {{ $recepient->profile->poskod }}, {{ $recepient->profile->bandar }}, {{ $recepient->profile->negeri }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right">Kampung</td>
                                        <td style="width: 5%">:</td>
                                        <td>
                                            @if(!empty($recepient->profile->village_id))
                                                {{ $recepient->profile->village->nama }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right">DM</td>
                                        <td style="width: 5%">:</td>
                                        <td>
                                            @if(!empty($recepient->profile->dm))
                                                {{ $recepient->profile->dm->nama }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right">DUN</td>
                                        <td style="width: 5%">:</td>
                                        <td>{{ $recepient->profile->dun->nama }}</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right">Status</td>
                                        <td style="width: 5%">:</td>
                                        <td>{{ $recepient->status }}</td>
                                    </tr>
                                </table>

							</div>

							<div class="modal-footer">
								<button class="btn btn-link" data-dismiss="modal"><i class="icon-cross2 font-size-base mr-1"></i> Close</button>
							</div>
						</div>
					</div>
                </div>
                @endforeach
				<!-- /iconified modal -->


@endsection
@section('script')
    <script>
        $(".delete").on("submit", function(){
            return confirm("Adakah anda pasti?");
        });
    </script>
@endsection
