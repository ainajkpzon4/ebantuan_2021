@extends('layouts.backend')

{{--
@section('top_button')
<a href="/penerima" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Penerima</span></a>
<a href="/penerima/create" class="btn btn-link btn-float text-default"><i class="icon-plus-circle2 text-primary"></i> <span>Tambah Penerima</span></a>
@endsection --}}

@section('breadcrumb')
<a href="/home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
<a href="/penerima/status/1" class="breadcrumb-item">Senarai Penerima</a>

@endsection

@section('content')

<div class="card">
    <div class="card-body">
        <h6>

        </h6>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="peribadi">
                <div class="card">
                    <div class="card-body">
                        <form method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="font-weight-semibold"><i class="icon-reading mr-2"></i> Maklumat Peribadi</legend>


                                        <div class="form-group">
                                            <label>No. Kad Pengenalan:</label>
                                            <input type="number" placeholder="Cth : 780101101234" name="no_kp" onkeyup="checkId(this.value)" id="no_kp_pasangan" value="{{ old('no_kp') }}" class="form-control no_kp" placeholder="901013101234" autocomplete="off">
                                            <span class="text-muted">Masukkan nombor tanpa tanda (-)</span>

                                        </div>

    </div>
</div>
</div>

@endsection

@section('script')

<script>

function checkId(nokp){

    $('.no_kp').keypress(function(e){
    if ( e.which == 13 ) return false;
    });

    if(nokp.length < 12 ){
        // alert('No.Kad Pengenalan yang dimasukkan kurang 12 Digit');
    }else if(nokp.length > 12 ){
        alert('No.Kad Pengenalan yang dimasukkan lebih 12 Digit');
        return false;
    }else if(nokp.length == 12){
        var recepientId = document.getElementById("no_kp_pasangan").value;
        // alert(no_kp_penerima);
        window.location.href = "/penerima/create/" + nokp;
    }
}

</script>

@endsection
