@extends('layouts.backend')


@section('top_button')
<a href="/penerima/status/1" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Penerima</span></a>
{{-- <a href="/penerima/search" class="btn btn-link btn-float text-default"><i class="icon-plus-circle2 text-primary"></i> <span>Tambah Penerima</span></a> --}}
@endsection

@section('breadcrumb')
<a href="/dashboard" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
<a href="/pemohon" class="breadcrumb-item">Senarai Penerima</a>

@endsection

@section('content')

<div class="card">
    <div class="card-header header-elements-inline">
        {{-- <h6 class="card-title"><b>Maklumat Tanggungan</b></h6> --}}
        <div class="header-elements">
            <div class="list-icons">
                <a href="{{ URL::previous() }}"><i class="icon-arrow-left8"></i> previous</a>

            </div>
        </div>
    </div>

    {{-- <div class="card-header header-elements-inline">
        <h6 class="card-title"><b>Maklumat Penerima</b></h6>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                <a class="list-icons-item" data-action="remove"></a>
            </div>
        </div>
    </div> --}}

    <div class="card-body">
        {{-- <h6>
            <ul class="nav nav-tabs bg-primary nav-justified">
                <li class="nav-item"><a href="/penerima/search/{{$recepientId}}" class="nav-link active">Maklumat Peribadi</a></li>
                <li class="nav-item"><a href="pasangan/search{{$recepientId}}" class="nav-link disabled">Maklumat Pasangan</a></li>
                <li class="nav-item"><a href="/tanggungan/search/{{$recepientId}}" class="nav-link disabled">Maklumat Anak</a></li>
                <li class="nav-item"><a href="/bantuan/{{$recepientId}}" class="nav-link disabled">Bantuan</a></li>
                <li class="nav-item"><a href="/bantuan_khas/{{$recepientId}}" class="nav-link disabled">Bantuan Khas</a></li>
            </ul>
        </h6> --}}
        <span class="text-muted">Tanda (</span><span class="text-danger"> *</span><span class="text-muted"> ) wajib diisi.</span>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="peribadi">
                <div class="card">
                    <div class="card-body">
                        <form method="post" enctype="multipart/form-data" autocomplete="off">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="font-weight-semibold"><i class="icon-reading mr-2"></i> Maklumat Peribadi</legend>

                                        <div class="form-group">
                                            <label>No. Kad Pengenalan:</label>
                                            <input type="text" maxlength="12" size="12" id="no_kp_pasangan" name="no_kp" value="{{ old('no_kp',$recepientId) }}" class="form-control" placeholder="901013101234" onkeyup="checkId(this.value)" readonly>
                                        </div>

                                        <div id="result">
                                            <div class="form-group">
                                                <label>Nama penerima: <span class="text-danger">*</span></label>
                                                <input type="text" name="nama" value="{{ old('nama') }}" class="form-control" placeholder="Mohd Roslee bin Ahmad">
                                            </div>

                                            <div class="form-group">
                                                <label>Tarikh Lahir: <span class="text-danger">*</span></label>
                                                <input type="date" name="tarikh_lahir" value="{{ old('tarikh_lahir', $tarikh_lahir) }}" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label>Pilih jantina: <span class="text-danger">*</span></label>
                                                <select name="jantina" data-placeholder="Pilih jantina" class="form-control form-control-select2" data-fouc>
                                                    <option></option>
                                                    <option value="L" @if(old('jantina', $jantina)=='L' ) selected @endif>LELAKI</option>
                                                    <option value="P" @if(old('jantina', $jantina)=='P' ) selected @endif>PEREMPUAN</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Pilih Agama: <span class="text-danger">*</span></label>
                                                <select name="agama" data-placeholder="Pilih agama" class="form-control form-control-select2" data-fouc>
                                                    <option></option>
                                                    <option value="I" @if(old('agama')=='I' ) selected @endif>ISLAM</option>
                                                    <option value="K" @if(old('agama')=='k' ) selected @endif>KRISTIAN</option>
                                                    <option value="B" @if(old('agama')=='B' ) selected @endif>BUDHA</option>
                                                    <option value="H" @if(old('agama')=='H' ) selected @endif>HINDU</option>
                                                    <option value="L" @if(old('agama')=='L' ) selected @endif>LAIN-LAIN</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Pilih Bangsa: <span class="text-danger">*</span></label>
                                                <select name="bangsa" data-placeholder="Pilih bangsa" class="form-control form-control-select2" data-fouc>
                                                    <option></option>
                                                    <option value="M" @if(old('bangsa')=='M' ) selected @endif>MELAYU</option>
                                                    <option value="C" @if(old('bangsa')=='C' ) selected @endif>CINA</option>
                                                    <option value="I" @if(old('bangsa')=='I' ) selected @endif>INDIA</option>
                                                    <option value="L" @if(old('bangsa')=='L' ) selected @endif>LAIN-LAIN</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Status Perkahwinan: <span class="text-danger">*</span></label>
                                                <select name="status_perkahwinan" data-placeholder="Pilih Status" class="form-control form-control-select2" data-fouc>
                                                    <option></option>
                                                    <option value="B" @if(old('status_perkahwinan')=='B' ) selected @endif>BUJANG</option>
                                                    <option value="K" @if(old('status_perkahwinan')=='K' ) selected @endif>BERKAHWIN</option>
                                                    <option value="P" @if(old('status_perkahwinan')=='P' ) selected @endif>PERNAH BERKAHWIN</option>
                                                </select>
                                            </div>

                                            <div>
                                                <div class="form-group">
                                                    <label>Kad OKU:</label>
                                                    <select name="kad_oku" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                                        <option></option>
                                                        <option value="Y">YA</option>
                                                        <option value="T" selected>TIDAK</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Nota Tambahan:</label>
                                                <textarea rows="3" cols="5" class="form-control" placeholder="Masukkan nota tambahan anda di sini">{{ old('nama') }}</textarea>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>

                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="font-weight-semibold"><i class="icon-home mr-2"></i> Maklumat Alamat & Kontak</legend>

                                        <div>
                                            <div class="form-group">
                                                <label>E-mel:</label>
                                                <input type="text" name="emel" value="{{ old('emel') }}" placeholder="Contoh : mohd@gmail.com" class="form-control">
                                            </div>
                                        </div>

                                        <div>
                                            <div class="form-group">
                                                <label>No.Telefon: <span class="text-danger">*</span></label>
                                                <input type="text" name="no_telefon" value="{{ old('no_telefon') }}" placeholder="017-4521003" class="form-control">
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>No. Rumah/Lot/Jalan: <span class="text-danger">*</span></label>
                                                    <input type="text" name="alamat" value="" placeholder="Contoh : No. 802, Jalan SG 1/0, Fasa 1" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Kampung: <span class="text-danger">*</span></label>
                                                    <select name="village" data-placeholder="Pilih Kampung/kawasan" class="form-control form-control-select2 @if ($errors->has('village')) border-danger @endif" data-fouc>
                                                        <option></option>
                                                        @foreach ($villages as $village)
                                                            <option value="{{ $village->id }}" @if(old('village')==$village->id) selected @endif>{{ $village->nama }}</option>
                                                        @endforeach
                                                    </select>
                                                    {{-- <input type="text" name="dm" value="{{ old('dm',$recepient->profile->dm->nama) }}" placeholder="Contoh : Gombak Utara" class="form-control"> --}}
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Poskod: <span class="text-danger">*</span></label>
                                                    <input type="text" name="poskod" value="68100" placeholder="68100" value="68100" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Bandar: <span class="text-danger">*</span></label>
                                                    <input type="text" name="bandar" value="Batu Caves" placeholder="Batu Caves" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Negeri: <span class="text-danger">*</span></label>
                                                    <input type="text" name="negeri" value="Selangor" placeholder="Selangor" value="Selangor" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Negara: <span class="text-danger">*</span></label>
                                                    <select name="negara" data-placeholder="Pilih Negara" class="form-control form-control-select2" data-fouc>
                                                        <option></option>
                                                        <option value="MAL" selected>Malaysia</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>DUN: <span class="text-danger">*</span></label>
                                                    <select name="dun" data-placeholder="Pilih DUN" aria-readonly="true" class="form-control form-control-select2" data-fouc>
                                                        <option></option>
                                                        <option value="1" selected>Gombak Setia</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Pengundi: <span class="text-danger">*</span></label>
                                                    <select name="pengundi" data-placeholder="Pilih Ya/Tidak" class="form-control form-control-select2 @if ($errors->has('pengundi')) border-danger @endif" data-fouc>
                                                        <option value="">-Sila Pilih-</option>
                                                        <option value="Y" @if(old('pengundi')=='Y') selected @endif>YA</option>
                                                        <option value="T" @if(old('pengundi')=='T') selected @endif>TIDAK</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>DM: <span class="text-danger">*</span></label>
                                                    <select name="dm" data-placeholder="Pilih DM" class="form-control form-control-select2 @if ($errors->has('dm')) border-danger @endif" data-fouc>
                                                        <option></option>
                                                        @foreach ($dms as $dm)
                                                        <option value="{{ $dm->id }}" @if(old('dm')==$dm->id) selected @endif>{{ $dm->nama }}</option>
                                                        @endforeach
                                                    </select>
                                                    {{-- <input type="text" name="dm" value="{{ old('dm',$recepient->profile->dm->nama) }}" placeholder="Contoh : Gombak Utara" class="form-control"> --}}
                                                </div>
                                            </div>

                                            <div>
                                                <div class="col-md-12">
                                                    <label>Status <span class="text-danger">*</span></label>
                                                    <select name="status" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                                        <option value="Aktif" @if(old('status')=='Aktif' ) selected @endif>Aktif</option>
                                                        <option value="Tidak Aktif" @if(old('status')=='Tidak Aktif' ) selected @endif>Tidak Aktif</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-primary"><i class="icon-floppy-disk"></i> Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            {{-- <div class="tab-pane fade" id="pasangan">
                {{ $recepient->profile->spouse}}
        </div>

        <div class="tab-pane fade" id="anak">
            DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg whatever.
        </div>

        <div class="tab-pane fade" id="bantuan">
            Aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthet.
        </div>

        <div class="tab-pane fade" id="bantuan_khas">
            bantuan khas.
        </div> --}}
    </div>
</div>
</div>

@endsection

@section('script')

    <script>

    function checkId(nokp){

        if(nokp.length < 12 ){
            // alert(nokp);
        }else if(nokp.length > 12 ){
            alert('lebih');
        }else if(nokp.length == 12){
            var recepientId = document.getElementById("no_kp_pasangan").value;
            // alert(no_kp_penerima);
            window.location.href = "/penerima/search/" + nokp;
        }
    }

    </script>

@endsection

