@extends('layouts.backend')

@section('top_button')
    {{-- <a href="/penerima" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Penerima</span></a> --}}
    <a href="/penerima/search" class="btn btn-link btn-float text-default"><i class="icon-plus-circle2 text-primary"></i> <span>Tambah Penerima</span></a>
@endsection

@section('breadcrumb')
<a href="/home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
<a href="/penerima/status/1" class="breadcrumb-item">Senarai Penerima</a>
<span class="breadcrumb-item active">Maklumat Penerima</span>



@endsection

@section('content')

<div class="card">

    <div class="card-body">

        <ul class="nav nav-tabs nav-tabs-solid nav-justified rounded bg-light">
            <li class="nav-item"><a href="/penerima/edit/{{ $recepient->no_kp}}" class="nav-link rounded-left active">Penerima</a></li>
            <li class="nav-item"><a href="/tanggungan/{{ $recepient->no_kp}}" class="nav-link">Tanggungan</a></li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link rounded-right dropdown-toggle" data-toggle="dropdown">Bantuan</a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="/bantuan/{{ $recepient->no_kp }}" class="dropdown-item">Bantuan IPR Selangor</a>
                    <a href="/bantuan_khas/{{ $recepient->no_kp }}" class="dropdown-item">Bantuan Khas</a>
                </div>
            </li>
        </ul>
        <div class="bg-secondary"><h4>Kemaskini Maklumat Penerima</h4></div>
        <span class="text-muted">Tanda (</span><span class="text-danger"> *</span><span class="text-muted"> ) wajib diisi.</span>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="peribadi">
                <div class="card">
                    <div class="card-body">
                        <form method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="font-weight-semibold"><i class="icon-reading mr-2"></i> Maklumat Peribadi</legend>


                                        <div class="form-group">
                                            <label>No. Kad Pengenalan:</label>
                                            <input type="text" maxlength="12" size="12" id="no_kp_pasangan" name="no_kp" value="{{ old('no_kp',$recepientId) }}" class="form-control" placeholder="901013101234" onkeyup="checkId(this.value)" readonly>
                                        </div>

                                        <div id="result">
                                            <div class="form-group">
                                                <label>Nama penerima: <span class="text-danger">*</span></label>
                                                <input type="text" name="nama" value="{{ old('nama', $recepient->profile->nama) }}" class="form-control" placeholder="Mohd Roslee bin Ahmad">
                                            </div>

                                            <div class="form-group">
                                                <label>Tarikh Lahir: <span class="text-danger">*</span></label>
                                                <input type="date" name="tarikh_lahir" value="{{ old('tarikh_lahir', $recepient->profile->tarikh_lahir ) }}" class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label>Pilih jantina: <span class="text-danger">*</span></label>
                                                <select name="jantina" data-placeholder="Pilih jantina" class="form-control form-control-select2" data-fouc>
                                                    <option></option>
                                                    <option value="L" @if(old('jantina', $recepient->profile->jantina)=='L' ) selected @endif>LELAKI</option>
                                                    <option value="P" @if(old('jantina', $recepient->profile->jantina)=='P' ) selected @endif>PEREMPUAN</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Pilih Agama: <span class="text-danger">*</span></label>
                                                <select name="agama" data-placeholder="Pilih agama" class="form-control form-control-select2" data-fouc>
                                                    <option></option>
                                                    <option value="I" @if(old('agama', $recepient->profile->agama)=='I' ) selected @endif>ISLAM</option>
                                                    <option value="K" @if(old('agama', $recepient->profile->agama)=='K' ) selected @endif>KRISTIAN</option>
                                                    <option value="B" @if(old('agama', $recepient->profile->agama)=='B' ) selected @endif>BUDHA</option>
                                                    <option value="H" @if(old('agama', $recepient->profile->agama)=='H' ) selected @endif>HINDU</option>
                                                    <option value="L" @if(old('agama', $recepient->profile->agama)=='L' ) selected @endif>LAIN-LAIN</option>
                                                </select>
                                            </div>
                                            {{-- <div class="form-group">
                                                <label>Nyatakan jika agama lain-lain:</label>
                                                <input type="text" name="agama_lain" class="form-control" placeholder="-">
                                            </div> --}}

                                            <div class="form-group">
                                                <label>Pilih Bangsa: <span class="text-danger">*</span></label>
                                                <select name="bangsa" data-placeholder="Pilih bangsa" class="form-control form-control-select2" data-fouc>
                                                    <option></option>
                                                    <option value="I" @if(old('bangsa', $recepient->profile->bangsa)=='I' ) selected @endif>ISLAM</option>
                                                    <option value="M" @if(old('bangsa', $recepient->profile->bangsa)=='M' ) selected @endif>MELAYU</option>
                                                    <option value="C" @if(old('bangsa', $recepient->profile->bangsa)=='C' ) selected @endif>CINA</option>
                                                    <option value="I" @if(old('bangsa', $recepient->profile->bangsa)=='I' ) selected @endif>INDIA</option>
                                                    <option value="L" @if(old('bangsa', $recepient->profile->bangsa)=='L' ) selected @endif>LAIN-LAIN</option>
                                                </select>
                                            </div>

                                            {{-- <div class="form-group">
                                                <label>Nyatakan jika bangsa lain-lain:</label>
                                                <input type="text" name="bangsa_lain" class="form-control" placeholder="Contoh : Bidayuh">
                                            </div> --}}

                                            <div class="form-group">
                                                <label>Status Perkahwinan: <span class="text-danger">*</span></label>
                                                <select name="status_perkahwinan" data-placeholder="Pilih Status" class="form-control form-control-select2" data-fouc>
                                                    <option></option>
                                                    <option value="B" @if(old('status_perkahwinan', $recepient->profile->status_perkahwinan)=='B' ) selected @endif>BUJANG</option>
                                                    <option value="K" @if(old('status_perkahwinan', $recepient->profile->status_perkahwinan)=='K' ) selected @endif>BERKAHWIN</option>
                                                    <option value="P" @if(old('status_perkahwinan', $recepient->profile->status_perkahwinan)=='P' ) selected @endif>PERNAH BERKAHWIN</option>
                                                </select>
                                            </div>

                                            <div>
                                                <div class="form-group">
                                                    <label>Kad OKU:</label>
                                                    <select name="kad_oku" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                                        <option></option>
                                                        <option value="Y" @if(old('kad_oku', $recepient->profile->kad_oku)=='Y' ) selected @endif>YA</option>
                                                        <option value="T" @if(old('kad_oku', $recepient->profile->kad_oku)=='T' ) selected @endif>TIDAK</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Nota Tambahan:</label>
                                                <textarea rows="3" name="nota" cols="5" class="form-control" placeholder="Masukkan nota tambahan anda di sini">{{ old('nota', $recepient->profile->nota ) }}</textarea>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>

                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="font-weight-semibold"><i class="icon-home mr-2"></i> Maklumat Alamat & Kontak</legend>

                                        <div>
                                            <div class="form-group">
                                                <label>E-mel:</label>
                                                <input type="text" name="emel" value="{{ old('emel', $recepient->profile->emel) }}" placeholder="Contoh : mohd@gmail.com" class="form-control">
                                            </div>
                                        </div>

                                        <div>
                                            <div class="form-group">
                                                <label>No.Telefon: <span class="text-danger">*</span></label>
                                                <input type="text" name="no_telefon" value="{{ old('no_telefon', $recepient->profile->no_telefon) }}" placeholder="017-4521003" class="form-control">
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>No. Rumah/Lot/Jalan: <span class="text-danger">*</span></label>
                                                    <input type="text" name="alamat" value="{{ old('alamat', $recepient->profile->alamat) }}" placeholder="Contoh : No. 802, Jalan SG 1/0, Fasa 1" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Kampung: <span class="text-danger">*</span></label>
                                                    <select name="village" data-placeholder="Pilih Kampung/kawasan" class="form-control form-control-select2 @if ($errors->has('village')) border-danger @endif" data-fouc>
                                                        <option></option>
                                                        @foreach ($villages as $village)
                                                        <option value="{{ $village->id }}" @if(old('village', $recepient->profile->village_id)==$village->id) selected @endif>{{ $village->nama }}</option>
                                                        @endforeach
                                                    </select>
                                                    {{-- <input type="text" name="dm" value="{{ old('dm',$recepient->profile->dm->nama) }}" placeholder="Contoh : Gombak Utara" class="form-control"> --}}
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Poskod: <span class="text-danger">*</span></label>
                                                    <input type="text" name="poskod" value="{{ old('poskod',$recepient->profile->poskod) }}" placeholder="68100" value="68100" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Bandar: <span class="text-danger">*</span></label>
                                                    <input type="text" name="bandar" value="{{ old('bandar',$recepient->profile->bandar) }}" placeholder="Batu Caves" class="form-control">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Negeri: <span class="text-danger">*</span></label>
                                                    <input type="text" name="negeri" value="{{ old('negeri',$recepient->profile->negeri) }}" placeholder="Selangor" value="Selangor" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Negara: <span class="text-danger">*</span></label>
                                                    <select name="negara" data-placeholder="Pilih Negara" class="form-control form-control-select2" data-fouc>
                                                        <option></option>
                                                        <option value="MAL" selected>Malaysia</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>DUN: <span class="text-danger">*</span></label>
                                                    <select name="dun" data-placeholder="Pilih DUN" aria-readonly="true" class="form-control form-control-select2" data-fouc>
                                                        <option></option>
                                                        <option value="1" selected>Gombak Setia</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Pengundi: <span class="text-danger">*</span></label>
                                                    <select name="pengundi" data-placeholder="Pilih Ya/Tidak" class="form-control form-control-select2 @if ($errors->has('pengundi')) border-danger @endif" data-fouc>
                                                        <option value="">-Sila Pilih-</option>
                                                        <option value="Y" @if(old('pengundi', $recepient->pengundi) =='Y') selected @endif>YA</option>
                                                        <option value="T" @if(old('pengundi', $recepient->pengundi)=='T') selected @endif>TIDAK</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>DM: <span class="text-danger">*</span></label>
                                                    <select name="dm" data-placeholder="Pilih DM" class="form-control form-control-select2 @if ($errors->has('dm')) border-danger @endif" data-fouc>
                                                        <option></option>
                                                        @foreach ($dms as $dm)
                                                        <option value="{{ $dm->id }}" @if(old('dm', $recepient->profile->dm_id)==$dm->id) selected @endif>{{ $dm->nama }}</option>
                                                        @endforeach
                                                    </select>
                                                    {{-- <input type="text" name="dm" value="{{ old('dm',$recepient->profile->dm->nama) }}" placeholder="Contoh : Gombak Utara" class="form-control"> --}}
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Status: <span class="text-danger">*</span></label>
                                                    <select name="status" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                                        <option></option>
                                                        <option value="Aktif" @if(old('status', $recepient->status)=='Aktif' ) selected @endif>Aktif</option>
                                                        <option value="Tidak Aktif" @if(old('status', $recepient->status)=='Tidak Aktif' ) selected @endif>Tidak Aktif</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>

                            <div class="text-center">
                                <a href="/penerima/destroy/{{ $recepient->no_kp }}"><button type="button" class="btn btn-danger delete"><i class="icon-bin"></i> Hapus </button></a>
                                <button type="submit" class="btn btn-primary"><i class="icon-floppy-disk"></i> Kemaskini</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            {{-- <div class="tab-pane fade" id="pasangan">
                {{ $recepient->profile->spouse}}
        </div>

        <div class="tab-pane fade" id="anak">
            DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg whatever.
        </div>

        <div class="tab-pane fade" id="bantuan">
            Aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda labore aesthet.
        </div>

        <div class="tab-pane fade" id="bantuan_khas">
            bantuan khas.
        </div> --}}
    </div>
</div>
</div>

@endsection

@section('script')

    <script>

    function checkId(nokp){

        if(nokp.length < 12 ){
            // alert(nokp);
        }else if(nokp.length > 12 ){
            alert('lebih');
        }else if(nokp.length == 12){
            var recepientId = document.getElementById("no_kp_pasangan").value;
            // alert(no_kp_penerima);
            window.location.href = "/penerima/search/" + nokp;
        }
    }

         $(".delete").on("click", function(){
            return confirm("Adakah anda pasti untuk menghapus rekod?");
        });
    </script>

@endsection

