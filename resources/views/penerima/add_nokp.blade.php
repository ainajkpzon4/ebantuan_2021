@extends('layouts.backend')

@section('breadcrumb')
    <a href="/penerima" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Penerima </a>
    <a href="/penerima/add" class="breadcrumb-item active">Tambah Pemohon</a>
    {{-- <span class="breadcrumb-item active">Senarai Pemohon</span> --}}
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <!-- Search field -->
				<div class="card">
					<div class="card-body">
                        <h5 class="mb-3"><i class="icon-info22" title="No.KP Penerima baru"></i>Tambah Penerima</h5>
                        {{-- <p class="text-muted">Masukkan no. kad pengenalan penerima</a></p> --}}

                        <form method="post" enctype="multipart/form-data">
                            @csrf
							<div class="input-group mb-3">
								<div class="form-group-feedback form-group-feedback-left">
									<input type="number" size="12" class="form-control form-control-lg alpha-grey" name="search" onkeyup="searchButton(this.value)" placeholder="Tambah penerima baru, Contoh : 901014101234">
									<div class="form-control-feedback form-control-feedback-lg">
										<i class="icon-add text-muted"></i>
									</div>
								</div>

								<div class="input-group-append">
                                    <div id="btnSubmit" style="display: none">
                                        <button type="submit" class="btn btn-primary btn-lg">Tambah</button>
                                    </div>
								</div>
							</div>
						</form>
					</div>
				</div>

        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    function searchButton(x){
        // alert('sini');
        // document.getElementById("txtHint").innerHTML = "";
        if (x.length == 12){
            document.getElementById("btnSubmit").style.display = 'block';
        }
        else if (x.length < 12){
            document.getElementById("btnSubmit").style.display = 'none';
        }
    }
</script>
@endsection
