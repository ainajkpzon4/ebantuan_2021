@extends('layouts.backend')

@section('title')
    Pengurusan Data
@endsection

@section('top_button')
    <a href="/rujukan/category" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Kategori Bantuan</span></a>
    <a href="/rujukan/category/create" class="btn btn-link btn-float text-default"><i class="icon-plus-circle2 text-primary"></i> <span>Tambah Kategori Bantuan</span></a>
@endsection

@section('breadcrumb')
    <a href="/home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="/rujukan/category" class="breadcrumb-item">Pengurusan Data</a>
    <span class="breadcrumb-item active">Kategori Bantuan</span>
@endsection

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Senarai Kategori Bantuan</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <table class="table datatable-basic">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Kategori</th>
                    <th>Status</th>
                    <th>Tindakan</th>
                </tr>
            </thead>
            <tbody>
                <?php $x=1; ?>
                @foreach ($categories as $category)
                <tr>
                    <td>{{ $x++ }}</td>
                    <td>{{ $category->nama }}</td>
                    <td>
                        @if ($category->status == 'Aktif')
                            <span class="badge badge-success text-uppercase">{{ $category->status }}</span>
                        @else
                            <span class="badge badge-danger text-uppercase">{{ $category->status }}</span>
                        @endif
                    </td>
                    <td>
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="/rujukan/category/show/{{ $category->id }}" class="dropdown-item"><i class="icon-list"></i> Papar</a>
                                    <a href="/rujukan/category/edit/{{ $category->id }}" class="dropdown-item"><i class="icon-pencil"></i> Kemaskini</a>
                                    <a href="/rujukan/category/destroy/{{ $category->id }}" class="dropdown-item delete"><i class="icon-trash"></i> Hapus</a>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>

                    </td>
                    <td class="text-center">

                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>

    </div>
@endsection

@section('script')
    <script>
        $(".delete").on("click", function(){
            return confirm("Adakah anda pasti?");
        });
    </script>
@endsection
