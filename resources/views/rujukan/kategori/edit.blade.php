@extends('layouts.backend')

@section('title')
    Pengurusan Data
@endsection

@section('top_button')
    <a href="/rujukan/category" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Kategori Bantuan</span></a>
@endsection

@section('breadcrumb')
    <a href="/dashboard" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="#" class="breadcrumb-item">Pengurusan Data</a>
    <a href="/rujukan/category" class="breadcrumb-item">Kategori Bantuan</a>
    <span class="breadcrumb-item active">Kemaskini</span>
@endsection

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Kemaskini Kategori Bantuan</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">

            <form method="post" enctype="multipart/form-data">
                @csrf

                <fieldset class="mb-3">
                    <legend class="text-uppercase text-info-800 font-size-sm font-weight-bold">Kategori Bantuan</legend>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Nama Kategori Bantuan<span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <input name="nama" type="text" class="form-control" placeholder="" value="{{ old('nama',$category->nama) }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Keterangan</label>
                        <div class="col-md-4">
                            <textarea name="keterangan" id="" cols="30" rows="5" class="form-control">{{ old('keterangan',$category->keterangan) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Status</label>
                        <div class="col-md-2">
                            <select name="status" class="form-control">
                                <option value="Aktif" @if (old('status',$category->status) == 'Aktif') selected @endif>Aktif</option>
                                <option value="Tidak Aktif" @if (old('status',$category->status) == 'Tidak Aktif') selected @endif>Tidak Aktif</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Dicipta Oleh</label>
                        <div class="col-md-2">
                            {{ $category->userCreatedBy->name }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Tarikh Dicipta</label>
                        <div class="col-md-2">
                            @if(!empty($category->created_at))
                            {{ date('d-m-Y', strtotime($category->created_at))}}
                            @else
                            -
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Dikemaskini Oleh</label>
                        <div class="col-md-2">
                            @if(!empty($category->userUpdatedBy->name))
                            {{ $category->userUpdatedBy->name }}
                            @else
                            -
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Tarikh Dikemaskini</label>
                        <div class="col-md-2">
                            @if(!empty($category->updated_at))
                            {{ date('d-m-Y', strtotime($category->updated_at))}}
                            @else
                            -
                            @endif
                        </div>
                    </div>


                    <hr>

                    <div class="text-center">
                        <button type="submit" class="btn btn-primary"> Kemaskini </button>
                    </div>


                </fieldset>
            </form>
        </div>

    </div>
@endsection

@section('script')

@endsection
