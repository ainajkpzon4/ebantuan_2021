@extends('layouts.backend')

@section('title')
    Pengurusan Data
@endsection

@section('top_button')
    <a href="/rujukan/bantuan" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Bantuan IPR Sel.</span></a>
@endsection

@section('breadcrumb')
    <a href="/dashboard" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="#" class="breadcrumb-item">Pengurusan Data</a>
    <a href="/rujukan/bantuan" class="breadcrumb-item">Bantuan</a>
    <span class="breadcrumb-item active">Kemaskini</span>
@endsection

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Kemaskini Bantuan</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">

            <form method="post" enctype="multipart/form-data">
                @csrf

                <fieldset class="mb-3">
                    <legend class="text-uppercase text-info-800 font-size-sm font-weight-bold">Bantuan IPR Selangor</legend>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Nama Bantuan<span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            {{$refDonation->nama }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Keterangan</label>
                        <div class="col-md-4">
                            {{$refDonation->keterangan }}
                        </div>

                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Status</label>
                        <div class="col-md-4">
                            @if ($refDonation->status == 'Aktif')
                            <span class="badge badge-success text-uppercase">{{ $refDonation->status }}</span>
                            @else
                                <span class="badge badge-danger text-uppercase">{{ $refDonation->status }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Dicipta Oleh</label>
                        <div class="col-md-2">
                            @if(!empty($refDonation->userCreatedBy->name))
                            {{ $refDonation->userCreatedBy->name }}
                            @else
                            -
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Tarikh Dicipta</label>
                        <div class="col-md-2">
                            @if(!empty($refDonation->created_at))
                            {{ date('d-m-Y', strtotime($refDonation->created_at))}}
                            @else
                            -
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Dikemaskini Oleh</label>
                        <div class="col-md-2">
                            @if(!empty($refDonation->userUpdatedBy->name))
                            {{ $refDonation->userUpdatedBy->name }}
                            @else
                            -
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Tarikh Dikemaskini</label>
                        <div class="col-md-2">
                             @if(!empty($refDonation->updated_at))
                            {{ date('d-m-Y', strtotime($refDonation->updated_at))}}
                            @else
                            -
                            @endif
                        </div>
                    </div>
                    <hr>

                    <div class="text-center">
                        <a href="{{ url()->previous() }}" class="btn btn-primary"> Kembali </a>
                    </div>

                </fieldset>
            </form>
        </div>

    </div>
@endsection

@section('script')

@endsection
