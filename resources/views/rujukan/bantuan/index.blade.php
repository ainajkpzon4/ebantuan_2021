@extends('layouts.backend')

@section('title')
    Pengurusan Data
@endsection

@section('top_button')
    <a href="/rujukan/bantuan" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Bantuan IPR Sel.</span></a>
    <a href="/rujukan/bantuan/create" class="btn btn-link btn-float text-default"><i class="icon-plus-circle2 text-primary"></i> <span>Tambah Bantuan</span></a>
@endsection

@section('breadcrumb')
    <a href="/home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="/rujukan/bantuan" class="breadcrumb-item">Pengurusan Data</a>
    <span class="breadcrumb-item active">Bantuan</span>
@endsection

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Senarai Semua Bantuan</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <table class="table datatable-basic">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Bantuan</th>
                    <th>Status</th>
                    <th>Tindakan</th>
                </tr>
            </thead>
            <tbody>
                <?php $x=1; ?>
                @foreach ($donations as $donate)
                <tr>
                    <td>{{ $x++ }}</td>
                    <td>{{ $donate->nama }}</td>
                    <td>
                        @if ($donate->status == 'Aktif')
                            <span class="badge badge-success text-uppercase">{{ $donate->status }}</span>
                        @else
                            <span class="badge badge-danger text-uppercase">{{ $donate->status }}</span>
                        @endif
                    </td>
                    <td>
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="/rujukan/bantuan/show/{{ $donate->id }}" class="dropdown-item"><i class="icon-list"></i> Papar</a>
                                    <a href="/rujukan/bantuan/edit/{{ $donate->id }}" class="dropdown-item"><i class="icon-pencil"></i> Kemaskini</a>
                                    <a href="/rujukan/bantuan/destroy/{{ $donate->id }}" class="dropdown-item delete"><i class="icon-trash"></i> Hapus</a>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>

                    </td>
                    <td class="text-center">

                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>

    </div>
@endsection

@section('script')
    <script>
        $(".delete").on("click", function(){
            return confirm("Adakah anda pasti untuk menghapus rekod?");
        });
    </script>
@endsection
