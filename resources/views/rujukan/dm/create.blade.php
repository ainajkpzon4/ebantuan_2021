@extends('layouts.backend')

@section('title')
    Pengurusan Data
@endsection

@section('top_button')
    <a href="/rujukan/dm" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai DM</span></a>
@endsection

@section('breadcrumb')
    <a href="/dashboard" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="#" class="breadcrumb-item">Pengurusan Data</a>
    <a href="/rujukan/bantuan" class="breadcrumb-item">Daerah Mengundi</a>
    <span class="breadcrumb-item active">Tambah</span>
@endsection

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Daftar DM</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            {{-- <p class="mb-4">
                Fill up the form below to create new area for this system.
            </p> --}}
            <form method="post" enctype="multipart/form-data">
                @csrf

                <fieldset class="mb-3">
                    <legend class="text-uppercase text-info-800 font-size-sm font-weight-bold">Bantuan</legend>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Nama DM<span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <input name="nama" type="text" class="form-control" placeholder="" value="{{ old('nama') }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Keterangan</label>
                        <div class="col-md-4">
                            <textarea name="keterangan" id="" cols="30" rows="10" class="form-control">{{ old('keterangan') }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Status</label>
                        <div class="col-md-2">
                            <select name="status" class="form-control">
                                <option value="Aktif" @if (old('status') == 'Aktif') selected @endif>Aktif</option>
                                <option value="Tidak Aktif" @if (old('status') == 'Tidak Aktif') selected @endif>Tidak Aktif</option>
                            </select>
                        </div>
                    </div>
                    <hr>

                    <div class="text-center">
                        <button type="submit" class="btn btn-primary"><i class="icon-add mr-2"></i> Simpan </button>
                    </div>

                </fieldset>
            </form>
        </div>

    </div>
@endsection

@section('script')

@endsection
