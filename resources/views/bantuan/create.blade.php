@extends('layouts.backend')

@section('title')
    Pengurusan Data
@endsection

@section('top_button')
    <a href="/home" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Kembali ke Senarai Gred</span></a>
@endsection

@section('breadcrumb')
    <a href="/home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="#" class="breadcrumb-item">Pengurusan Data</a>
    <a href="/grade" class="breadcrumb-item">Gred</a>
    <span class="breadcrumb-item active">Tambah</span>
@endsection

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Daftar Gred</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            {{-- <p class="mb-4">
                Fill up the form below to create new area for this system.
            </p> --}}
            <form method="post" enctype="multipart/form-data">
                @csrf

                <fieldset class="mb-3">
                    <legend class="text-uppercase text-info-800 font-size-sm font-weight-bold">Gred</legend>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Nama <span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <input name="nama" type="text" class="form-control" placeholder="" value="{{ old('nama') }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Kumpulan Perkhidmatan <span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <select name="kumpulan_perkhidmatan" class="form-control">
                                <option value="Pengurusan Tertinggi" @if (old('kumpulan_perkhidmatan') == 'Pengurusan Tertinggi') selected @endif>Pengurusan Tertinggi</option>
                                <option value="Pengurusan dan Profesional" @if (old('kumpulan_perkhidmatan') == 'Pengurusan dan Profesional') selected @endif>Pengurusan dan Profesional</option>
                                <option value="Pelaksana" @if (old('kumpulan_perkhidmatan') == 'Pelaksana') selected @endif>Pelaksana</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Keterangan</label>
                        <div class="col-md-4">
                            <textarea name="keterangan" id="" cols="30" rows="10" class="form-control">{{ old('keterangan') }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-3">Status</label>
                        <div class="col-md-2">
                            <select name="status" class="form-control">
                                <option value="aktif" @if (old('status') == 'aktif') selected @endif>Aktif</option>
                                <option value="tidak aktif" @if (old('status') == 'tidak aktif') selected @endif>Tidak Aktif</option>
                            </select>
                        </div>
                    </div>

                    <hr>

                    <div class="form-group row">
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary"><i class="icon-add mr-2"></i> Simpan </button>
                        </div>
                    </div>

                </fieldset>
            </form>
        </div>

    </div>
@endsection

@section('script')

@endsection
