@extends('layouts.backend')

@section('title')
    Pengurusan Data
@endsection

@section('top_button')
    <a href="/grade" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Kembali ke Senarai Gred</span></a>
@endsection

@section('breadcrumb')
    <a href="/dashboard" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="#" class="breadcrumb-item">Pengurusan Data</a>
    <a href="/grade" class="breadcrumb-item">Gred</a>
    <span class="breadcrumb-item active">Papar</span>
@endsection

@section('content')
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Maklumat Gred</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <fieldset class="mb-3">
                <legend class="text-uppercase text-info-800 font-size-sm font-weight-bold">Gred</legend>

                <div class="form-group row">
                    <label class="col-form-label col-md-3">Nama <span class="text-danger">*</span></label>
                    <div class="col-md-4">
                        {{ $grade->nama }}
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-md-3">Kumpulan Perkhidmatan <span class="text-danger">*</span></label>
                    <div class="col-md-4">
                        {{ $grade->kumpulan_perkhidmatan }}
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-md-3">Keterangan</label>
                    <div class="col-md-4">
                        {{ $grade->keterangan }}
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-md-3">Status</label>
                    <div class="col-md-4">
                        {{ $grade->status }}
                    </div>
                </div>

                <hr>

                <div class="form-group row">
                    <div class="col-md-4">
                        <form class="delete" action="/grade/destroy/{{ $grade->id }}" method="POST">

                            <a href="/grade/edit/{{ $grade->id }}"><span class="btn bg-primary"><i class="icon-add mr-2"> Kemaskini</i></span>

                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}
                            <button type="submit" class="btn bg-danger" title="Padam"><i class="icon-trash mr-2"> Padam</i></button>
                    </form>
                </div>
                </div>
            </fieldset>
        </div>

    </div>
@endsection

@section('script')
    <script>
        $(".delete").on("submit", function(){
            return confirm("Adakah anda pasti?");
        });
    </script>
@endsection

