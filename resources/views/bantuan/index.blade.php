@extends('layouts.backend')

{{-- @section('title')
    Urusetia
@endsection --}}
@section('top_button')
    {{-- <a href="/penerima" class="btn btn-link btn-float text-default"><i class="icon-list2 text-primary"></i> <span>Senarai Penerima</span></a> --}}
    <a href="/penerima/search" class="btn btn-link btn-float text-default"><i class="icon-plus-circle2 text-primary"></i> <span>Tambah Penerima</span></a>
@endsection

@section('breadcrumb')
    <a href="/home" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Laman Utama</a>
    <a href="/penerima/status/1" class="breadcrumb-item">Senarai Penerima</a>
    <span class="breadcrumb-item active">Bantuan IPR Selangor</span>

@endsection

@section('content')

<div class="card">
    <div class="card-body">
        <ul class="nav nav-tabs nav-tabs-solid nav-justified rounded bg-light">
            <li class="nav-item"><a href="/penerima/edit/{{ $recepient->no_kp}}" class="nav-link">Penerima</a></li>
            <li class="nav-item"><a href="/tanggungan/{{ $recepient->no_kp}}" class="nav-link">Tanggungan</a></li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link rounded-right dropdown-toggle active" data-toggle="dropdown">Bantuan</a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="/bantuan/{{ $recepient->no_kp }}" class="dropdown-item active">Bantuan IPR Selangor</a>
                    <a href="/bantuan_khas/{{ $recepient->no_kp }}" class="dropdown-item">Bantuan Khas</a>
                </div>
            </li>
        </ul>
        <div class="card">
            <div class="card-body">
                <h5>Bantuan IPR Selangor</h5>
                <form method="POST">
                    @csrf
                    <div class="col-md-3 form-group-feedback form-group-feedback-left">
                        <input type="text" class="form-control form-control-sm alpha-grey" name="tahun" placeholder="Cth : 2020"><button type="submit" class="btn btn-primary btn-sm">Carian</button>
                        <div class="form-control-feedback form-control-feedback-sm">
                            <i class="icon-search4 text-muted"></i>
                        </div>
                    </div>
                </form>
            </div>

            {{-- Index Recepient --}}
            <table class="table table-xs">
                <thead>
                    <tr>
                        <th style="width:25%">Bantuan</th>
                        <th style="width:13%">Tarikh Mohon</th>
                        <th style="width:13%">Tarikh lulus</th>
                        <th style="width:15%">Status Kelulusan</th>
                        <th style="width:15%">Tindakan</th>
                    </tr>
                </thead>
                <tbody>

                    <td><button type="button" class="btn bg-blue-600 btn-labeled btn-labeled-left btn-sm" data-toggle="modal" data-target="#modal_add_recepient"><b><i class="icon-plus3"></i></b>Penerima : {{ $recepient->profile->nama }}</button></td>

                    @foreach ($donations as $donation)
                        <tr>
                            <td>{{ $donation->refDonation->nama }}</td>

                            <td>
                                @if(!empty($donation->tarikh_mohon))
                                    {{ date('d-m-Y', strtotime($donation->tarikh_mohon)) }}
                                @else
                                    -
                                @endif
                            </td>
                            <td>
                                @if(!empty($donation->tarikh_lulus))
                                    {{ date('d-m-Y', strtotime($donation->tarikh_lulus)) }}
                                @else
                                    -
                                @endif
                            </td>

                            <td>
                            @if($donation->status_kelulusan == 'Lulus')
                                <span class="badge badge-success">Lulus</span>
                            @elseif(($donation->status_kelulusan == 'Tidak Lulus'))
                                <span class="badge badge-secondary">Tidak Lulus</span>
                            @endif

                            </td>
                            <td>
                                <form class="delete" action="/bantuan/destroy/{{ $donation->id }}" method="POST">
                                    <a href="#" data-toggle="modal" data-target="#recepientShowModal-{{ $donation->id }}" class="btn bg-info-600 badge-icon rounded-round" title="Papar"><i class="icon-list"></i></a>
                                    <a href="#" data-toggle="modal" data-target="#modal_edit_recepient_{{ $donation->id }}" class="btn bg-info-600 badge-icon rounded-round" title="Kemaskini"><i class="icon-pencil"></i></a>
                                    <input type="hidden" name="_method" value="DELETE">
                                    {{ csrf_field() }}
                                    <button type="submit" id="confirm" class="btn bg-danger-600 badge-icon rounded-round" title="Hapus"><i class="icon-trash"></i></button>

                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{-- END Index Recepient --}}
<hr>
            {{-- Index Dependent --}}
            {{-- <div class="card-body">
                <h6>Maklumat Tanggungan</h6>
            </div> --}}
            {{-- @foreach ($recepient->dependents as $dependent)
            <table class="table table-xs">

                <tbody>
                    <td><button type="button" class="btn bg-blue-600 btn-labeled btn-labeled-left btn-sm" data-toggle="modal" data-placeholder="Tambah Bantuan" data-target="#modal_add_dependent_{{ $dependent->id }}"><b><i class="icon-plus3"></i></b>{{ $dependent->nama }} ( {{$dependent->hubungan}} )</button></td>
                        @foreach ($dependent->dependentDonations as $dependentDonation)
                        <tr>
                            <td style="width: 25%">{{ $dependentDonation->refDonation->nama }}</td>
                            <td style="width: 13%">
                                @if(!empty($dependentDonation->tarikh_mohon))
                                    {{ date('d-m-Y', strtotime($dependentDonation->tarikh_mohon)) }}
                                @else
                                    -
                                @endif
                            </td>
                            <td style="width: 13%">
                                @if(!empty($dependentDonation->tarikh_lulus))
                                    {{ date('d-m-Y', strtotime($dependentDonation->tarikh_lulus)) }}
                                @else
                                    -
                                @endif
                            </td>

                            <td style="width: 15%">
                                @if($dependentDonation->status_kelulusan == 'Lulus')
                                <span class="badge badge-success">Lulus</span>
                            @elseif(($dependentDonation->status_kelulusan == 'Tidak Lulus'))
                                <span class="badge badge-secondary">Tidak Lulus</span>
                            @endif

                            </td>
                            <td style="width:15%">
                                <form class="delete" action="/bantuan/tanggungan/destroy/{{ $dependentDonation->id }}" method="POST">
                                    <a href="#" data-toggle="modal" data-target="#dependentShowModal-{{ $dependentDonation->id }}" class="btn bg-info-600 badge-icon rounded-round" title="Papar"><i class="icon-list"></i></a>
                                    <a href="#" data-toggle="modal" data-target="#modal_edit_dependent_{{ $dependentDonation->id }}" class="btn bg-info-600 badge-icon rounded-round" title="Kemaskini"><i class="icon-pencil"></i></a>
                                    <input type="hidden" name="_method" value="DELETE">
                                    {{ csrf_field() }}
                                    <button type="submit" id="confirm" class="btn bg-danger-600 badge-icon rounded-round" title="Padam-b"><i class="icon-trash"></i></button>

                                </form>
                            </td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
            @endforeach --}}
            {{-- END Index Dependent --}}


            {{-- Modal : View Recepient Other Donation --}}
            @foreach ($recepient->donations as $donation)
            <div class="modal fade" id="recepientShowModal-{{ $donation->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <h6 class="modal-title">Maklumat Bantuan IPR Selangor (Penerima)</h6>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="card">
                            <div class="card-body">

                                <table class="table table-sm">
                                    <tr>
                                        <td style="width: 15%; text-align: right">Nama Bantuan</td>
                                        <td style="width: 5%">:</td>
                                        <td>{{ $donation->refDonation->nama }}</td>
                                    </tr>

                                    <tr>
                                        <td style="width: 15%; text-align: right">Tanggungan</td>
                                        <td style="width: 5%">:</td>
                                        @if(!empty($donation->dependent_id))
                                        <td>{{ $donation->dependent->nama }}</td>
                                        @endif
                                    </tr>

                                    <tr>
                                        <td style="width: 40%; text-align: right">Tarikh Mohon</td>
                                        <td style="width: 5%">:</td>
                                        <td>
                                            @if(!empty($donation->tarikh_mohon))
                                                {{ date('d-m-Y', strtotime($donation->tarikh_mohon)) }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 40%; text-align: right">Tarikh Lulus</td>
                                        <td style="width: 5%">:</td>
                                        <td>
                                            @if(!empty($donation->tarikh_lulus))
                                                {{ date('d-m-Y', strtotime($donation->tarikh_lulus)) }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="width: 40%; text-align: right">Tarikh Mula</td>
                                        <td style="width: 5%">:</td>
                                        <td>
                                            @if(!empty($donation->tarikh_mula))
                                                {{ date('d-m-Y', strtotime($donation->tarikh_mula)) }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="width: 40%; text-align: right">Tarikh Tamat</td>
                                        <td style="width: 5%">:</td>
                                        <td>
                                            @if(!empty($donation->tarikh_akhir))
                                                {{ date('d-m-Y', strtotime($donation->tarikh_akhir)) }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: right">Status Kelulusan</td>
                                        <td style="width: 5%">:</td>
                                        <td>{{ $donation->status_kelulusan }}</td>
                                    </tr>

                                    <tr>
                                        <td style="width: 15%; text-align: right">Dokumen</td>
                                        <td style="width: 5%">:</td>
                                        <td>

                                            <div class="form-check form-check-inline form-check-right">
                                                <label class="form-check-label">
                                                    Salinan KP
                                                    <input type="checkbox" class="form-check-input-styled" @if($donation->salinan_kp == '1') checked @endif disabled data-fouc>
                                                </label>
                                            </div>

                                            <div class="form-check form-check-inline form-check-right">
                                                <label class="form-check-label">
                                                    KP1
                                                    <input type="checkbox" class="form-check-input-styled" @if($donation->kp1 == '1') checked @endif disabled data-fouc>
                                                </label>
                                            </div>

                                            <div class="form-check form-check-inline form-check-right">
                                                <label class="form-check-label">
                                                    KP2
                                                    <input type="checkbox" class="form-check-input-styled" @if($donation->kp2 == '1') checked @endif disabled data-fouc>
                                                </label>
                                            </div>

                                            <div class="form-check form-check-inline form-check-right">
                                                <label class="form-check-label">
                                                    Slip Gaji
                                                    <input type="checkbox" class="form-check-input-styled" @if($donation->slip_gaji == '1') checked @endif disabled data-fouc>
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            @endforeach


             {{-- Modal : View Dependent Other Donation --}}
            {{-- @foreach ($recepient->dependents as $dependent)
                @foreach ($dependent->dependentDonations as $dependentDonation)
                    <div class="modal fade" id="dependentShowModal-{{ $dependentDonation->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header bg-primary">
                                    <h6 class="modal-title">Maklumat Bantuan IPR Selangor (Tanggungan)</h6>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="card">
                                    <div class="card-body">

                                        <table class="table table-sm">
                                                <tr>
                                                    <td style="width: 15%; text-align: right">Nama Bantuan</td>
                                                    <td style="width: 5%">:</td>
                                                    <td>{{ $dependentDonation->refDonation->nama }}</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 40%; text-align: right">Tarikh Mohon</td>
                                                    <td style="width: 5%">:</td>
                                                    <td>
                                                        @if(!empty($dependentDonation->tarikh_mohon))
                                                            {{ date('d-m-Y', strtotime($dependentDonation->tarikh_mohon)) }}
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 40%; text-align: right">Tarikh Lulus</td>
                                                    <td style="width: 5%">:</td>
                                                    <td>
                                                        @if(!empty($dependentDonation->tarikh_lulus))
                                                            {{ date('d-m-Y', strtotime($dependentDonation->tarikh_lulus)) }}
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width: 40%; text-align: right">Tarikh Mula</td>
                                                    <td style="width: 5%">:</td>
                                                    <td>
                                                        @if(!empty($dependentDonation->tarikh_mula))
                                                            {{ date('d-m-Y', strtotime($dependentDonation->tarikh_mula)) }}
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width: 40%; text-align: right">Tarikh Tamat</td>
                                                    <td style="width: 5%">:</td>
                                                    <td>
                                                        @if(!empty($dependentDonation->tarikh_akhir))
                                                            {{ date('d-m-Y', strtotime($dependentDonation->tarikh_akhir)) }}
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td style="width: 15%; text-align: right">Status Kelulusan</td>
                                                    <td style="width: 5%">:</td>
                                                    <td>{{ $dependentDonation->status_kelulusan }}</td>
                                                </tr>

                                                <tr>
                                                    <td style="width: 15%; text-align: right">Dokumen</td>
                                                    <td style="width: 5%">:</td>
                                                    <td>

                                                        <div class="form-check form-check-inline form-check-right">
                                                            <label class="form-check-label">
                                                                Salinan KP
                                                                <input type="checkbox" class="form-check-input-styled" @if($dependentDonation->salinan_kp == '1') checked @endif disabled data-fouc>
                                                            </label>
                                                        </div>

                                                        <div class="form-check form-check-inline form-check-right">
                                                            <label class="form-check-label">
                                                                KP1
                                                                <input type="checkbox" class="form-check-input-styled" @if($dependentDonation->kp1 == '1') checked @endif disabled data-fouc>
                                                            </label>
                                                        </div>

                                                        <div class="form-check form-check-inline form-check-right">
                                                            <label class="form-check-label">
                                                                KP2
                                                                <input type="checkbox" class="form-check-input-styled" @if($dependentDonation->kp2 == '1') checked @endif disabled data-fouc>
                                                            </label>
                                                        </div>

                                                        <div class="form-check form-check-inline form-check-right">
                                                            <label class="form-check-label">
                                                                Slip Gaji
                                                                <input type="checkbox" class="form-check-input-styled" @if($dependentDonation->slip_gaji == '1') checked @endif disabled data-fouc>
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>

                                        </table>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
            @endforeach --}}

            <!-- modal_add_donation_recepient -->
                <div id="modal_add_recepient" class="modal fade" tabindex="-1">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Bantuan IPR Selangor</h5>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <form action="/bantuan/create/{{ $recepient->no_kp }}" method="post" class="form-horizontal">
                                @csrf
								<div class="modal-body">
                                    <td><span type="text" class="btn bg-blue-600 btn-labeled btn-labeled-left btn-sm"><b><i class="icon-user"></i></b> {{ $recepient->profile->nama }} ({{ $recepient->no_kp }})</span></td>
                                    <br>
                                    <span class="muted">Tanda </span> <span class="text-danger">*</span> <span class="muted">wajib di isi. </span>
                                    <hr>

									<div class="form-group row">
                                        <label class="col-form-label col-sm-3">Nama Bantuan <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <select name="donation" data-placeholder="Pilih Bantuan" class="form-control form-control-select2 @if ($errors->has('donation')) border-danger @endif" data-fouc>
                                                <option></option>
                                                @foreach ($refDonations as $refDonation)
                                                <option value="{{ $refDonation->id }}" @if(old('donation')==$refDonation->id) selected @endif>{{ $refDonation->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-sm-3">Tanggungan (Menerima)</label>
                                        <div class="col-sm-9">
                                            <select name="dependent" data-placeholder="Pilih" class="form-control form-control-select2 @if ($errors->has('dependent')) border-danger @endif" data-fouc>
                                                <option></option>
                                                @foreach ($dependents as $dependent)
                                                <option value="{{ $dependent->id }}" @if(old('dependent')==$dependent->id) selected @endif>{{ $dependent->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
										<label class="col-form-label col-sm-3">Tarikh Mohon <span class="text-danger">*</span></label>
										<div class="col-sm-9">
											<input class="form-control" type="date" name="tarikh_mohon">
										</div>
                                    </div>

                                    <div class="form-group row">
										<label class="col-form-label col-sm-3">Tarikh Lulus <span class="text-danger">*</span></label>
										<div class="col-sm-9">
											<input class="form-control" type="date" name="tarikh_lulus">
										</div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-sm-3">Status Kelulusan <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <select name="status_kelulusan" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                                <option value="Lulus" @if(old('status_kelulusan')=='Lulus' ) selected @endif>Lulus</option>
                                                <option value="Tidak Lulus" @if(old('status_kelulusan')=='Tidak Lulus' ) selected @endif>Tidak Lulus</option>
                                            </select>
                                        </div>
                                    </div>

									<div class="form-group row">
										<label class="col-form-label col-sm-3">Tarikh Mula</label>
										<div class="col-sm-9">
											<input class="form-control" type="date" name="tarikh_mula">
										</div>
                                    </div>

                                    <div class="form-group row">
										<label class="col-form-label col-sm-3">Tarikh Akhir</label>
										<div class="col-sm-9">
											<input class="form-control" type="date" name="tarikh_akhir">
										</div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-sm-3">Dokumen</label>
                                        <div class="col-sm-9">
                                            <div class="form-group mb-3 mb-md-2">
                                                <div class="form-check form-check-inline form-check-right">
                                                    <label class="form-check-label">
                                                        Salinan KP
                                                        <input type="checkbox" class="form-check-input-styled" name="salinan_kp" value="1" data-fouc>
                                                    </label>
                                                </div>

                                                <div class="form-check form-check-inline form-check-right">
                                                    <label class="form-check-label">
                                                        KP1
                                                        <input type="checkbox" class="form-check-input-styled" name="kp1" value="1" data-fouc>
                                                    </label>
                                                </div>

                                                <div class="form-check form-check-inline form-check-right">
                                                    <label class="form-check-label">
                                                        KP2
                                                        <input type="checkbox" class="form-check-input-styled" name="kp2" value="1" data-fouc>
                                                    </label>
                                                </div>

                                                <div class="form-check form-check-inline form-check-right">
                                                    <label class="form-check-label">
                                                        Slip Gaji
                                                        <input type="checkbox" class="form-check-input-styled" name="slip_gaji" value="1" data-fouc>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label class="col-form-label col-sm-3">Nota Tambahan:</label>
										<div class="col-sm-9">
                                            <textarea rows="5" cols="3" name="nota" class="form-control" placeholder="Masukkan nota tambahan anda di sini">{{ old('nota') }}</textarea>
										</div>
                                    </div>

									<div class="form-group row">
                                        <label class="col-form-label col-sm-3">Status</label>
                                        <div class="col-sm-9">
                                            <select name="status" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                                <option value="Aktif" @if(old('status')=='Aktif' ) selected @endif>Aktif</option>
                                                <option value="Tidak Aktif" @if(old('status')=='Tidak Aktif' ) selected @endif>Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
									<button type="submit" class="btn bg-primary">Simpan</button>
								</div>
                            </form>

						</div>
					</div>
				</div>
            <!-- /modal_add_donation_recepient -->


            <!-- modal_add_donation_dependent -->
            @foreach ($recepient->dependents as $dependent)
                <div id="modal_add_dependent_{{ $dependent->id }}" class="modal fade" tabindex="-1">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Bantuan IPR Selangor</h5>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>

                            <form action="/bantuan/tanggungan/create/{{ $recepient->no_kp }}" method="post" class="form-horizontal">
                                @csrf
								<div class="modal-body">
                                    <td><span type="text" class="btn bg-blue-600 btn-labeled btn-labeled-left btn-sm"><b><i class="icon-user"></i></b> {{ $dependent->nama }} ({{ $recepient->no_kp }})</span></td>
                                    <br>
                                    <span class="muted">Tanda </span> <span class="text-danger">*</span> <span class="muted">wajib di isi. </span>
                                    <hr>
                                    <input type="hidden" name="dependentId" value="{{$dependent->id}}">
									<div class="form-group row">
                                        <label class="col-form-label col-sm-3">Nama Bantuan <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <select name="donation" data-placeholder="Pilih Bantuan" class="form-control form-control-select2 @if ($errors->has('donation')) border-danger @endif" data-fouc>
                                                <option></option>
                                                @foreach ($refDonations as $refDonation)
                                                <option value="{{ $refDonation->id }}" @if(old('donation')==$refDonation->id) selected @endif>{{ $refDonation->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
									</div>

                                    <div class="form-group row">
										<label class="col-form-label col-sm-3">Tarikh Mohon <span class="text-danger">*</span></label>
										<div class="col-sm-9">
											<input class="form-control" type="date" name="tarikh_mohon">
										</div>
                                    </div>

                                    <div class="form-group row">
										<label class="col-form-label col-sm-3">Tarikh Lulus </label>
										<div class="col-sm-9">
											<input class="form-control" type="date" name="tarikh_lulus">
										</div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-sm-3">Status Kelulusan <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <select name="status_kelulusan" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                                <option value="Lulus" @if(old('status_kelulusan')=='Lulus' ) selected @endif>Lulus</option>
                                                <option value="Tidak Lulus" @if(old('status_kelulusan')=='Tidak Lulus' ) selected @endif>Tidak Lulus</option>
                                            </select>
                                        </div>
                                    </div>

									<div class="form-group row">
										<label class="col-form-label col-sm-3">Tarikh Mula</label>
										<div class="col-sm-9">
											<input class="form-control" type="date" name="tarikh_mula">
										</div>
                                    </div>

                                    <div class="form-group row">
										<label class="col-form-label col-sm-3">Tarikh Akhir</label>
										<div class="col-sm-9">
											<input class="form-control" type="date" name="tarikh_akhir">
										</div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-sm-3">Dokumen <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <div class="form-group mb-3 mb-md-2">
                                                <div class="form-check form-check-inline form-check-right">
                                                    <label class="form-check-label">
                                                        Salinan KP
                                                        <input type="checkbox" class="form-check-input-styled" name="salinan_kp" value="1" data-fouc>
                                                    </label>
                                                </div>

                                                <div class="form-check form-check-inline form-check-right">
                                                    <label class="form-check-label">
                                                        KP1
                                                        <input type="checkbox" class="form-check-input-styled" name="kp1" value="1" data-fouc>
                                                    </label>
                                                </div>

                                                <div class="form-check form-check-inline form-check-right">
                                                    <label class="form-check-label">
                                                        KP2
                                                        <input type="checkbox" class="form-check-input-styled" name="kp2" value="1" data-fouc>
                                                    </label>
                                                </div>

                                                <div class="form-check form-check-inline form-check-right">
                                                    <label class="form-check-label">
                                                        Slip Gaji
                                                        <input type="checkbox" class="form-check-input-styled" name="slip_gaji" value="1" data-fouc>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-sm-3">Nota Tambahan:</label>
										<div class="col-sm-9">
                                            <textarea rows="5" cols="3" name="nota" class="form-control" placeholder="Masukkan nota tambahan anda di sini">{{ old('nota') }}</textarea>
										</div>
                                    </div>

									<div class="form-group row">
                                        <label class="col-form-label col-sm-3">Status</label>
                                        <div class="col-sm-9">
                                            <select name="status" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                                <option value="Aktif" @if(old('status')=='Aktif' ) selected @endif>Aktif</option>
                                                <option value="Tidak Aktif" @if(old('status')=='Tidak Aktif' ) selected @endif>Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
                                    <button type="submit" class="btn bg-primary">Simpan</button>
                                </div>
                            </form>


						</div>
					</div>
                </div>
            @endforeach
            <!-- /modal_add_donation_dependent -->


            <!-- EDIT RECEPIENT DONATION
                modal_edit_donation_recepient -->
                @foreach ($recepient->donations as $donation)
                <div id="modal_edit_recepient_{{ $donation->id }}" class="modal fade" tabindex="-1">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Bantuan IPR Selangor</h5>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <form action="/bantuan/edit/{{ $donation->id }}" method="post" class="form-horizontal">
                                @csrf
								<div class="modal-body">
                                    <td><span type="text" class="btn bg-blue-600 btn-labeled btn-labeled-left btn-sm"><b><i class="icon-user"></i></b> {{ $recepient->profile->nama }} ({{ $recepient->no_kp }})</span></td>
                                    <br>
                                    <span class="muted">Tanda </span> <span class="text-danger">*</span> <span class="muted">wajib di isi. </span>
                                    <hr>
									<div class="form-group row">
                                        <label class="col-form-label col-sm-3">Nama Bantuan <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <select name="donation" data-placeholder="Pilih Bantuan" class="form-control form-control-select2 @if ($errors->has('donation')) border-danger @endif" data-fouc>
                                                <option></option>
                                                @foreach ($refDonations as $refDonation)
                                                <option value="{{ $refDonation->id }}" @if(old('donation', $donation->ref_donation_id)==$refDonation->id) selected @endif>{{ $refDonation->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-sm-3">Tanggungan (Menerima)</label>
                                        <div class="col-sm-9">
                                            <select name="dependent" data-placeholder="Pilih" class="form-control form-control-select2 @if ($errors->has('dependent')) border-danger @endif" data-fouc>
                                                <option></option>
                                                @foreach ($dependents as $dependent)
                                                <option value="{{ $dependent->id }}" @if(old('dependent', $donation->dependent_id)==$dependent->id) selected @endif>{{ $dependent->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-sm-3">Tarikh Mohon <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="date" name="tarikh_mohon" value="{{ old('tarikh_mohon', $donation->tarikh_mohon )}}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-sm-3">Tarikh Lulus <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="date" name="tarikh_lulus" value="{{ old('tarikh_lulus', $donation->tarikh_lulus) }}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-sm-3">Status Kelulusan <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <select name="status_kelulusan" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                                <option value="Lulus" @if(old('status_kelulusan', $donation->status_kelulusan)=='Lulus') selected @endif>Lulus</option>
                                                <option value="Tidak Lulus" @if(old('status_kelulusan', $donation->status_kelulusan)=='Tidak Lulus') selected @endif>Tidak Lulus</option>
                                            </select>
                                        </div>
                                    </div>

									<div class="form-group row">
										<label class="col-form-label col-sm-3">Tarikh Mula</label>
										<div class="col-sm-9">
                                        <input class="form-control" type="date" name="tarikh_mula" value="{{ $donation->tarikh_mula }}">
										</div>
                                    </div>

                                    <div class="form-group row">
										<label class="col-form-label col-sm-3">Tarikh Akhir</label>
										<div class="col-sm-9">
											<input class="form-control" type="date" name="tarikh_akhir" value="{{ $donation->tarikh_akhir }}">
										</div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-sm-3">Dokumen </label>
                                        <div class="col-sm-9">
                                            <div class="form-group mb-3 mb-md-2">

                                                <div class="form-check form-check-inline form-check-right">
                                                    <label class="form-check-label">
                                                        Salinan KP
                                                        <input type="checkbox" class="form-check-input-styled" name="salinan_kp" value="1" @if($donation->salinan_kp == true) checked @endif data-fouc>
                                                    </label>
                                                </div>

                                                <div class="form-check form-check-inline form-check-right">
                                                    <label class="form-check-label">
                                                        KP1
                                                        <input type="checkbox" class="form-check-input-styled" name="kp1" value="1" @if($donation->kp1 == true) checked @endif data-fouc>
                                                    </label>
                                                </div>

                                                <div class="form-check form-check-inline form-check-right">
                                                    <label class="form-check-label">
                                                        KP2
                                                        <input type="checkbox" class="form-check-input-styled" name="kp2" value="1" @if($donation->kp2 == true) checked @endif data-fouc>
                                                    </label>
                                                </div>

                                                <div class="form-check form-check-inline form-check-right">
                                                    <label class="form-check-label">
                                                        Slip Gaji
                                                        <input type="checkbox" class="form-check-input-styled" name="slip_gaji" value="1" @if($donation->slip_gaji == true) checked @endif data-fouc>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-form-label col-sm-3">Nota Tambahan:</label>
										<div class="col-sm-9">
                                            <textarea rows="5" cols="5" name="nota" class="form-control" placeholder="Masukkan nota tambahan anda di sini">{{ old('nota', $donation->nota ) }}</textarea>
										</div>
                                    </div>

									<div class="form-group row">
                                        <label class="col-form-label col-sm-3">Status</label>
                                        <div class="col-sm-9">
                                            <select name="status" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                                <option value="Aktif" @if(old('status', $donation->status)=='Aktif' ) selected @endif>Aktif</option>
                                                <option value="Tidak Aktif" @if(old('status',$donation->status)=='Tidak Aktif' ) selected @endif>Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
									<button type="submit" class="btn bg-primary">Kemaskini</button>
								</div>
                            </form>

						</div>
					</div>
                </div>
                @endforeach
            <!-- EDIT
                /modal_add_donation_recepient -->


            <!-- EDIT DEPENDENT DONATION
                modal_edit_donation_dependent -->
            @foreach ($recepient->dependents as $dependent)
                @foreach ($dependent->dependentDonations as $dependentDonation)
                    <div id="modal_edit_dependent_{{ $dependentDonation->id }}" class="modal fade" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Bantuan IPR Selangor</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <form action="/bantuan/tanggungan/edit/{{ $dependentDonation->id }}" method="post" class="form-horizontal">
                                    @csrf
                                    <div class="modal-body">
                                        <td><span type="text" class="btn bg-blue-600 btn-labeled btn-labeled-left btn-sm"><b><i class="icon-user"></i></b> {{ $dependent->nama }} ({{ $dependent->no_kp }})</span></td>
                                        <hr>
                                        <input type="hidden" name="dependentId" value="{{$dependent->id}}">
                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3">Nama Bantuan</label>
                                            <div class="col-sm-9">
                                                <select name="donation" data-placeholder="Pilih Bantuan" class="form-control form-control-select2 @if ($errors->has('donation')) border-danger @endif" data-fouc>
                                                    <option></option>
                                                    @foreach ($refDonations as $refDonation)
                                                    <option value="{{ $refDonation->id }}" @if(old('donation', $dependentDonation->id)==$refDonation->id) selected @endif>{{ $refDonation->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3">Tarikh Mohon</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="date" name="tarikh_mohon" value="{{ old('tarikh_mohon', $dependentDonation->tarikh_mohon )}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3">Tarikh Lulus</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="date" name="tarikh_lulus" value="{{ old('tarikh_lulus', $dependentDonation->tarikh_lulus) }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3">Status Kelulusan</label>
                                            <div class="col-sm-9">
                                                <select name="status_kelulusan" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                                    <option value="Lulus" @if(old('status_kelulusan', $dependentDonation->status)=='Lulus') selected @endif>Lulus</option>
                                                    <option value="Tidak Lulus" @if(old('status_kelulusan', $dependentDonation->status)=='Tidak Lulus') selected @endif>Tidak Lulus</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3">Tarikh Mula</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="date" name="tarikh_mula" value="{{ old('tarikh_mula', $dependentDonation->tarikh_mula )}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3">Tarikh Akhir</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="date" name="tarikh_akhir" value="{{ old('tarikh_akhir', $dependentDonation->tarikh_akhir) }}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3">Dokumen </label>
                                            <div class="col-sm-9">
                                                <div class="form-group mb-3 mb-md-2">

                                                    <div class="form-check form-check-inline form-check-right">
                                                        <label class="form-check-label">
                                                            Salinan KP
                                                            <input type="checkbox" class="form-check-input-styled" name="salinan_kp" value="1" @if($dependentDonation->salinan_kp == true) checked @endif data-fouc>
                                                        </label>
                                                    </div>

                                                    <div class="form-check form-check-inline form-check-right">
                                                        <label class="form-check-label">
                                                            KP1
                                                            <input type="checkbox" class="form-check-input-styled" name="kp1" value="1" @if($dependentDonation->kp1 == true) checked @endif data-fouc>
                                                        </label>
                                                    </div>

                                                    <div class="form-check form-check-inline form-check-right">
                                                        <label class="form-check-label">
                                                            KP2
                                                            <input type="checkbox" class="form-check-input-styled" name="kp2" value="1" @if($dependentDonation->kp2 == true) checked @endif data-fouc>
                                                        </label>
                                                    </div>

                                                    <div class="form-check form-check-inline form-check-right">
                                                        <label class="form-check-label">
                                                            Slip Gaji
                                                            <input type="checkbox" class="form-check-input-styled" name="slip_gaji" value="1" @if($dependentDonation->slip_gaji == true) checked @endif data-fouc>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3">Nota Tambahan:</label>
                                            <div class="col-sm-9">
                                                <textarea rows="5" cols="5" name="nota" class="form-control" placeholder="Masukkan nota tambahan anda di sini">{{ old('nota', $dependentDonation->nota) }}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-form-label col-sm-3">Status</label>
                                            <div class="col-sm-9">
                                                <select name="status" data-placeholder="Sila Pilih" class="form-control form-control-select2" data-fouc>
                                                    <option value="Aktif" @if(old('status', $dependentDonation->status)=='Aktif' ) selected @endif>Aktif</option>
                                                    <option value="Tidak Aktif" @if(old('status', $dependentDonation->status)=='Tidak Aktif' ) selected @endif>Tidak Aktif</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
                                        <button type="submit" class="btn bg-primary">Kemaskini</button>
                                    </div>
                                </form>


                            </div>
                        </div>
                    </div>
                @endforeach
            @endforeach
            <!-- /modal_add_donation_dependent -->
        </div>

    </div>
</div>

@endsection

@section('script')
    <script>
        $(".delete").on("submit", function(){
            return confirm("Adakah anda pasti untuk menghapus rekod?");
        });

    </script>
@endsection
